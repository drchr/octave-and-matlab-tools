%% Return struct with details about this package.
%
% Note: This script is expected to be located in the root
% of the package directory tree, i.e. one level above
%     misc/package_info.m
%
%!demo
%!  old_dir = pwd();
%!  cd(fileparts(fileparts(which('package_info'))));
%!  info = package_info_about_this_package()
%!  # Gets info struct about this package
%
% See also package_info.m

% © 2017 Christian Ridderström <chr@kth.se>. Licensed under LGPL v3 or later.
function info = package_info_about_this_package()
  narginchk(0, 0);

  name = 'chr-toolbox (TBC)';
  version = '1.10';
  desc = 'Package with miscellanous Octave/MATLAB script by Christian Ridderström';
  info = struct('name', name, 'version', version, 'description', desc);
end



%%% Section with Octave test cases
%
% Convenience function to avoid modifying tests if renaming the FUT.
% (FUT = function under test).
%
%!function fut = FUT() fut = @package_info_about_this_package; end


%!test % Simple smoke test
%!  fut = FUT();
%!  fut();


%!error<narginchk: too many input arguments>FUT()(1)
