#!/bin/bash
#
# Support script to generate HTMl documentation from .org-files
# in a directory.
#
# Example:
#	generate-HTML-for-org-mode-documents.sh doc
#
# Caveat: Script assumes it's executed from the project's root.

doc_dir=$1
doc_files=( $doc_dir/*.org )

function remove_extension() {
    local filename="$1"
    local extension="$2"
    printf "%s" "${filename/.$extension/}"
}

function replace_extension() {
    local filename=$1
    local old_extension=$2
    local new_extension=$3
    local base_name=$(remove_extension "$filename" "$old_extension")
    printf "%s" "${base_name}.$new_extension"
}

function generate_html() {
    local file_org=$1
    local file_html=$(replace_extension $file_org org html)
    printf "# Generate %s from %s\n" "$file_org" "$file_html"
    emacs --batch $file_org -f org-html-export-to-html
    printf "# ... HTML generated from %s\n" "$file_org"
}

function is_version_controlled() {
    local file="$1"
    git ls-files --error-unmatch "$file" >/dev/null 2>/dev/null
}

for file_org in "${doc_files[@]}"; do
    file_html=$(replace_extension "$file_org" org html)
    is_version_controlled $file_html && printf "# Skipped %s\n" $file_html
    is_version_controlled $file_html || generate_html $file_org
done

exit 0                     # Set exit to ok, e.g. if used from 'make'.

