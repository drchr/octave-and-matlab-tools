%% Add the subfolder 'misc/' to the Octave/MATLAB path, making the
% scripts therein globally usable.
%
% Example:
% 	run(fullfile('<path-to-repo-root>', 'add_paths_for_misc.m'));

% © 2017 Christian Ridderström <chr@kth.se>. Licensed under LGPL v3 or later.
addpath(fullfile(fileparts(mfilename('fullpath')), 'misc'));


%%% Section with test cases - No tests defined.
