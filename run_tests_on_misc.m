%% Run (Octave) test cases for scripts in the subfolder 'misc/'.
%
% Example:
%       run(fullfile('<path-to-repo-root>', 'run_tests_on_misc.m'));

% © 2017 Christian Ridderström <chr@kth.se>. Licensed under LGPL v3 or later.

runtests(fullfile(fileparts(mfilename('fullpath')), 'misc'));

%%% Section with test cases - No tests defined.
