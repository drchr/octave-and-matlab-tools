function y=raw(expObj, varargin)
% ExpData/raw -	Returns raw signal data
%
% Fcn:	Returns raw signal data of an ExpData-object
% In:	expObj	= ExpData-object
%	sig	= Signal information
%	varargin = Optional arguments, see examples
% Out:	y	= Signal data
% 
% Stx:	raw(expObj, 
%
% In the examples below, 'sig' is a typical name of a signal.
% >> D = raw(e);	% Return matrix with data for all signals
% >> raw(e, '(:,1)')	% Return D((:,1)
% >> raw(e, '(1:2,:)')	% Return D(1:2,:)
% >> raw(e, {L,':'});	% Return subsref(D, substruct('()',{L,':'}))
%
% >> s=raw(e, 'sig');		% Signal data for all signals 'sig'
% >> s=raw(e, 'sig', '(1,:)');	% Equiv. to:  raw(e,'sig'); ans(1,:)
%
% Two examples of getting 1st occurence, 2nd component of 'sig'
% >> s=raw(e, substruct('.', 'sig', '()', {1,2}));
% >> s=raw(e, {'sig', '()', {1,2}});  
%
% Getting more advanced, by extracting first two rows of the above
% >> s=raw(e, {'sig', '()', {1,2}}, '(1:2,:)')
%
% Copyright Christian Ridderström <chr@kth.se>, 2002-2019
  if expObj.status.verbose >= 2
    fprintf('ExpData/raw(): %d arguments\n', length(varargin));
    for i=1:length(varargin), disp(varargin{i}); end;
  end

  if length(varargin) == 0, y = expObj.data; return; end;

  if ischar(varargin{1})                % Is 1st arg. a string?
    if hasSignal(expObj, varargin{1})   % Is it a signal?
      y = subsref(expObj, substruct('.', varargin{1}));
    elseif varargin{1}(1)=='('		% Is it e.g. '(1:3,3:4)'?
      y = eval(sprintf('expObj.data%s', varargin{1}));
      return;
    else
      Err('Signal ''%s'' not found!', varargin{1});
    end
  elseif iscell(varargin{1})            % Is 1st arg. a cell array?
    if ischar(varargin{1}{1}) & hasSignal(expObj, varargin{1}{1})
      S = substruct('.', varargin{1}{:});
    else                                % Must be for full data
      S = substruct('()', varargin{1});
    end
    if expObj.status.verbose >= 2
      fprintf('ExpData/raw(): substruct(''.'', varargin{1}{:}) ->\n');
      for i=1:length(S), disp(S(i)); end;
    end
    if strcmp(S(1).type, '.')
      y = subsref(expObj, S);
    else
      y = subsref(expObj.data, S);
      return;
    end
  elseif isstruct(varargin{1})
    y = subsref(expObj, varargin{1});
  else
    ErrArgType(varargin{1}, 'Should be a string or cell array');
  end
  
  if length(varargin) == 1, return; end;
  if ischar(varargin{2})
    if varargin{2}(1) == '('
      y = eval(sprintf('y%s', varargin{2}));
    else
      Err('Should be a string starting with ''(''!');
    end
  else
    ErrArgType(varargin{2}, 'Should be a string');
  end
    
function Err(varargin)
  Error(mfilename, varargin{:})

function ErrArgType(varargin)
  ErrorArgType(mfilename, varargin{:})
