function [expObj,t] = translateTime(expObj, deltaTime)
% ExpData/translateTime - Returns a new object with a translated time
%
% Fcn:	Changes the time from t -> t + deltaTime
% Ex:	e = translateTime(e, deltaTime);
%
% Rename this function to:
%	timeShift
%
%
% Copyright Christian Ridderström <chr@kth.se>, 2002-2019
  [t, tCol] = timeVector(expObj);
  t  = t + deltaTime;
  expObj.data(:, tCol) = t;
