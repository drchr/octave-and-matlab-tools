% ExpData/Constants -	Defines numerical constants for ExpData class
%
% Fcn:	Returns numerical constants for use with the ExpData class
% In:	expObj	= Not used
%	varargin = Optional arguments, see examples
% Out:	C	= A structure with constants, or numerical value
% 
% The structure of constants is as follows:
%   .status
%      .mode = { 'Empty', 'Simulation', 'Experiment' }
%      .dataSource = { 'Empty', 'Numerical', 'xPC' }
%
% Ex:	constants;		% Returns entire structure with constants
%	constants('status');	% Returns structure for 'status'
%
% Example of typical use:
% >> persistent C; if ~exists('C', 'var'), C = constants; end;
% >> C.status.mode...
%
% Copyright Christian Ridderström <chr@kth.se>, 2002-2019
function c=Constants(expObj, varargin)
  persistent C; if isempty(C), C = constants; end;
  c=constants(varargin{:});
