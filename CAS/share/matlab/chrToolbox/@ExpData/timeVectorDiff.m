function [dt, tCol] = timeVectorDiff(expObj)
% ExpData/timeVectorDiff - Returns the time vector difference
%
% Ex:	>>dt = timeVectorDiff(e);
%	or
%	>>[dt, tCol] = timeVectorDiff(e);
% Note:	length('dt') == size(data,1)-1
%
% Copyright Christian Ridderström <chr@kth.se>, 2002-2019
  if nargin == 0, help ExpData/timeVectorDiff; return; end;
  if ~isa(expObj, 'ExpData'), error('Only for ''ExpData''-objects'); end;

  I = strmatch('Clock', {expObj.signals.name});
  if isempty(I)
    warning('No ''Clock''-signal, time not available!'); tCol = [];
  else
    if length(I)>1, warning('Several instances of ''Clock'' found!'); end;
    tCol = expObj.signals(I(1)).I{1};
  end

  if isempty(tCol); dt = []; else dt=diff(expObj.data(:,tCol)); end;
