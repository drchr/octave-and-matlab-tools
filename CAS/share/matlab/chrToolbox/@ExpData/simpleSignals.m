function y = simpleSignals(expObj)
% ExpData/simpleSignals	- Get simple signals information
%
% Fcn:	
% In:	expObj	= ExpData-object
% 	sigName	= String with name of the signal, or 
%		  cell array with names
% Out:	y	= 0, is the string 'sigName' is not a signal
%		= 1, if the string 'sigName' is a signal
%		= vector as above
%
% Copyright Christian Ridderström <chr@kth.se>, 2002-2019
  if isempty(expObj.signals)
    error('No signals in data!');
  end
  y = expObj.simpleSignals;
