function help(varargin)
% ExpData Help-function
%
%	Members
% .data		Stores the data
% .signals	Stores signal information about the data
% .model	Stores handle (name) of the model?
% .info		Stores informationstructure about the ExpData-object
% .status	Stores statusinformation about the ExpData-object
% .rate		
% Copyright Christian Ridderström <chr@kth.se>, 2002-2019

%
% Log:
% 2001-02-04,	chr	Created
%
  if nargin==1
    if isa(varargin{1},'ExpData'), help ExpData/help; return; end;
  end;
