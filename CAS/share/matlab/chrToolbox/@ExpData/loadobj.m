function [expObj] = loadobj(obj)
% ExpData/loadobj - Overloaded load function for the ExpData-class
%
% In:	
%
%
% Copyright Christian Ridderström <chr@kth.se>, 2002-2019
  
%
% Log:
% 2001-02-04,	chr	Created
% 2001-11-26,	chr	Changed name of the class from expdata to ExpData
%
  warningOn = 1;
  if isa(obj, 'ExpData')
    if version(obj) == objver, expObj = obj; return; end;
    obj = struct(obj);
  elseif isa(obj, 'expdata')    	% This code is not tested!
    obj = struct(obj);    		% This code is not tested!
  elseif ~isa(obj, 'struct')
    error('The object must be a structure!');
  end
  
  % Now the object must be a structure.
  if ~isfield(obj, 'objectVersion')
    obj.objectVersion  = 0.9;	% Add a version number
  end
  
  %
  % Now we have to try and convert the object from its
  % old version to the current version.
  %
  % This will be done in several steps, where the object
  % first might be converted from v0.9 -> v1.0, then from
  % v1.0 -> v1.2 and so on.
  %
  
  %
  % Converting the object from 0.9 -> 1.0
  % This change only involves adding the versioncapabilites.
  %
  if obj.objectVersion == 0.9
    % We are almost done, since the member has already been added.
    obj.objectVersion = 1.0;
    if warningOn
      sprintf(['The ExpData-object ''%s'' was converted ' ...
	       'from version 0.9 to 1.0\n'], inputname(1));
      if warningOn > 1, warning(ans); else fprintf(['Warning: ' ans]); end;
    end
  end
  
  if obj.objectVersion == 1.0
    if warningOn
      sprintf(['The ExpData-object ''%s'' was converted ' ...
	       'from version 1.0 to 2.0\n'], inputname(1));
      if warningOn > 1, warning(ans); else fprintf(['Warning: ' ans]); end;
    end
  end
  
  if obj.objectVersion == objver
    expObj = class(obj, 'ExpData');
  else
    error(sprintf('Unable to convert from %d to %d'), ...
      obj.objectVersion, objver);
  end
  
