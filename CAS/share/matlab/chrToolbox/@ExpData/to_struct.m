function [signals] = to_struct(obj)
% ExpData/struct - Return struct with signals from ExpData object
%
% Fcn:	Generate struct with signals data
% In:	obj     = An ExpData object
% Out:	signals = Struct with signals as separate fields
%
% Example:
%   % Extract only the signals as a struct
%   signals = struct(expObj);
%
% Copyright Christian Ridderström <chr@kth.se>, 2002-2019
  signals = struct();

  for ii = 2:length(obj.signals)
    indices = obj.signals(ii).I;
    sigs = obj.data(:, [indices{:}]);
    signals.(obj.signals(ii).name) = sigs;
  end

  %% Note: It's actually slightly slower to use 'cell2struct'
  % signals = cell2struct(cellfun(@(idx) obj.data(:, [idx{:}]), ...
  %                               { obj.signals(2:end).I }, 'uniform', 0)', ...
  %                       { obj.signals(2:end).name }');
end
