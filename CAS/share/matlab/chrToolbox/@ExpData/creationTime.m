function [time] = creationTime(expObj, info)
% ExpData/creationTime	- Return time when the object was created
%
% Ex:	>>time = creationTime(expObj)
% Note:	Use 'datestr()' to convert the time into a text:
%	>>datestr(time)
% Just doing:
%	>>creationTime(expObj);
% prints the creation time
%
% Copyright Christian Ridderström <chr@kth.se>, 2002-2019
  if nargout==0,
    fprintf('Creation time: %s\n', datestr(expObj.info.creationTime));
  else
    time = expObj.info.creationTime;
  end;
