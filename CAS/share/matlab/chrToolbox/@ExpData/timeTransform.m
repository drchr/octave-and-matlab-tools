function [expObj,t] = timeTransform(expObj, dt, k)
% ExpData/timeTransform - Return new object with transformed time
%
% Fcn:	Changes the time from t -> dt + k*t
% Ex:	e = translateTime(e, 0.6, 1);
% 	[e,t] = translateTime(e, 0.6, 1);
%
% Rename this function to:
%	timeTransform(expObj, offset, scaling)
% that does: t = t*scaling + offset;
%
%
% Copyright Christian Ridderström <chr@kth.se>, 2002-2019
  [t, tCol] = timeVector(expObj);
  if nargin <= 1, dt = 0; end;
  if nargin <= 2, k = 1; end;
  t  = dt + k*t;
  expObj.data(:, tCol) = t;
