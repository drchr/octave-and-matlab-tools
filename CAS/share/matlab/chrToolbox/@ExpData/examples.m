% ExpData/examples -	Examples of how to use ExpData
%
% Creating an ExpData-object:
%
% 	>>e = ExpData(tg, 0.01, [-4 -1]);
%	% Upload data from the target computer, 'tg',
%	% in the intervall [-4, -1] from end of simulationa
%	% at the sample rate 0.01 S.
%
%	>>e = ExpData(tg, 0.1);
%	% Returns all available data from the target, 'tg'
%
%	>>s = ExpData('simtrot');	% Use data from simulation
%	% Take the name of the workspace-variable from the model.
%
%	>>s = ExpData({'simtrot', Data}); % Take data from the
%	% argument 'Data' and not using a variable name from the
%	% model. So far only for simulation.
%
%	>>s = ExpData('simtrot', 0.01);	% Use data from simulation
%	% Interpolate the data to use only every 0.01 s.
%
%
% Copyright Christian Ridderström <chr@kth.se>, 2002-2019
