function y = isempty(expObj)
% ExpData/isempty	- Check if the object is empty (contains no signal data)
%
% Fcn:	
% In:	expObj	= ExpData-object
% Out:	y	= 0, the object contains signal data
%		= 1, the object contains no signal data
% Todo:   Add second argument to check for no signal data v.s. no signal defs.
%
% Copyright Christian Ridderström <chr@kth.se>, 2002-2019
  y = isempty(expObj.data);
