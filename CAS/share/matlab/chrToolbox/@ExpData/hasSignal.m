function y = hasSignal(expObj, sigs)
% ExpData/hasSignal	- Checks if the object contains a certain signal
%
% Fcn:	Checks if the object contains certain signals
% In:	expObj	= ExpData-object
% 	sigs	= Cell array with signal names to check for, or
%		  a single signal name (string)
% Out:	y	= 0, is the string 'sigs' is not a signal
%		= 1, if the string 'sigs' is a signal
%		= vector as above
% Note:	This functionality is duplicated in the method isSignal()
%
% Copyright Christian Ridderström <chr@kth.se>, 2002-2019
  if isempty(expObj.signals)
    error('No signals in data!');
  end
  if ischar(sigs), sigs = {sigs}; end
  y = zeros(size(sigs));
  names = {expObj.signals.name};
  for i=1:length(sigs)
    y(i) = any(strcmp(sigs{i}, names));
  end
