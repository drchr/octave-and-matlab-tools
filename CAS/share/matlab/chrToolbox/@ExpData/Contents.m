% ExpData/Contents
%
% This catalog defines the class ExpData and contains these
% files/directories:
%
% char.m		Method, converts to string(s)
% Constants.m		Method, returns values of numerical constants
% Contents.m		Help, this file
% creationTime.m	Method, returns time of creation
% display.m		Method, displays ExpData objects
% double.m		Method, converts to double ??
% end.m			Method, overloads end-indexing
% examples.m		Help, examples
% ExpData.m		Constructor for ExpData objects
% hasSignal.m		Method, checks for signals in the object
% help.m		Help, general help about ExpData
% info.m		Method, accesses the info. structure
% isempty.m		Method, checks if the object contains no data
% isSignal.m		Method, checks for signals in the object
% loadobj.m		Method, overloads loading of ExpData objects
% Members.m		Help about members of ExpData objects
% Methods.m		Help about methods of ExpData objects
% raw.m			Method, return signals as simple matrices
% rows.m		Method, return the n:o rows of signal data
% simpleSignals.m	Method, return simple signal information
% subsref.m		Method, access signals and their components
% timeVector.m		Method, return time signal
% timeVectorDiff.m	Method, return difference of time signal
% translateTime.m	Method, return new object with time translated
% version.m		Method, return version of an ExpData object
% private/		Directory with private methods
%	objver.m	Return version of an ExpData object
%	N2S.m		Convert indices vector to compact string format
%	constants	Create/defines constants (or constants structure)
%

% Copyright (c) 1998-, 2019- by Christian Ridderström <chr@kth.se>
% Dept. of Machine Design, Royal Institute of Technology, Sweden
% $Revision: 1.3 $  $Date: 2003/05/04 23:06:08 $
