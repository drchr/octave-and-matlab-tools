function [y, expObj] = info(expObj, info)
% ExpData/info	- Set .info method of ExpData
%
% Ex:	>>info(expObj)
%	>>[y, expObj] = info(expObj, 'Min information');
% Note:	To set the info, you must reassign the ExpData-object, 'expObj'
%
% Copyright Christian Ridderström <chr@kth.se>, 2002-2019
  if nargin >= 2
    expObj.info = info;
  end;
  if nargout >= 1 | nargin == 1, y = expObj.info; end;
