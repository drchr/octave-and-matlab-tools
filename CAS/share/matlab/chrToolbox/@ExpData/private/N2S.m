function s = N2S(x)
% Convert number to compact string
%
% ----------------------------------------------------------------------
% Fcn:	Convert an integer array to a nicely formatted string
% In:	x	= Array with integers
% Out:	s	= String
% Ex:	x = 1:4	-> s = '1-4'
%	x = 1	-> s = '1'
%
% Copyright Christian Ridderström <chr@kth.se>, 2002-2019
  if iscell(x)
    if length(x) == 0, s = ''; return; end
    s = N2S(x{1});
    for i=2:length(x)
	s = [s ', ' N2S(x{i})];
    end
    return
  end
  if length(x) == 0
    s = ''; return;
  elseif length(x) == 1
    s = sprintf('%d', x); return;
  end
  if all(diff(x) == 1)
    s = sprintf('%d-%d', x(1), x(end)); return;
  else
    s1 = num2str(x);
  end

  s = strrep(s1, '  ', ' ');
  while length(s) ~= length(s1)
    s1 = s;
    s = strrep(s1, '  ', ' ');
  end
  s = deblank(s);

  
