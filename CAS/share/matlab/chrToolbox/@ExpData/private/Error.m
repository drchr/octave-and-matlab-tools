%
% ----------------------------------------------------------------------
% Fcn:	Print nicer error messages
% In.	
% Tip:	Create a local function as follows:
%		function E(varargin), Error(mfilename, varargin{:});
%	and then invoke Error() using E(...)
%
% Copyright Christian Ridderström <chr@kth.se>, 2002-2019
function Error(fileName, msg, varargin)
  switch nargin
   case 0, fileName = '?'; msg = '';
   case 1, msg = '';
  end
  if nargin >= 1
    if ischar(fileName)
      fileName = [fileName '[.m]'];
    else
      fileName = num2str(fileName);
    end;
  end
  fprintf(['Error@%s: ' msg '\n'], fileName, varargin{:});
  if ~isempty(lasterr), fprintf('lasterr = ''%s''\n', lasterr); end;

  if 0
    if isempty(fileName)
      fprintf('Call stack:\n');
    else
      fprintf('Error occured in %s with call stack:\n', fileName);
    end
  end
  fprintf('Call stack:\n');
  C = dbstack;
  for i=2:length(C)
    if length(C(i).name) > 60
      name = [C(i).name(1:5) '... ' C(i).name(end-60:end)];
    else
      name = C(i).name;
    end
    fprintf('  %s#%d\n', name, C(i).line);
  end

  builtin('error', ' ')

  fprintf('This text should never be printed\n');
