% ExpData/private/constants -	Defines numerical constants for ExpData class
%
% Fcn:	Returns numerical constants for use with the ExpData class
% In:	varargin	= Optional arguments, see examples
% Out:	C	= A structure with constants, or numerical value
% 
% The structure of constants is as follows:
%   .status
%      .mode = { 'Empty', 'Simulation', 'Experiment' }
%      .dataSource = { 'None', 'Numerical', 'xPC' }
%
% Ex:	constants;		% Returns entire structure with constants
%	constants('status');	% Returns structure for 'status'
%
% Example of typical use:
% >> persistent C; if ~exists('C', 'var'), C = constants; end;
% >> C.status.mode...
%
% Copyright Christian Ridderström <chr@kth.se>, 2002-2019
function c=constants(varargin)
  persistent C
  if isempty(C)
    C.status.mode = struct('Empty', 		0, ...
                           'Simulation', 	1, ...
                           'Experiment',	2);
    C.status.dataSource = struct('None',	0, ...
                                 'Numerical',	1, ...
                                 'xPC',		2);
  end
  if nargin==0, c=C; else c = getfield(C, varargin{:}); end;
