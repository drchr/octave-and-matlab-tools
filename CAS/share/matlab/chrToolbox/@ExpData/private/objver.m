function ver=objver(obj)
% ExpData/private/objver -	Returns the version of an ExpData-object
%
% Fcn:	Returns the version of an ExpData-object, or the version
%	of the latest version of the class ExpData.
%
% Copyright Christian Ridderström <chr@kth.se>, 2002-2019
  if nargin==0
    ver = 2.1;
  else
    if any(strcmp(fieldnames(obj), 'info')) & ...
          isfield(obj.info, 'objectVersion')
      ver = obj.info.objectVersion;
    elseif any(strcmp(fieldnames(obj), 'objectVersion'))
      ver = obj.objectVersion;
    else
      ver = 0.9;
    end
  end;
