function Warning(fileName, varargin)
%
% ----------------------------------------------------------------------
% Fcn:	Print nicer warning messages
% In:	fileName = String with filename of parent
%	varargin = {formatString, ...}
% Note:	The user is recommended to create a local function as follows:
%	function W(varargin), Warning(mfilename, varargin{:});
% and then invoke the warning using W(...)
%
% Copyright Christian Ridderström <chr@kth.se>, 2002-2019
  fprintf(['ExpData-warning: ', varargin{1}, '\n'], varargin{2:end});
  fprintf('Warning occured in %s.m with call stack:\n', fileName);
  C = dbstack;
  for i=2:length(C)
    if length(C(i).name) > 60
      name = [C(i).name(1:5) '... ' C(i).name(end-60:end)];
    else
      name = C(i).name;
    end
    fprintf('  %s#%d\n', name, C(i).line);
  end
  
