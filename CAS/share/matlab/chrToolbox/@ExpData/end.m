function index=end(expObj, K, N)
% ExpData/end -	Returns end information for overloading end
%
% Fcn:	Overloaded end-operator
% In:	expObj	= ExpData-object
%	K	=
%	N	= 
% Out:	y	= Signal data
% 
% Ex:	raw(e, end)
%
% Copyright Christian Ridderström <chr@kth.se>, 2002-2019
  Log(expObj.status, 3, 'K = %f, N = %f', K, N);
  index = size(expObj.data,1);
  
  
