% ExpData/Methods	This file documents the methods of ExpData-objects
%
% This file contains overall documentation of methods in the ExpData
% class. For other help, do:
%  >> help ExpData		% Creating ExpData objects
%  >> help ExpData/Members	% Members of ExpData
%  >> help ExpData/help		% General help for ExpData
%  >> help ExpData/examples	% Show examples of usage
%
% * Overloaded methods and operators:
%
% char.m		Method, converts to string(s)
% display.m		Method, displays ExpData objects
% struct.m              Method, returns struct with separate signals
% double.m		Method, converts to double, i.e. returns data matrix
% end.m			Method, overloads end-indexing
% isempty.m		Method, checks if the object contains no data
% loadobj.m		Method, overloads loading of ExpData objects
% subsref.m		Method, access signals and their components
%
% * Methods
%
% Constants.m		Defines numerical constants
% creationTime.m	Returns time of creation
% hasSignal.m		Checks for signals in the object
% info.m		Accesses the info. structure
% isempty.m		Checks if the object contains no data
% raw.m			Return signals as simple matrices
% rows.m		Return the n:o rows of signal data
% simpleSignals.m	Return simple signal information
% timeVector.m		Return time signal
% timeVectorDiff.m	Return difference of time signal
% translateTime.m	Return new object with time translated
% version.m		Return version of an ExpData object
% to_struct.m           Return struct with separate signals
%
% Copyright Christian Ridderström <chr@kth.se>, 2002-2019
