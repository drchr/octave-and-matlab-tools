function y=rows(expObj, sig, varargin)
% ExpData/raw -	Returns the number of rows of data
%
% Fcn:	Returns the number of rows of data in the ExpData-object
% In:	expObj	= ExpData-object
% Ex:	>>rows(e);
%
% Copyright Christian Ridderström <chr@kth.se>, 2002-2019
  y = size(expObj.data, 1);
  

  
  
