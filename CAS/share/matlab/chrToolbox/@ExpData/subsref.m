function [d, I] = subsref(expObj, S)
% ExpData/subsref	- Access fields and data
%
% Fcn:	Get signals from an ExpData object
% In:	expObj	= The ExpData object
%	S	= Structure with reference information (see substruct.m)
% Out:	d	= Data values
%	I	= Indices that this signal corresponds to
%
% Examples:
% >> S = expObj.signals;	% Returns signal information
% >> D = expObj.data;		% Returns signal data
% >> D = expObj.FromWS;		% Return data in [t data]-format
%
% Accessing a specific signal 'Sig':
% >> expObj.Sig;		% All occurences and components
% >> expObj.Sig(1:10);		% Columns 1:10 of combined signal data
% >> expObj.Sig(:, 1);		% 1st component of all occurences of Sig
% >> expObj.Sig(2, :);		% All components of 2nd occurence of Sig
%
% Select different times out of the object by calling with curly braces
% >> expObj1 = expObj{1, 2}; % Returns object for time in [1,2]
% >> expObj1 = expObj{ timeVector };	% Returns object with the
%					% timeinstances in the timeVector
%	
% By calling the object with normal paranthesis you select data as if
% it were a "normal vector"
%
% >> expObj1 = expObj( 1:3:100 ); 	% Picks the indices
% >> expObj1 = expObj( expObj.x>a );	% Returns object of the instances
%					% where the relation is true
%
% Accessing data and filtering output:
% >> e.Sig.lp(0.4);		% Low-pass filter the data (dt = 0.4 s)
% >> e.Sig(1,2).lp(0.4);	% Selection and low-pass-filtering
% >> e.Sig.dt;			% Difference of data
% >> e.Sig.dt(0.4);		% Difference with low-pass filter
% You can filter with any filter using ss or tf
% Example: Integration
% >> in=tf([1],[1 0]);
% >> e.Sig.filt(in)
%
% Copyright Christian Ridderström <chr@kth.se>, 2002-2019
  if expObj.status.verbose >= 4
    Log(expObj.status, 'ExpData/subsref(): S(1:%d) = ', length(S));
    for i=1:length(S); disp(S(i)); end;
  end
    
  d = [];
  debug = 0;				% For debugging only
  if debug
    fprintf('ExpData/subsref has been called with:\n');
    for i=1:length(S), disp(S(i)); end;
  end
  
  %
  % Find which signal this is about, or which time instances
  %
  if strcmp(S(1).type, '.')		% '.'-reference
    if strcmp(S(1).subs, 'signals')
      d = expObj.signals;
      return;
    elseif strcmp(S(1).subs, 'FromWS')
      d = [timeVector(expObj) expObj.data];
      return;
    else
     I = findField(expObj, S(1).subs);
    end
  else
    t = timeVector(expObj);
    if strcmp(S(1).type, '{}')		% '{}'-reference
      if length(S(1).subs) == 1
	if islogical(S(1).subs{1}),	% Args: Vector of times
	  error('ExpData/subsref({}) can''t have logical argument');
	else
	  expObj.data = interp1(t, expObj.data, S(1).subs{1});
	end
      elseif length(S(1).subs) == 2,	% Args: starttime, endtime
        expObj.data = expObj.data(S(1).subs{1} <= t & t <= S(1).subs{2},:);
      else
	error('Wrong n:o of arguments to ExpData/subsref({})');
      end
    elseif strcmp(S(1).type, '()')	% '()'-reference
      if length(S(1).subs) == 1		% Logical vector or indices
	expObj.data = expObj.data(S(1).subs{1},:);
      else
	error('Wrong n:o of arguments to ExpData/subsref(())');
      end
    else
      error('Not supported!');
    end
    d = expObj;
    return
  end

  %
  % Do any additional selection of signal components
  %
  if length(S) > 1
    switch S(2).type
     case '()'
      if length(S(2).subs) == 1
	I = cat(2, I{:}); I = {I(S(2).subs{1})};
      elseif length(S(2).subs) == 2
	I = {I{S(2).subs{1}}};
	for i=1:length(I), I{i} = I{i}(S(2).subs{2}); end
      else
	error('Invalid n:o of arguments!');
      end
      SS = S(3:end);
     otherwise SS = S(2:end);
    end    
  else
    SS = S([]);
  end
  
  d = expObj.data(:, cat(2, I{:}));	% Select columns with data
  if debug, fprintf('I = %s\n\n', num2str(cat(2, I{:}))); end;

  %
  %	Handle additional manipulation of signal data
  %
  if length(SS) >= 1, dt = timeVectorDiff(expObj); end;
  
  s=1; while s <= length(SS) & ~isempty(d)
    switch SS(s).type
     case '.'	% Filter the data
      switch SS(s).subs
       case 'dt'
	if s < length(SS)
	  if strcmp(SS(s+1).type, '()')
	    tau = SS(s+1).subs{1};
	    s = s+1;
	  end	    
	else
	  tau=mean(dt);
	end	  
	lp = tf([1 0],[tau 1]);
	for c=1:size(d,2)
	  %time=timeVector(expObj);
	  %n=find(time<=tau+time(1));
	  offset=mean(d(1,c));
	  d(:,c) = lsim(lp, d(:,c)-offset,timeVector(expObj));
	end
       case 'filt'
	if s < length(SS)
	  if strcmp(SS(s+1).type, '()')
	    filter = SS(s+1).subs{1};
	    for c=1:size(d,2)
	      d(:,c) = lsim(filter, d(:,c),timeVector(expObj));
	    end	    
	    s = s+1;
	  end	  
	else
	  error('You must supply filter as argument!')
	end
       case 'lp'
	if s < length(SS)
	  if strcmp(SS(s+1).type, '()')
	    tau = SS(s+1).subs{1};
	    lp = tf(1,[tau 1]);
	    for c=1:size(d,2)
	      d(:,c) = lsim(lp, d(:,c),timeVector(expObj));
	    end	    
	    s = s+1;
	  end	  
	else
	  error('You must supply a time constant argument!')
	end
       otherwise error('Only ''.dt'',''.lp'' and ''.filt'' supported so far');
      end
     otherwise error('Only ''.'' supported so far');
    end
    s = s +1;
  end 
  
%
% ----------------------------------------------------------------------
% Fcn:	Find the fields with the name 'name'
% Out:	I	= List with vectors with column indices to the signals
%  
function I = findField(expObj, name)
  if isempty(expObj.signals), error('No signals in data!'); end
  I = find(strcmp(name, {expObj.signals.name}));
  
  if length(I) == 0
    fprintf('Can''t find signal ''%s'' among the signals:\n',name);
    display(expObj, '-s');
    error(['Reference to unknown signal: ' name]);
  elseif length(I) > 1
    error(sprintf('Found several %s-signals!', name));
  else
    I = expObj.signals(I).I;
  end
    
  
