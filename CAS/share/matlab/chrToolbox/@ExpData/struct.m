function [signals, S] = struct(obj)
% ExpData/struct - Convert ExpData object to struct with signals
%
% Fcn:	Generate struct with signals data
% In:	obj     = An ExpData object
% Out:	signals = Struct with signals as separate fields
%       S       = The ExpData object's internal fields as a struct
%
% Example:
%   % Extract only the signals as a struct
%   signals = struct(expObj);
%
%   % Extract signals struct and the object's struct
%   [signals, S] = struct(expObj);
%
% Copyright Christian Ridderström <chr@kth.se>, 2002-2019
  if nargout == 2
    S = struct('data',           obj.data, ...
               'signals',        obj.signals, ...
               'simple_signals', obj.simpleSignals, ...
               'info',           obj.info, ...
               'status',         obj.status);
  end

  signals = to_struct(obj);
end
