function y = double(expObj)
% ExpData/double	- Convert ExpData object to double
%
% Fcn:	Returns the data-matrix
%
% Copyright Christian Ridderström <chr@kth.se>, 2002-2019

y = expObj.data;
