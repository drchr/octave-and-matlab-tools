function display(expObj, varargin)
% ExpData/display	- Display metcod of ExpData
%
% Fcn:	Displays an ExpData object, i.e. it's fields
% In:	expObj	= The ExpData object
% Opts:	-l, --long	= Long format
%	-s, --signals	= Show signals information
%	
% Copyright Christian Ridderström <chr@kth.se>, 2002-2019
  S = struct('compact', 1, 'long', 0, 'signals', 0, ...
		'time', 0, 'dataSize', 0);
  i=1;
  while i <= length(varargin)
    switch varargin{i}
     case {'-l', '--long'},
      S.compact=0; S.long=1; S.signals=1; S.time=1; S.dataSize=1;
      
     case {'-s', '--signals'},
      S.compact = 0; S.signals = 1; 
      
    end
    i=i+1;      
  end
  
  if S.long
    if isempty(expObj)
      fprintf('%s = ExpData-object (no signal data)\n', inputname(1));
    else
      fprintf('%s = ExpData-object\n', inputname(1));
    end
    fprintf('   model\t= %s\n', num2str(expObj.info.model));
    fprintf('   rate\t\t= %.3g\n', expObj.info.rate);
    fprintf('   created\t= %s\n', datestr(expObj.info.creationTime));
    fprintf('   version\t= %s\n', num2str(expObj.info.modelVersion));
    for l=1:length(expObj.info.general)
      names = fieldnames(expObj.info.general{l});
      fprintf('   info.general(%d):\n', l);
      for ll=1:length(names)
	fprintf('     %s \t= %s\n', names{ll}, ...
		num2str(getfield(expObj.info.general{l}, names{l})));
      end    
    end;
    fprintf('   status:\n'); disp(expObj.status);
    if isempty(expObj), return; end;
  end
  
  if isempty(expObj), t = []; else t = timeVector(expObj); end;

  if S.compact
    if ~strcmp(inputname(1), 'ans'), 
      fprintf('%s = ', inputname(1));
    end
    fprintf('ExpData-object');

    if ~isempty(expObj.info.model)	% Print model name
      fprintf(' from ''%s''', num2str(expObj.info.model));
    end;
  
    if rows(expObj) == 0,		% Make string w/ n:o data
      fprintf(' without data');
    else
      fprintf(' with %d data', rows(expObj));
      if length(t) == 1
	fprintf(' (t=%g)', t);
      elseif length(t) > 1
	fprintf(' (t=%g-%.2g)', t(1), t(end));
      end;
    end

    if length(expObj.signals) > 1,	% Make string with n:o signals
      fprintf(' in signals\n');
      %   Print signals
      fprintf('%s', char(expObj));
    else
      fprintf(' and without signal information\n');
    end;
  end; % S.compact

  if S.time
    fprintf('   Clock\t= %g %s %g [s]\n', t(1), '..', t(end));
  end;
  
  if S.dataSize
    s = size(expObj.data);
    fprintf('   Data size\t= %d x %d\n', s(1), s(2));
  end
  
  if S.long, fprintf('   signals:\n'); end;
  if S.signals
    s = char(expObj, 1);
    fprintf('    %s\n', s{:});
  end
end
