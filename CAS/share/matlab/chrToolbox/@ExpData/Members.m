% ExpData/Members	This file documents the members of ExpData-objects
%
%	Members
% .data		Stores the data
% .signals	Stores signal information about the data
% .model	Stores handle (name) of the model?
% .info		Stores informationstructure about the ExpData-object
% .status	Stores statusinformation about the ExpData-object
% .rate		
%
% Copyright Christian Ridderström <chr@kth.se>, 2002-2019
