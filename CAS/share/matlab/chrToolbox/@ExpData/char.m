function s = char(expObj, doCell)
% ExpData/char	- Convert ExpData object to char
%
% Fcn:	Generate string with field information
% In:	expObj	= An ExpData object
%	doCell	= Optional argument, if true, return cell string array
% Out:	s	= A string with signal information, or if doCell = 1, a
%		  cell string array
%
% Copyright Christian Ridderström <chr@kth.se>, 2002-2019
  if nargin == 2 & doCell
    s = [ print_field, ...
          print_field(expObj.signals) ];
  elseif length(expObj.signals) > 1
    s = [ print_field(), ...
          print_field(expObj.signals(2:end)) ];
    s = [strjoin(s, '\n') sprintf('\n') ];
  else
    s = '';
  end
end


function s = print_field(S, n0Arg)
  if nargin == 2, n0 = num2str(n0Arg); else n0 = '25'; end;
  fmt = ['  %-3s %-' n0 's %s'];
  if nargin == 0 || isempty(S),
    s = { sprintf(fmt(2:end), 'N:o', ' Name', ' Indices') };
  else
    s = {};
    for i=1:length(S)
      s = {s{:}, sprintf(fmt, N2S(length(S(i).I)), S(i).name, N2S(S(i).I))};
    end
  end
end
