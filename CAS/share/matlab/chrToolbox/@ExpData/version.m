function ver=version(obj)
% ExpData/version -	Returns the version of an ExpData-object
%
% Fcn:	Returns the version of an ExpData-object
% Note:	Always requires an argument, otherwise MATLAB
%	cannot call this function.
%
% Copyright Christian Ridderström <chr@kth.se>, 2002-2019
  ver=objver(obj);
