function [t, tCol] = timeVector(expObj)
% ExpData/timeVector	- Returns the time vector
%
% Ex:	>>t = timeVector(e);
%	or
%	>>[t, tCol] = timeVector(e);
%
% Copyright Christian Ridderström <chr@kth.se>, 2002-2019
  if nargin == 0, help ExpData/timeVector; return; end;
  if ~isa(expObj, 'ExpData'), error('Only for ''ExpData''-objects'); end;

  I = strmatch('Clock', {expObj.signals.name});
  if isempty(I)
    Warning('No ''Clock''-signal, time not available!'); tCol = [];
  else
    if length(I)>1, Warning('Several ''Clock''-signals found!'); end;
    tCol = expObj.signals(I(1)).I{1};
  end

  if isempty(tCol), t = []; else t=expObj.data(:,tCol); end;
