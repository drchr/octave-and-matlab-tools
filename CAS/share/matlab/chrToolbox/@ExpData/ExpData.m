function [expObj] = ExpData(mdlOrTg, rate, range)
% ExpData Constructor
%
% Construct ExpData object
% Stx:	ExpData(mdl)
%	>>ExpData(mdl)
% In:	mdlOrTg	= Reference to a model or a XPC-object, or 
%		  a list with two elements: {block/mdl reference, data}
%		  Alternatively a structure exported from dSpace
%
% Ex:	For some examples, execute the command:
%	>>help ExpData/examples
%
% Help:	For more information, execute the following command
%	>>help ExpData/help
%
% Idea:	Change name? It is intimately connected with findSignals... maybe
% use a name related to that? How about:
%	Signals		- ?
%	SignalData	- ?
%	SigData		- ?
%
% I should think about changing the names of the fields now that I have
% the opportunity
%
% Copyright Christian Ridderström <chr@kth.se>, 2002-2019
  
%
% Log:
% 2000-05-30,	chr	Created
% 2000-06-20,	chr	Changed name, started on a new structure
%
  persistent C; if isempty(C), C=constants; end;
  % Create sub-structure with status information
  status0 = struct(...
      'debug', 0, ...			% Debug mode of object
      'verbose', 3, ...			% Verbosity of object
      'mode', C.status.mode.Empty, ... 
      'dataSource', C.status.dataSource.None, ...
      'msg', {{}} ...			% Arbitray list of strings
      );
  
  % Create sub-structure with general object information
  info0 = struct(...
      'model', [], ...			% Name of Simulink block diagram
      'modelVersion', [], ...		% ?
      'comment', [], ...		% Arbitrary additional comments
      'general', {{}}, ...		% Additional structured info.
      'rate', NaN, ...			% Sample rate
      'creationTime', now, ...		% Time of creation
      'objectVersion', objver);		% Version of ExpData-constructor

  %% Warning: Remember to update method struct if changing statement below
  expObj = struct(...			% The elements contains:
      'data', [], ...			% actual data
      'signals', getFields([0 0]), ...	% signal information
      'simpleSignals', [], ...		% ?
      'info', info0, ...		% General information structure
      'status', status0);		% Status information structure

  if expObj.status.verbose >= 4 
      Log(expObj.status, 3, 'Default expObj struct:');display(expObj);
  end
  if nargin==0			% Create emtpy data?
    expObj = class(expObj, 'ExpData');
    return
  elseif isa(mdlOrTg, 'ExpData')	% Copy data
    expObj = class(mdlOrTg, 'ExpData');
    return
  end

  if isstruct(mdlOrTg),
  	if isfield(mdlOrTg,'RTProgram'),
  		expObj=extractDSpaceData(mdlOrTg);
	else

	% Try using the structure as the object
	% Then recursively set the data from mdlOrTg into expObj
            for fieldN = fieldnames(mdlOrTg)',
		field=fieldN{1};
		if isfield(expObj,field),
			expObj=setfield(expObj,field,getfield(mdlOrTg,field));
		else
		   warning(sprintf( ...
		    'The field %s in argument is not a valid field of the object. Ignoring' ...
		   	,field));
		end
            end
        end 
     expObj = class(expObj, 'ExpData');
     return
  end
	
  
  if nargin < 2, rate = 0; end;
  if nargin < 3, range = [-inf, 0]; end;
  
  if iscell(mdlOrTg),
    dataArg = mdlOrTg{2};
    mdlOrTg = mdlOrTg{1};
  else
    dataArg = [];
  end
  
  if isa(mdlOrTg, 'xpc')	% Upload data from target?
    tgS = struct(sync(mdlOrTg));
    tg = mdlOrTg;
    mdl = tgS.Application;
    try
      get_param(mdl, 'name');	% Check if the model is loaded!
    catch
      error(sprintf('Error: Couldn''t find the model ''%s''! Is it open?', mdl));
    end
    
    sampleRate = str2num(tgS.SampleTime);
    dataSize = sscanf(tgS.OutputLog, 'Matrix(%dx%d)');
    if rate == 0
      W('Using lowest possible sample rate: %.3f [s]\n', sampleRate);
      rate = sampleRate;
    end;
    il = rate/sampleRate;
    if il ~= fix(il) | fix(il) < 1
      il = fix(il);
      if il < 1, il = 1; end;
      warning(sprintf('Chosen sample rate not possible, using %.3f [s]', il*sampleRate));
    end
    
    endTime = str2num(tgS.ExecTime);
    startTime = max(0, endTime - dataSize(1)*sampleRate);
    
    startTimeS = max(startTime, endTime + min(range(1), 0));
    endTimeS = max(startTimeS, endTime + min(range(2), 0));
    
    if endTimeS - startTimeS <= 0
      error('Desired time interval is <= 0');
    end
    
    start = fix((startTimeS - startTime)/sampleRate);
    count = (endTimeS - startTimeS)/sampleRate;
    
    expObj.data = xpcgate('getoutp', start, fix(count/il), il);
    expObj.status.mode = C.status.mode.Experiment;
    expObj.status.dataSource = C.status.dataSource.xPC;
    % 
  else			% Use data from simulation
    if isempty(dataArg)
      if strcmp(get_param(mdlOrTg, 'type'), 'block_diagram')
	mdl = mdlOrTg;
      elseif strcmp(get_param(mdlOrTg, 'type'), 'block')
	mdl = bdroot(mdlOrTg);
	W('''%s/%s'' is a block, using the root diagram ''%s'' instead\n', ...
	  get_param(mdlOrTg,'parent'), get_param(mdlOrTg,'name'), ...
	  get_param(mdl, 'name'));
      end
      N = get_param(mdl, 'outputSaveName');
      expObj.data = evalin('base', N, '[]');
      
      if isempty(expObj.data)
	W('Cannot find simulation data in ''%s''\n', N);
      end
    else
      expObj.data = dataArg;
      mdl = mdlOrTg;
    end
    expObj.status.mode = C.status.mode.Simulation;
    expObj.status.dataSource = C.status.dataSource.Numerical;
  end    

  [signals, SS, mdl, tCol] = getFields(size(expObj.data), mdl);
  expObj.signals = signals;
  expObj.simpleSignals = SS;
  expObj.info.rate = rate;
  expObj.info.model = mdl;
  expObj.info.comment = 'New object';
  expObj.info.modelVersion = get_param(expObj.info.model, ...
				       'modelversion');

  if ~isa(mdlOrTg, 'xpc') & ~isempty(expObj.data) & ~isempty(tCol)
    rangeI = [];
    simStart = expObj.data(1,tCol);
    simStop = expObj.data(end, tCol);
    if range(1) < 0
      range = sort(range(1:2));
      tStart = simStop + range(1);
      tStop = simStop + range(2);
      
      rangeI = find(tStart <= expObj.data(:,tCol) & ...
		    expObj.data(:,tCol) <= tStop );
      if length(rangeI) < 2, rangeI = []; end;
    end    
    if ~isempty(rangeI), expObj.data = expObj.data(rangeI,:); end;
  end  
  
  if ~isempty(tCol) & ~isempty(expObj.data) & rate ~= 0
    tout = expObj.data(1,tCol):rate:expObj.data(end,tCol);
    expObj.data = interp1(expObj.data(:,tCol),expObj.data,tout);
  end  

  if expObj.status.verbose >= 4 
      Log(expObj.status, 3, 'Final expObj struct:'); display(expObj);
  end
  expObj = class(expObj, 'ExpData');

%
% ----------------------------------------------------------------------
% Fcn:	Create datastructure describing fields and indices
% In:	dataSize	= Size of the data (row x cols)
%	mdl		= Name of block or cellarray of names
%
function [S0, SS, mdl, tCol] = getFields(dataSize, mdl)
  S0.name = 'data';
  S0.I = {1:dataSize(2)};
  if nargin == 1, return, end;
  
  [S, SS] = findSignals(mdl, '-c');	% SS = SimpleSignals

  [fy, I] = sort({S.name}); S = S(I);	% Sort the names

  I = strmatch('Clock', {S.name});
  if isempty(I),
    W('The signal ''Clock'' was not found!\n');
    tCol = [];
  else
    S(end+1) = S(I(1));
    S(end).name = 't';
    tCol = S(end).I{1};
  end  

  S0 = [S0; S];

  
function W(varargin),
  if isunix,
    Warning(mfilename, varargin{:});
  else
    feval('@ExpData/private/Warning',mfilename,varargin{:});
  end



%
% ----------------------------------------------------------------------
% Fcn:	Extract information from a dspace experiment
% In:	s	=  Structure from dSpace with all the data in
%		   s.Y.Data which have all the timevalues in s.X(1).Data
% Out:  expObj = A structure tha can define the expdata object
% 
function expObj = extractDSpaceData(s),
  status0 = struct(...
      'debug', 0, ...			% Debug mode of object
      'verbose', 3, ...			% Verbosity of object
      'mode', 'Experiment', ...
      'dataSource', 'dSpace', ...
      'msg', {{}} ...			% Arbitray list of strings
      );
  
  % Create sub-structure with general object information
  info0 = struct(...
      'model', s.RTProgram.ObjectFile, ...	% Name of Simulink block diagram
      'modelVersion', [], ...			% ?
      'comment', ['Execution time: ' ...
      		   s.Capture.DateTime], ...	% Arbitrary additional comments
      'general', {{}}, ...			% Additional structured info.
      'rate', s.Capture.SamplingPeriod, ...	% Sample rate
      'creationTime', now, ...			% Time of creation
      'objectVersion', objver);			% Version of ExpData-constructor
  
  expObj = struct(...				% The elements contains:
      'data', [], ...				% actual data
      'signals', getFields([0 0]), ...		% signal information
      'simpleSignals', [], ...			% ?
      'info', info0, ...			% General information structure
      'status', status0);			% Status information structure


  [expObj]=addSignal('Clock',double(s.X.Data'),expObj);
  expObj.signals(end+1).name='t';
  expObj.signals(end).I{1}=1;
  
  e=[];
fieldN={};  % The signal names found so far
fieldwidht=[];  % the number of the signals found so far.  
for i = 1:length(s.Y)
  %fprintf('Signal no: %g\n',i);
  d=s.Y(i);
  name=d.Name;
  n=find(name=='"');
  for j=1:2:length(n),
    token{(j+1)/2}=name(n(j)+1:n(j+1)-1);
  end
  if strmatch('Out',token{end}),
    [ans,tmp]=strtok(token{end},'[]');
    if ~isempty(tmp),
      tmp=strtok(tmp,'[]');
      [tmp,ind]=strtok(tmp,',');
      ind=strtok(ind,',');
      ind=str2num(ind);
      token{end}=[];
    else
      ind=1;
      token{end}=[];
    end
  end
  % Now searching backwards after a name with last token beeing a bracket with number
  for j=length(token):-1:1,
    [nom,rem]=strtok(token{j});
    if ~isempty(rem),
      [width]=strtok(rem,'[] ');
      % check if the name already exist
      nameindex=find(strcmp(nom,fieldN));
      if isempty(nameindex),  % The signal name is new
        fieldN{end+1}=nom;  % Add the name to the list
        fieldindex=length(fieldN); % a local variable telling where to look for this name
        fieldwidth(fieldindex)=0;
      else
        fieldindex=nameindex(1);
      end
      e=setfield(e,nom,{1:length(d.Data),fieldwidth(fieldindex)+1},double(d.Data)');
      fieldwidth(fieldindex)=fieldwidth(fieldindex)+1; % Book the added signal
      break;
    end
  end
end
  
  
  for name=fieldnames(e)',
  	expObj=addSignal(name{1},getfield(e,name{1}),expObj);
  end
% Sort the names
  [ans,I]=sort({expObj.signals(2:end).name});
  expObj.signals(2:end)=expObj.signals(I+1);
  expObj.signals(1).I{1}=1:size(expObj.data,2);

%
% ----------------------------------------------------------------------
% Fcn:	Adds one signal to the structure
% In:	name	=  The name of the signal, so far there is no check that it is
% 		   not already existing
%	x	= the data information for the signal name
%	signals = the signal structure 
%	data	= the data structure
% Out:  expObj = A structure tha can define the expdata object
% 
% 
function [e] = addSignal(name,x,e),
	n=size(e.data,2);
	m=1:size(x,2);
	e.data(:,n+m)=x;
	e.signals(end+1).name=name;
	e.signals(end).I{end+1}=n+m;

