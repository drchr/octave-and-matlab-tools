function [varargout] = findSignals(h0, varargin) 
% findSignals	- Find all signals connected to a block
%
% Use:	[...] = findSignals(h0, opt1, opt2, opt3 ...)
% Fcn:	Recursively finds all signal components and their indices
% In:	h0	= Name(s) of starting block or block diagram
% Doc:	See doc/findSignals.lyx for documentation
% Options:
%    -v		Be verbose (more options means more verbose)
%    -m		Return message (used by block mask functions)
%    -c		Return signal information in composite signal array
%    -p		Print signal information in a nice format
%    +p		Do not print signal information
%    -pc 	Print cache information for block referred to by 'h0'
%    -po | -ps	Print the selected options or the selector arguments
%    -L		Also parse start blocks inside libraries
%    -C 	Cache information abou signals leaving this block
%    -S		Skip checking the first block's name or cache for signals
%    -D S	'S' defines signals when invoked by DefineSignals block
%    -B bus	'bus' is the bus name when invoked by SignalCache block
%    -A altSel	Alternate signal selector
%    {signalSelector}	Cell array arguments are interpreted as signal
%		selector arguments. For the syntax, look in this file
%		just before the function parseSignalSelector(). 
% Out:	There are several alternative output lists. See the examples
%	below, where combinations of the data below is output:
%	  msg	= An appropriate string message
%	  S	= SimpleSignal array with signal information
%	  el	= Elements to select using a selector block
%	  w	= Width of bus, to be used in a selector block
%
% Ex:	Usage in the mask of a SignalCache block
%	[msg] = findSignals(gcbh, '-m', '-C', '-S', '-B', busName);
%
% Ex:	Usage in the mask of a SignalDefinition block
%	[msg] = findSignals(gcbh, '-m', '-C', '-D', signalDefString)
%
% Ex:	Usage in the mask of a SignalSelect block
%	[msg,el,w]=findSignals(gcbh, '-m', '-C' '-S', signalSelectors, ...)
%
% Ex:	Usages in findSignals(), when one the above blocks is encountered
%	[S] = findSignals(gcbh, '-B', busName)
%	[S] = findSignals(gcbh, '-D', signalDefString)
%	[S] = findSignals(gcbh, '-S', signalSelectors, ...)
%
% Ex:	Usage by ExpData  ???? check later ????
%	[S] = findSignals(h)
%
% Note:	For information about data structures, look at the functions:
%	SimpleSignal(), parseSignalSelector() and CompositeSignals()
%
% Copyright Christian Ridderstr�m <chr@kth.se>, 2002-2019
  
% To do:
%	- Check recursing structure of mainLoop, maybe remove some recursions
%	- Make IsLeaf() also return blocktype?
%	- Make the skip signals block work
%	- Create a FromExpData-block (makes animating data easier)
%		- Let define-signals take sig.info from an ExpData object?
%	- Create a ToExpData-block?
%

  if nargin==0, help findSignals; return; end

  % Parse arguments and do pre-checking
  [O, h, sigSel, altSel] = parseArguments(h0, nargout, varargin{:});
  
  S = SimpleSignal;			% Create empty list of signals
  isMdl = strcmpi(get_param(bdroot(h), 'blockDiagramType'), 'model');
    
  for i=1:length(h)			% For each handle...
    dS = [];
    if length(O.defineString)		% Use signal define string?
      [dS, msg] = defineSignals(O, h, O.defineString);
      if O.cache, blockCache(h(i), dS); end;
    elseif isMdl(i) | O.parseLib	% Don't parse block in a library?
      % Optionally check for a bus cache
      if length(O.bus), dS = busCache(bdroot(h(i)), O.bus); end;
      
      % If no bus cache, find signals as usual
      if isempty(dS), dS = mainLoop(O, h(i)); end;
      
      % Optionally cache signal information in block- and bus cache
      if O.cache & length(O.bus),
	Log(O, h(i), 2,'Cache to block- and  bus-cache %s', O.bus);
	busCache(bdroot(h0), O.bus, dS);
	blockCache(h(i), dS);
      end
    else;
      Log(O, h(i), 2, 'Won''t process a block in a library');
    end;
      
    S = [S, dS];			% Append signal information
  end
  
  if O.verbose > 2; showSignals(S, 'before post-processing'); end

  if length(sigSel)  			% Perform post-processing
    [CS, occ, I0, w] = CompositeSignals(S);
    [S, msg, el] = selectSignals(O, h, CS, sigSel, altSel);
    if isempty(el) | ~length(S)		% No signals found?
      if isMdl(i) | O.parseLib, Error(h, 'No signals found!'); end;
    end
    if O.cache, blockCache(h(1), S); end;
  end

  if O.printResult,
    showSignals(S, 'found by findSignals() for ''%s''', FullName(h));
  end;
  if ~length(sigSel) & O.composite,
    if O.composite & nargout == 2, varargout{2} = S; end;
    S = CompositeSignals(S);
  end;
  if nargout == 1,
    if O.returnMsg, varargout = {msg}; else varargout = {S}; end;
  elseif nargout == 2
    if ~O.composite, Error('Invalid options!'); end;
    varargout{1} = S;
  elseif nargout == 3
    if ~length(sigSel), msg = 'No selection!'; el = 1; w=1; end;
    varargout = {msg, el, w};
  end
      
%
% ----------------------------------------------------------------------
% Fcn:	Main loop for finding signals
% In:	O	= Options structure
%	h0	= Handle to a single block being checked
% Out:	S	= SimpleSignal array with signal information
%
function [S] = mainLoop(O, h0)
  O.level = O.level + 1;		% Increase level indicator
  Log(O, h0, 2, 'Enter mainLoop');

  % Check for cache/leaf except the first time if option 'skip' was given
  if O.skip, O.skip=0; S = SimpleSignal; else S=IsLeaf(O, h0); end;
  if length(S),Log(O,h0,2,'Exit mainLoop, leaf/cache found');return;end;
  
  dS = [];				% Preparing for check afterwards
  pc = get_param(h0, 'portConnectivity');
  for i=1:length(pc)
    h = pc(i).SrcBlock;
    if isempty(h), break; end;		% Assumes pc has SrcBlocks first
    if ~ishandle(h), Error(h0,'Unconnected inport(%d)!', i);end; 
    
    type = get_param(h, 'blockType');
    
    dS = IsLeaf(O, h);
    if ~length(dS)			% Not a leaf, recurse downwards
      Log(O, h, 'checking [%s] #signals=%d', type, length(S));
      switch type
	% From does not really require a recurse...
       case 'From',	dS = mainLoop(O, FindGoto(O, h));
       case 'Inport',	dS = DoInport(O, h);
       case 'Mux',	dS = mainLoop(O, h);
       case 'FromWorkspace', dS = fromWorkspace(O, h)
	
       case {'Gain', 'ZeroOrderHold', 'UnitDelay', 'Demux'  'LTI System'},
	dS = mainLoop(O, h);		% Recurse not really necessary...

       case 'SubSystem',
	if ~length(dS)
	  N = num2str(1+pc(i).SrcPort);
	  h = find_system(h, 'followLinks', 'on', 'searchDepth', 1, ...
			  'lookUnderMasks', 'all','blockType','Outport', ...
			  'Port', N);
	  if isempty(h), Error(h0, 'Outport %s not found!', N); end;
	  
	  % Recurse is strictly speaking not necessary here...
	  dS = mainLoop(O, get_param(h, 'handle'));
	end 
	
       otherwise, Error(h, 'Unknown type: ''%s''', type);
      end
    end
    Log(O, h, '=> #signals += %d', length(dS));
    S = [S, dS];
  end
  if ~length(dS), Error(h0, 'This block has no valid connections'); end;
  Log(O, h0, 2, 'Leaving main loop, #signals = %d', length(S));

%
% ----------------------------------------------------------------------
% Fcn:	Find Goto block that corresponds to a given From block
% In:	O	= Options
%	h0	= Handle to From-block
% Out:	h	= Handle to corresponding Goto-block
%
function [h] = FindGoto(O, h0)
  h = h0; tag = get_param(h, 'GotoTag');
  
  while ~isempty(h)			% While GotoBlock not found...
    H = find_system(h, 'followlinks', 'on', 'lookundermasks', 'all', ...
		    'blocktype', 'Goto', 'GotoTag', tag);
    
    if length(H) == 1			% Found exactly one block?
      h = get_param(H{1}, 'handle'); 
      Log(O, h0, 2, 'corresponds to GotoBlock: %s', h);
      return;
    elseif length(H) > 1		% Found too many blocks?
      for l=1:length(H)
	fprintf('GotoBlock: %s\n', FullName(H{l}));
      end 
      Error(h0, 'Too many (%d) GotoBlocks (%s) found', length(H), tag);
    end
      
    h = get_param(h, 'parent');
  end
  Error(h0, 'Goto block for tag ''%s'' not found', tag);

%
% ----------------------------------------------------------------------
% Fcn:	Search an inport for signal information
% In:	O	= Options
%	h0	= Handle to the inport
% Out:	S	= Signal information found.
%
function [S] = DoInport(O, h0)
  h = h0; 				% Handle to the current inport

  while 1				% Ascend hierarchy of subsystems
    Log(O, h0, 'looking at ''%s''s parent', get_param(h, 'name'));
    pc = get_param(get_param(h, 'parent'), 'portconnectivity');
    
    % Get handle to the block that is connected to the parent's inport
    pi = str2num(get_param(h, 'port'));
    h = pc(pi).SrcBlock;
    if isempty(h), Error(h0, 'source of inport not found'); end;
    Log(O, h, 2, 'found as source connected to this inport');
    S = IsLeaf(O, h);			% Check for signal definition
    if length(S), return; end;
    
    type=get_param(h,'blockType'); if ~strcmp(type,'Inport'), break; end;
  end
  
  switch type
   case 'From', h = FindGoto(O, h);
   case 'SubSystem'
    N = num2str(pc(pi).SrcPort+1);
    g = find_system(h, 'followlinks', 'on', 'searchdepth', 1, ...
		    'lookUnderMasks', 'all', 'blocktype', 'Outport', ...
		    'Port', N);
    if isempty(g),
      Error(h, 'Outport %s not found', N, get_param(h, 'name'));
    else
      h = get_param(g, 'handle');
    end
    
   case 'FromWorkspace', S = fromWorkspace(O, h); return;
  end
  
  Log(O, h, 2, 'will continue looking for signals here');
  S = mainLoop(O, h);

%
% ----------------------------------------------------------------------
% Fcn:	Check if a FromWorkspace block exports an ExpData-object
% In:	O	= Options
%	h	= Handle to the FromWorkspace-block
% Out:	S	= Signal information found.
% Note:	The solution is simple, since it just checks if the property
% 'variableName' ends with the string '.FromWS' and then assumes that the
% name of the ExpData-object is whatever is in front of '.FromWS'
%
function [S] = fromWorkspace(O, h)
  vn = get_param(h, 'variableName');
  I = findstr(vn, '.FromWS');
  if isempty(I), Error(h, 'does not use ExpData.FromWS-member!'); end;
  if length(I)>1, Error(h, 'found several .FromWS-strings here!'); end;
  vn = vn(1:I-1);
  if ~evalin('base', sprintf('exist(''%s'',''var'')', vn), '0')
    Error(h, 'unable to find ''%s'' in base workspace!', vn);
  end
  if strcmp(evalin('base', sprintf('class(%s)', vn)), 'ExpData')
    Log(O, h, 'Adding signals from ExpData-object ''%s''', vn);
    S = evalin('base', sprintf('simpleSignals(%s)', vn));
  else
    Error(h, '''%s'' is not an ExpData-object in base workspace!', vn);
  end

%
% ----------------------------------------------------------------------
% Fcn:	Create a 'simple signal' structure
% In:	name	= Name of signal, or a CompositeSignal array
%	width	= Width of signal
% Out:	S	= Simple signal structure, or [] if no arguments given.
% Note:	The structure of a SimpleSignal is as follows:
%	  .name		:string = Name of the signal
%	  .width	:int	= Width of the signal
% Ex:	>>SimpleSignal('aName', 3) =>
%		.name  = 'aName'
%		.width = 3
% 	>>SimpleSignal()	   =>	[]
%	>>SimpleSignal(CS)	- Convert from CompositeSignal array
%
function [S] = SimpleSignal(name, width)
  if nargin >= 2, 
    S = struct('name', name, 'width', width);
  elseif nargin == 1
    CS = name; S = [];
    for i=1:length(CS)
      for j=1:length(CS(i).I)
	S = [S, struct('name', CS(i).name, 'width', length(CS(i).I{j}))];
      end
    end
  else
    S = struct('name', '', 'width', []);
    S=S([]);				% Is this necessary?
  end
  
%
% ----------------------------------------------------------------------
% Fcn:	See if a block is a 'leaf' and if so return signal information
% In:	O	= Options structure
%	h	= Handle to a single block
% Out:	S	= SimpleSignal array with signal information
%
function [S] = IsLeaf(O, h)
  maskType = get_param(h, 'masktype');
  if strncmp(maskType, 'Signals-', 8)
    S = checkCache(O, h);
    if length(S); return; end;
  elseif strcmp(maskType, 'TrunkBus_selector') | ...
        strcmp(maskType, 'LegBus_selector')
    Log(O, h, 1, 'IsLeaf: Trying old find_signals()');
    [w,CS] = find_signals(h, '--look-in-userdata');
    S = [];
    for i=1:length(CS)
      S = [S, struct('name',CS(i).name,'width',length(CS(i).indices))];
    end
    return;
  end
  bt = get_param(h, 'blocktype');
  if strcmp(bt,'S-Function')
    Log(O, h, 2, 'Found FindSignals S-Function');
    S = defineSignals(O, h, [get_param(h, 'FunctionName') ' SFcn']);
    if length(S); return; end;
  end
  
  S = SimpleSignal;             % todo: factor into sep. fcn, handle multiple sigs in name
  name = get_param(h, 'name');
  i = findstr(name, '[');
  if ~isempty(i)
    w = sscanf(name(i(1):end), '[%d]');
    if ~isempty(w)
      S = SimpleSignal(strtok(name), w);
    end
  end
  
%
% ----------------------------------------------------------------------
% Fcn:	Debugging information used to optionally print information
% In:	O	= Options structure, O.verbose decides showing or not
%	h	= Handle to block, whose name and parent is printed
%	varargin= {formatString, ...}
% Ex:	Log(O, h, 'Message %d', 1)
%	Log(O, h, 2, 'Message printed if verbocity >= 2')
%
function Log(O, h, varargin)
  if isempty(O) || ~O.verbose, return; end,
  name = FullName(h);
  
  if ~isnumeric(varargin{1})		% Check for verbocity-level
    i = 1;
  elseif O.verbose < varargin{1}
    return
  else
    i = 2;
  end
  fprintf(['%d@%-25s ' varargin{i} '\n'],O.level,name,varargin{i+1:end});

%
% ----------------------------------------------------------------------
% Fcn:	Print nicer warning messages
% In:	O	= Options structure, only O.level is used
%	h	= Handle to block, whose name and parent is printed
%	varargin= {formatString, ...}
function Warning(O, h, fmt, varargin)
  name = FullName(h);
  fprintf(['Warning: findSignals#%d @ %s: ' fmt '\n'], ...
	  O.level, name, varargin{:});
  fprintf('In %s.m with call stack:\n', mfilename);
  C = dbstack;
  for i=2:length(C)
    if length(C(i).name) > 60
      name = [C(i).name(1:5) '... ' C(i).name(end-60:end)];
    else
      name = C(i).name;
    end
    fprintf('  %s#%d\n', name, C(i).line);
  end
  
%
% ----------------------------------------------------------------------
% Fcn:	Return a string with the name of a block referred to by a handle
% In:	h	= Block handle(s), or invalid handles
% Out:	name	= Full name of the block, i.e. 'parentName/blockName'
function name = FullName(h)
  if ischar(h)
    try, name = [get_param(h, 'parent') '/' get_param(h, 'name')];
    catch, name = ['? ' h ' ?'];
    end;
  elseif ishandle(h)
    name = strcat(get_param(get_param(h, 'parent'), 'name'), '/', ...
		  get_param(h, 'name'));
    if iscell(name)
      strcat(name,','); name = ['{' cat(2,ans{:})]; name(end)='}';
    end
  else
    name = num2str(h);
  end
  name = strrep(name, sprintf('\n'), '\\n');

%
% ----------------------------------------------------------------------
% Fcn:	Local error generating function, to print useful information
%
function Error(h, msg, varargin)
  persistent c
  name = FullName(h);
  fprintf(['Error@findSignals(%s): ' msg '\n'], name, varargin{:});
  fprintf('%s\n', lasterr);
  C = dbstack;
  for i=1:length(C)
    if length(C(i).name) > 60
      name = [C(i).name(1:5) '...' C(i).name(end-60:end)];
    else
      name = C(i).name;
    end
    fprintf('  %s#%d\n', name, C(i).line);
  end;
  error(sprintf(['findSignals(%s): ' msg '\n'], name, varargin{:}));
  
%
% ----------------------------------------------------------------------
% Fcn:	Parse arguments to findSignals()
% In:	h0	= Handle(s) or blockname(s)
%	nout	= N:o output arguments to findSignals
% Out:	O	= Optionsstructure etc
%	h	= Vector with block handles for the blocks that should be
%		  searched for signal information.
%	sigSel	= Signal selector information
%	altSel	= Any possible alternate signal selector
%
function [O, h, sigSel, altSel] = parseArguments(h0, nout, varargin)
  O = struct('verbose',	      0, ...	% Level of verbosity
	     'level', 	      0, ...	% Level of recursion for mainLoop
	     'skip', 	      0, ...	% Skip signal test for 1st block?
	     'parseLib',      0, ...	% Parse start blocks in libraries?
	     'cache', 	      0, ...	% Store info. in cache
	     'bus',	     '', ...	% Name of signal bus to check
	     'defineString', '', ...	% Signal definition string
	     'composite',     0, ...	% Return composite signal info.?
	     'returnMsg',     0, ...	% Return message?
	     'printResult',   nout==0);	% Print results nicely?
  Print = struct('options',   0, 'selectors', 0, 'cache', []);
  h = [];
  sigSel = [];
  altSel = [];

  % Make h0 into an array of block handles
  if ~isnumeric(h0)			% Convert h0 into block handles
    if iscell(h0), get_param(h0, 'handle'); h0 = [cat(1, ans{:})];
    elseif ischar(h0), h0 = get_param(h0, 'handle');
    else error('Invalid argument'); end;
  end
  
  for i=1:length(h0)			% Expand possible model handles
    if strcmp(get_param(h0(i), 'type'), 'block')
      h = [h; h0(i)];
    else
      Log(O, h0(i), 'a block diagram will searched for outports');
      if h0(i) ~= bdroot(h0(i))
	Warning(O, h0(i), 'is not a root block diagram!');
      end
      h = [h; find_system(h0(i),'SearchDepth',1,'BlockType','Outport')];
    end
  end
 
  l = 1; while l <= length(varargin); 	% Parse arguments
    if ischar(varargin{l})
      switch varargin{l}
       case '-c',	O.composite = 1;
       case '-m',	O.returnMsg = 1;
       case '-v',	O.verbose = O.verbose+1;
	if O.verbose == 1,
	  fprintf('Level@block in findSignals(''%s'',..)\n',FullName(h));
	end;
       case '+p',	O.printResult = 0;
       case '-p',	O.printResult = 1;
       case '-po',	Print.options = 1;
       case '-ps',	Print.selectors = 1;
       case '-pc',	Print.cache = 1;
       case '-B',	O.bus = varargin{l+1}; l=l+1;
       case '-C',	O.cache = 1;
       case '-D',	O.defineString = varargin{l+1}; l=l+1;
       case '-S',	O.skip = 1;
       case '-A',	altSel = varargin{l+1}; l=l+1;
       case '-L',	O.parseLib = 1;
       otherwise Error(h, 'option ''%s'' unknown!', varargin{l});
      end
    elseif iscell(varargin{l})		% Is the argument a cell array?
      sigSel = [sigSel; parseSignalSelector(O, h, varargin{l})];
    end
    l = l + 1;
  end
  if length(altSel), altSel = parseSignalSelector(O, h, altSel); end;
  if O.cache & length(h) ~= 1
    error('Option -C invalid with multiple blocks');
  end
  if length(O.defineString) & ~O.cache
    Warning(O,h,'Signal def. string given without using cache option');
  end
  if length(O.defineString) & length(h) ~= 1
    Warning(O,h,'Signal def. string given with multiple blk. arguments');
  end
  if length(sigSel) & length(h) ~= 1
    Warning(O,h,'Signal sel. used with multiple blk. arguments');
  end
  
  Log(O, h, 2, 'Arguments parsed');
  if Print.options, fprintf('Options:\n'); O, end; % Log options?
  if Print.selectors,			% Log parsed signal selectors?
    fprintf('Signal selectors (%d):\n', length(sigSel));
    for k=1:length(sigSel), sigSel(k), end;
  end;
  if Print.cache, [O, h] = printCache(O, h); end; % Log cache?

%
% ----------------------------------------------------------------------
% Fcn:	Parse a signal selector argument
% In:	O	= Options structure
%	h	= Handle to block
%	S	= Cell array with signal selector information
% Out:	c	= Signal selector structure as follows:
%	 .in	= Name of incoming signal
%	 .out	= Name of outgoing signal (i.e. relabelled name)
%	 .occ	= Which occurence of the signal, -1 => all
%	 .I	= Which components of the signal, -1 => all
%
% Examples of selecting signals, renaming or comp. selection not allowed
% - Select all occurences of one, two or several different signals
% 	{'signalName1', 'signalName2', ... 'signalName3'}
% - Select 2nd occurence of one, two or several different signals
% 	{2, 'signalName1', 'signalName2', ... 'signalName3'}
%
% Examples of selecting a single signal and renaming it:
% Renaming all occurences of a single signal, all components selected
% 	{'signalName', -1, 'newSignalName'}
%
% Renaming all occurences of a single signal, some components selected
% 	{'signalName', 1:3, 'newSignalName'}
%
% Renaming fourth occurence of a single signal, all components selected
% 	{4, 'signalName', -1, 'newSignalName'}
%
% Renaming fourth occurence of a single signal, some components selected
% 	{4, 'signalName', 1:4, 'newSignalName'}
%
%
% The argument 'signalSelector' is a cell containing one of the following
% alternative sequences:
%
% Alt 1:	[occurences] signalName [components outName]
% Alt 2:	[occurences] signalName(s) .. signalName(s)
%
% where the optional array 'occurences' indicates what occurences of the
% signal 'signalName' to select.
%
% The optional arguments
%
%	components, outName	 (Note: Note tested yet)
%
% select 'signalName'['components'] to propagate, with the new name
% 'outName'
%
% In the second form, each 'signalName(s)' can be a space-separated
% string with signalnames.
%
% 	signalName(s) = signalName [' ' signalName] ...
%
% In the examples below, first the example argument is given, and then the a
% row vector with the corresponding signals. The real output will of course
% be a row-vector with indices corresponding to these signals.
%
% Ex:	All occurences and components of the signal 'S'
%	{'S'}			=> [S(1,:) .. S(end,:)]
%
% Ex:	All occurences and components of the signals 'S1' and 'S2'
%	{'S1' 'S2'}		=>
%	=> [S1(1,:) S1(2,:) .. S1(end,:), S2(1,:) S2(2,:) .. S2(end,:)]
%
% Ex:	The Lth occurence(s) with all components of 'S1' and 'S2',
%	where L can be a vector (Ex: L = [1 2 3])
%	{L 'S1' 'S2'}		=>
%	=> [S1(1,L) S1(2,L) .. S1(end,L), S2(1,L) S2(2,L) .. S2(end,L)]
%
% Ex:	2nd component of the 1st occurence of the signal 'S' that
%	is renamed to 'T'. 
%	{1 'S' 2 'T'}		=> S(1,2)
%
%	Note that the component argument, just as the occurece
%	argument, may be replaced by a vector.
%
% Ex:	The first three components of the 1st and 2nd occurences,
%	that are renamed as 'T'
%	{[1 2] 'S' [1 2 3] 'T'}	=> [S(1,1:3), S(2,1:3)]
%
% The renaming scheme can also be used to rename signals with an unkown
% number of components.
%
% Ex:	All components of all signals 'S', renamed to 'T'
%	{'S' -1 'T'}		=> [S(1,:) S(2,:) ... S(end,:)]
%  
% It is not quite clear how to propagate signals with multiple
% occurences. For now, I will simply propagate all of them. This means that
% if there were four signals with the name 'S', and there is an outputName
% 'SS', there will also be four signals name 'SS' in the output.  I choose
% this since it is easy to convert four signals into one signal by just
% passing them through for instance a gain block (that has been given the
% new name).
%
function C = parseSignalSelector(O, h, SS)
  c = struct('in','', 'out', [], 'occ', -1, 'I', -1);
  if isempty(SS), C = c([]); return; end;
  Log(O, h, 2, 'Entering parseSignalSelector()');

  % Check for S = {occurence, 'signalName ...', ...}
  if isnumeric(SS{1}), c.occ = SS{1}; S={SS{2:end}}; else S=SS; end;
      
  if isempty(S) | ~ischar(S{1}),Error(h,'No signal in selector');end;
  C = [];
  for k=1:length(S)			% Parse signal name strings
    s = S{k};                           % Let s = selector string
    if ~ischar(s), break; end;		% Break if it is not a string
    s = deblank(s);                     % Remove blanks before or after
    if isempty(s), Error(h,'Empty selector string, no signal name'); end;
    while ~isempty(s)			% Parse selector string
      Log(O, h, 3, 'parsing selector string ''%s''');
      [c.in, s] = strtok(s);		% Store signal name in c.name
      if strcmp(c.in, 'SFcn')		% Is it an S-function reference?
	[c.in, s] = strtok(s);		% Get Sfcn name, prepend inputs
	if strcmp(c.in, '-'),		% Look at block to the right?
	  if length(h)~=1, Error(h,'Multiple blocks not allowed'); end;
	  pc = get_param(h, 'portconnectivity');
	  c.in = get_param(pc(2).DstBlock, 'FunctionName');
	end
	s = deblank([getSFcnInfo(c.in, 'i') ' ' s]);
      else
        Log(O, h, 2, 'c.in = %s', c.in);
        if c.in(end) == ']'
          cMod = c;
          i = findstr(c.in, '[');
          cMod.occ = str2num(c.in(i+1:end-1));
          cMod.in = c.in(1:i-1);
          Log(O, h, 2, '''%s'' mod. to %d, %s', c.in, cMod.occ, cMod.in);
          C = [C; cMod];                % Add signal to list
        else
          C = [C; c];			% Otherwise, add signal to list
        end
      end
    end
  end
  
  % Check for S = {'signalName', components, 'NewSignalName'}
  if isnumeric(s)			% Is it a component argument?
    if length(C) ~= 1,Error(h,'Component requires exactly 1 signal');end;
    if length(S) ~= 3, Error(h, 'Component requires full selector'); end;
    C.I = s;
    if ~ischar(S{3}), Error(h, 'New signal name must be a string'); end;
    if isempty(S{3}), Error(h, 'New signal name cannot be empty'); end;
    C.out = S{3};
  end
  
%
% ----------------------------------------------------------------------
% Fcn:	Check if a block is a 'Signals-...'-block
% In:	O	= Options structure 
%	h	= Handle to block
% Out:	maskType= The block's mask type if it's a signals block,
%		  otherwise ''.
%
% Masktypes			Used by blocks
% --------			--------------
% Signals-selector		Signal selector (manual)
%				Signal selector
% Signals-selector-from		From signal selector (manual)
%				From signal selector
% Signals-cache			Signals Cache
% Signals-define		Define signals
%
function S = checkCache(O, h)
%
% These lines should not be needed any more, since they are tested in
% IsLeaf() instead.
%
%  S = SimpleSignal;
%  if ~strcmp(get_param(h, 'blockType'), 'SubSystem'), return; end
%  maskType = get_param(h, 'masktype');
%  if ~strncmp(maskType, 'Signals-', 8), return; end;

  % Check for cached signal information of this block
  S = blockCache(h);
  if length(S), Log(O, h, 2, 'Found block cache'); return; end;

  Log(O, h, 'There should have been cache information here!');
  Log(O,h,'Trying to set the cache');
  maskType=get_param(h,'masktype');
  switch(maskType)
  case 'Signals-define'
    Log(O,h,get_param(h,'sigDef'));
    get_param(h,'sigDef');  % to remove the leading and trailing '
    findSignals(h,'-C','+p','-D',ans(2:end-1));
    Log(O,h, 'Adding cache for type %s', maskType);
  case 'Signals-cache'
    %get_param(h,'BusName'); % to remove the leading and trailing '
    %findSignals(h,'-p','-C','-S','-B',ans(2:end-1));
    %Log(O,h,['Adding cache to Signals-cache block']);
    Warning(O, h, 'Don''t know how to generate the cache for type %s', ...
            maskType);
  case 'Signals-selector'
    S = mainLoop(setfield(O, 'skip', 1), h);
    Log(O,h, 'Not adding cache for type %s', maskType);
    return
    
  otherwise
    Warning(O, h, 'Don''t know how to generate the cache for type %s', ...
            maskType);
  end
  % Now try again the check for cache information
  S = blockCache(h);
  if length(S), Log(O, h, 2, 'Found block cache'); return; end;
  Error(h, 'Don''t know what to do here');


  %
  % Strange... we should have found a cache, something is wrong:
  % 1. Block h0 has not executed it's mask yet..., or
  % 2. Block h0 could not find any signals...
  %
  % This is perhaps where I should add code to parse other signal
  % blocks, i.e. when findSignals() is not called by their mask.
  %
  
%
% ----------------------------------------------------------------------
% Fcn:	Create SimpleSignal array from a string definition
% In:	O	= Options structure 
%	h	= Handle to block
%	s0	= String defining signals
% Out:	S	= SimpleSignal array with signal information
%	msg	= Message listing the defined signals
%
function [S, msg] = defineSignals(O, h, s0)
  Log(O, h, 'Signals ''defined'' as follows: %s', s0);
  if isempty(s0), Error(h, 'No signals defined!'); end
  S = SimpleSignal; s = s0; msg = '';
  while ~isempty(s)			% Parse signal definition string
    [N, s] = strtok(s);			% Get signal name
    [W, s] = strtok(s);			% Get signal width
    if isempty(W), Error(O, h,'Invalid signal definition ''%s''',s0);end;
    if strcmpi(W, 'SFcn')
      s = deblank([getSFcnInfo(N, 'o') ' ' s]);
      Log(O, h, 'S-function exports ''%s''', s);
    else
      S = [S, SimpleSignal(N, str2num(W))];
      if isempty(msg)
	msg = sprintf('%s[%d]', N, S(end).width);
      else
	msg = [msg sprintf('\n%s[%d]', N, S(end).width)];
      end;
    end
  end
  
%
% ----------------------------------------------------------------------
% Fcn:	Convert an integer array to a nicely formatted string
% In:	x	= Array with integers
% Out:	s	= String
% Ex:	x = 1:4	-> s = '1-4'
%	x = 1	-> s = '1'
%
function s = N2S(x)
  if iscell(x)
    if length(x) == 0, s = ''; return; end
    s = N2S(x{1});
    for i=2:length(x)
	s = [s ', ' N2S(x{i})];
    end
    return
  end
  if length(x) == 0
    s = ''; return;
  elseif length(x) == 1
    s = sprintf('%d', x); return;
  end
  if all(diff(x) == 1)
    s = sprintf('%d-%d', x(1), x(end)); return;
  else
    s1 = num2str(x);
  end

  s = strrep(s1, '  ', ' ');
  while length(s) ~= length(s1)
    s1 = s;
    s = strrep(s1, '  ', ' ');
  end
  s = deblank(s);

%
% ----------------------------------------------------------------------
% Fcn:	Make CompositeSignal array
% In:	S	= Array of SimpleSignals
% Out:	CS	= CompositeSignal array, with structures such as:
%	CompositeSignal
%	  .name		:string	= Name of signal
%	  .I		:cell array with indices of the signal
%	occ	= Array with occurance for the signals
%	I0	= Array with starting indices for the signals
%		  The indices of a signal can be generated as follows:
%			indices = I0(i) + (1:S(i).w)
%	w	= Total width of the signals
%
function [CS, occ, I0, w] = CompositeSignals(S)
  if ~length(S), CS = []; occ = []; I0 = []; w = 0; return; end;
  occ = ones(1, length(S));		% Initialize occurences vector
  I0 = zeros(1, length(S));		% Initialize I0 vector
  for i=2:length(S)			% Update the occurence-fields
    I0(i) = I0(i-1) + S(i-1).width;
    occ(i) = 1 + sum(strcmp(S(i).name, {S(1:i-1).name}));
  end
  w = I0(end) + S(end).width;		% Total width of signals

  CS = [];				
  for i = 1:length(S)			% Create compact signal array
    if occ(i) == 1
      CS = [CS; struct('name',S(i).name,'I',{{I0(i)+(1:S(i).width)}})];
    else
      I = strcmp(S(i).name, {CS.name});
      CS(I).I = {CS(I).I{:}, I0(i)+(1:S(i).width)};
    end
  end

%
% ----------------------------------------------------------------------
% Fcn:	Select desired signals from a CompositeSignal array
% In:	O	= Options structure
%	h	= Handle to blocks (in case of error messages)
%	CS	= CompositeSignal array
%	SS	= Signal selector array
%	AS	= Alternate signal selector
% Out:	S	= Selected CompositeSignals, if no signal selectors are
%		  given, the signals are sorted alphabetically
%	msg	= String about outgoing selected signals
% 			[newName=]name<occ>(comp)
%	el	= Elements of incoming signals to be selected
%
function [S, msg, el] = selectSignals(O, h, CS, SS, AS);
  S = []; msg = ''; el = [];
  % Code that can be used to sort signals
  % if ~isempty(CS), [ns, I] = sort({CS.name}); S = CS(I); end;
  Log(O, h, 'Selecting signals');
  
  if isempty(CS), msg = 'No signals!'; return; end;
  for i=1:length(SS)
    I = strcmp(SS(i).in, {CS.name});
    if sum(I) ~= 1,
      if isempty(AS)
	Error(h,'Problem finding ''%s''.',SS(i).in);
      else
	[S, msg, el] = selectSignals(O, h, CS, AS, []);
	return;
      end
    end;
    cs = CS(I);				% Select signal

    if isempty(SS(i).out),
      s=SS(i).in;
    else
      s=[SS(i).out '=' SS(i).in];
      cs.name = SS(i).out;		% Rename outgoing signal
    end;
    if SS(i).occ ~= -1
      if max(SS(i).occ) > length(cs.I);
	if isempty(AS)
	  Error(h,'Can''t find desired occurence of %s', SS(i).in);
	else
	  [S, msg, el] = selectSignals(O, h, CS, AS, []);
	  return;
	end
      end
      cs.I = {cs.I{SS(i).occ}};
      s = [s '<' N2S(SS(i).occ) '>'];
    else
      s = [s '<' N2S(1:length(cs.I)) '>'];
    end 

    if SS(i).I ~= -1
      s=[s sprintf('(%s)',N2S(SS(i).I))];
      for j=1:length(cs.I)
	if max(SS(i).I) > length(cs.I{j})
	  if isempty(AS)
	    Error(h,'Can''t find desired components of %s', SS(i).in);
	  else
	    [S, msg, el] = selectSignals(O, h, CS, AS, []);
	    return;
	  end
	end
	cs.I{j}; cs.I{j} = ans(SS(i).I);
      end
    end;
    cat(2, cs.I); el = [el, ans{:}];

    S = [S cs];				% Add signal to output signals
    % Add string describing of the selected signal
    if i == 1, msg = s; else msg = [msg '\n' s]; end;
  end
  
  Log(O, h, 2, 'Signals selected');
  
  % Convert data into a format usable directly by a selector block
  if isempty(el)
    msg = ['-' N2S(occ) '-'];
    if isempty(AS)
      Warning(O, h, 'Signals not found!\n');
    else
      [S, msg, el] = selectSignals(O, h, CS, AS, []);
      return;
    end
  end
  
  S = SimpleSignal(S);			% Convert to SimpleSignal array

%
% ----------------------------------------------------------------------
% Fcn:	Log signal information
% In:	S	= Array of SimpleSignals
%	hdr	= Header string to show before list of signals:
% Out:	-
%
function showSignals(S, hdr, varargin)
  if length(S)
    fprintf(['Signals ' hdr], varargin{:});
  else
    fprintf(['No signal information ' hdr], varargin{:}); return;
  end;
  [CS,occ,I0,w] = CompositeSignals(S);	% Convert S to composite signals
  fprintf(' (width=%d)\n', w);

  csFlag = 1;				% Log composite signal data
  n0 = '25';				% Allocated space for signal name
  if csFlag
    fs = ['  %-3s %-' n0 's %s\n'];
    fprintf(fs(2:end), 'N:o', ' Name', ' Indices');
    for i=1:length(CS)
      fprintf(fs, N2S(length(CS(i).I)), CS(i).name, N2S(CS(i).I));
    end
  else
    fs = ['  %-' n0 's %-5s %-4s %-4s\n'];
    fprintf(fs(3:end), '  Name', '  Width', '  I0', '  Occ.');
    for i=1:length(S)
      fprintf(fs, S(i).name, N2S(S(i).width), N2S(I0(i)), N2S(occ(i)));
    end
  end

%
% ----------------------------------------------------------------------
% Fcn:	Log signal information from the cache
% In:	S	= Array of SimpleSignals
%	hdr	= Header string to show before list of signals:
% Out:	-
%
function [O, h] = printCache(O, h)
  if length(h)
    for i=1:length(h)
      name = removeNewLine(get_param(h(i), 'name'));
      showSignals(blockCache(h(i)), 'in block cache of %s', name);
      if length(O.bus),
	H = bdroot(h(i));
	name = removeNewLine(get_param(H, 'name'));
	showSignals(busCache(H,O.bus),'in bus cache %s::%s',name,O.bus);
      end
    end
  else
    empty = 1; C = blockCache;
    for i=1:length(C.h)
      if ishandle(C.h(i))
	empty = 0;
	showSignals(C.info{i}, 'in block cache of %s/%s', ...
		    removeNewLine(get_param(C.h(i),'parent')), ...
		    removeNewLine(get_param(C.h(i),'name')));
      end
    end
    if empty, fprintf('The block cache is empty.\n'); end;
    empty = 1; C = busCache;
    for i=1:length(C.h)
      if ishandle(C.h(i))
	empty = 0;
	showSignals(C.info{i}, 'in bus cache %s::%s', ...
		    removeNewLine(get_param(C.h(i), 'name')),C.name{i});
      end
    end
    if empty, fprintf('The bus cache is empty.\n'); end;
  end
  
  h=[]; O.printResult = 0;    
  
function name = removeNewLine(s)
  name = strrep(s, sprintf('\n'), '\n');
   
%
% ----------------------------------------------------------------------
% Functionality of cache for signal information
% Use:	Clear block- and bus-cache
%	>>clear findSignals
%
%     - Clean cache (delete unneccessary entries)
%	>>blockCache([]); busCache([]);
%
%     -	Store signal information 'S' in
%	>>blockCache(h, S);		% block with handle 'h'
%	>>busCache(mdlH, bus, S);	% bus 'bus' for model 'mdlH'
%
%     -	Retrieve signal information from...
%	>>S = blockCache(h);		% block with handle 'h'
%	>>S = busCache(mdlH, bus);	% bus 'bus' for model 'mdlH'
% 
%     -	Retrieve entire cache (for debugging purposes)
%	>>{blockCache, busCache};
%
% Structure of the bus cache
%	.h	= [mdlHandle1, modelHandle2, ... mdlHandleN]
%	.name	= {busName1,   busName2,     ... busNameN}
%	.info	= {busInfo1,   busInfo2,     ... busInfoN}
%
% Structure of the block cache
%	.h	= [blkHandle1, blkHandle2, ... blkHandleN] 
%	.info	= {blkInfo1,   blkInfo2,   ... blkInfoN}
%
function out = busCache(h, name, varargin)
  persistent C;			

  if isempty(C), C = struct('h', [], 'name', {{}}, 'info', {{}}); end

  if nargin==0, out=C; return; end;	% Return entire cache?

  if isempty(h)				% Clean out invalid handles?
    I = logical(ishandle(C.h));
    C = struct('h',C.h(I), 'name',{{C.name{I}}}, 'info',{{C.info{I}}});
    return
  end

  if ~isempty(C.h)			% Find index to cache elements
    II = find(C.h == h);		% Find matching model handles
    if ~isempty(II)
      I = strmatch(name, {C.name{II}}, 'exact');
      I = II(I);			% Find matching bus name
    else
      I = [];
    end
  else
    I = [];				% No matches since cache is empty
  end
  if length(I) > 1, error('Multiple cache-hits!'); end;

  if isempty(I)				% No cache hit?
    if nargin == 3			% Store info?
      C.h = [C.h, h];			% Add entry (inefficient code...)
      C.name = {C.name{:}, name};
      C.info = {C.info{:}, varargin{1}};
      out = 1;				% We have added an entry
    else
      out = [];				% Return empty, for an entry
    end					% that does not exist
  else					% We have a cache hit
    if nargin == 3			% Store info?
      C.info{I} = varargin{1};
      out = 0;				% Stored in an existing entry
    else				% Retrieve existing info
      out = C.info{I};
    end
  end
%
%
function out = blockCache(h, varargin)
  persistent C;				
  if isempty(C), C=struct('h', [], 'info', {{}}); end;

  if nargin==0, out=C; return; end;	% Return entire cache?
  if isempty(h)				% Clean out invalid handles?
    if isempty(C.h), return; end;
    I = ishandle(C.h);
    C = struct('h', C.h(I), 'info', {{C.info{I}}});
    return
  end

  % Find matching entry
  if isempty(C.h), I = []; else I = find(C.h == h); end;
  if length(I) > 1, error('Multiple cache-hits!'); end;

  if isempty(I)				% Does entry not exist?
    if nargin == 2			% Store info?
      C.h = [C.h, h];			% Add entry (inefficient code...)
      C.info = {C.info{:}, varargin{1}};
      out = 1;				% We have stored in a new entry
    else
      out = [];				% Return empty, for an entry
    end					% that does not exist
  else
    if nargin == 2			% Store info?
      C.info{I} = varargin{1};
      out = 0;				% We have stored in an existing entry
    else				% Retrieve existing info
      out = C.info{I};
    end
  end
