function [flag] = chrMake(varargin)
%chrMake	- Make functionality for S-functions in Simulink models
%
% Stx:	chrMake(opts1, ..., optsN, model, srcDirs, mexDir);
% Fcn:	Make-file to automatically compile S-functions.
% In:	opts..	= Options, see below
%	model	= Name of model as a string.
%	srcDirs	= Directories where source files can be found
%	mexDir	= Directory where compiled mex-files should be stored.
%		  If this argument is not given or given as 'auto',
%		  the default is selected as:
%			<source-path>/../mex
%
% Note:	If 'srcDirs' has a subcatalog named 'src', it is automatically
%	added to the list of source directories.
%
% Options:
%   -f			Function compile of single file
%   -a			Force a compile of ALL .C-files
%   -s			Silent when compiling.
%   -v			Verbose, more -v means more verbose
%   -q			Quiet, reduce messages printed
%   -n			No action, i.e. only show commands.
%   -M			Compile standard MATLAB .mex, not S-Functions
%
%   --outdir mexDir	Where to put .mex-files. If not given, .mex-files
%     or  -O mexDir	are placed in '<1st src-dir>../mex'
%
%   --model-dir		Automatically determine source and destination 
%			using the .m-function 'model_dir'
%   --show-opts		Show options selected.
%   --no-warnings	Don't print warnings.
%   --no-block-list	Don't list blocks referring to modules
%			that could not be found.
%   --no-found-mex	No warnings about .mex-files found that has no
%			corresponding source file.
%   --no-M-file-warning	Don't warn about .m-files thar are used as S-fcn.
%   -wIPath	No warnings about missing include paths
%
% Note: The examples below assume a UNIX-style filepath, use \ on PC
% Example 1 --- To only compile source that is more recent
%	>>chrMake quadmdl ../../models/leggedrobots/src --output ../mex
%
% Example 2 --- To force compilation of all source files
%	>>chrMake -a quadmdl ../../share/models/leggedrobots/src
%
% Example 3 --- To see what needs to be done, but don't do anything.
%	>>chrMake -n quadmdl ../../share/models/leggedrobots/src
%
% Example 4 --- Compile S-functions from Sophia needed in a .mdl-file
%	>>chrMake --model-dir quadmdl
%	(i.e. the function model_dir.m is used to get source path)
%
% Example 5 --- Compile normal .mex-functions
%	>>chrMake -M ./src --output ./mex
%
% Copyright Christian Ridderström <chr@kth.se>, 2002-2019
  
  if nargin == 0, help chrMake; return; end;

  % Parse arguments and determine source and mex-directories
  [O, srcDirs] = parseArguments(varargin{:});

  if O.SFcnMode
    % Find S-functions, i.e. S-function modules that may have source code
    SFcnModules = findSFcnModules(O);
  
    % Find corresponding C-functions and modules without source code
    [C_files, notFound] = findCFiles(O, SFcnModules, srcDirs);
  
    % Optionally show the list of S-functions without source code
    showNotFound(O, notFound);
  elseif isempty(O.singleTarget)
    [C_files, notFound] = findAllCFiles(O, srcDirs);
  else
    C_files = {O.singleTarget};
  end
  
  % Make S-functions with more recent source code
  flag = makeList(O, C_files);
  
%  
% ----------------------------------------------------------------------
% Fcn: Create the options structure
%
function [O, srcDirs] = parseArguments(varargin)
  O = struct('SFcnMode',	1, ...
	     'showCommands',	1, ...
	     'autoCompile',	1, ...
	     'do_compile',	1', ...
	     'verbose',	0, ...
	     'quiet',	0, ...
	     'showOpts',	0, ...
	     'warnings', 	struct('foundMexFile', 1, ...
				       'moduleNotFound', 1, ...
				       'noIncludePath', 1, ...
				       'foundMFile', 1), ...
	     'listBlocksWOSrc', 1, ...
	     'singleTarget', '', ...
	     'model', 	NaN, ...
	     'use_model_dir', 0, ...
	     'mexDir', 	NaN, ...% .mex-file output directory
	     'mexOpts', 	{{'-O'}}, ...
	     'include_dir', 1, ...
	     'incDirs', 	{{}});

  args = {};
  i = 1; while i <= length(varargin)
    switch varargin{i}
     case '-n', O.do_compile = 0;	% No action
     case '-v', O.verbose = O.verbose+1; O.showOpts = 1;
     case '-s', O.showCommands = 0;
     case '-a', O.autoCompile = 0;
     case '-f', O.singleTarget = '';
     case '-q', O.quiet = 1;
     case '-M', O.SFcnMode = 0;
     case '--show-opts', O.showOpts = 1;
     case '--no-M-file-warning', O.warnings.foundMFile = 0;
     case '--no-warnings',
      O.warnings.foundMFile = 0;
      O.warnings.foundMexFile = 0;
      O.moduleNotFound = 0;
     case '--no-block-list', O.listBlocksWOSrc = 0;
     case '--no-found-mex', O.warnings.foundMexFile = 0; 
     case '--no-include_dir', O.include_dir = 0;
     case '-wIPath', O.warnings.noIncludePath = 0;
     case '--model-dir',    O.use_model_dir = 1;
     case {'--outdir','-O'}, O.mexDir = varargin{i+1}; i=i+1;
     otherwise
      args = {args{:}, varargin{i}};
    end   
    i = i+1;
  end

  try
    if O.include_dir, O.incDirs = include_dir('inc'); end;
  catch
    if O.warnings.noIncludePath
      fprintf(['Failed to set include path. If necessary, create the'...
	       ' file ''include_dir.m'' or adjust with options!\n']);
    end
  end
  
  if length(args) == 0, error('Not enough arguments given!'); end
  if exist(args{1}) == 4,		% Does .mdl-file exist?
    O.model = args{1};
  else
    if O.SFcnMode, 
      fprintf('No ''%s.mdl'', assuming option ''-M''!\n', args{1});
      O.SFcnMode = 0;
    end
  end

  if O.SFcnMode				% Making S-Functions?
    if length(args) == 1		% 'make model'
      if ~O.use_model_dir & ~O.quiet
	fprintf('chrMake(): Assuming option ''--model-dir''\n');
      end
      srcDirs = model_dir('src');
      if ~iscell(srcDirs), srcDirs = {srcDirs}; end
      O.mexDir = model_dir('mex');
    else			% 'make model srcdir'
      srcDirs = {args{2:end}};
    end
  else					% Making MATLAB .mex-files
    if length(args) == 1 & exist(args{1},'file')
      O.singleTarget = args{1};
      srcDirs = {fileparts(args{1})};	% Extract dir
      O.mexDir = srcDirs{1};
    end
    if isnan(O.mexDir), O.mexDir = args{end}; end;
    if length(args) == 1 & ~exist(args{1},'file')% 'make dir'
      srcDirs = {args{1}};
      if ~O.quiet,
	fprintf('Using ''%s'' as source directory and\n', args{1}); 
	fprintf('using ''%s'' as destination directory.\n', O.mexDir); 
      end
    else				% 'make src1 ... srcN mexDir'
      srcDirs = {args{1:end-2}};
    end  
  end

  for i=1:length(srcDirs)
    if ~exist(srcDirs{i}, 'dir')
      error(sprintf('Source directory ''%s'' not there!', srcDirs{i}));
    end
  end

  if isnan(O.mexDir)			% Auto-find mexDir?
    O.mexDir = fullfile(srcDirs{1}, '..', 'mex');
    if ~O.quiet
      fprintf('''Mex''-dir automatically selected to ''%s''\n', ...
	      O.mexDir);
    end
  end  
  
  if ~exist(O.mexDir, 'dir')		% Check that mexdir exist
    error(sprintf('The mex-directory ''%s'' does not exist!', O.mexDir));
  end
  
  if O.showOpts			% Maybe show options etc
    fprintf('Source directories: \n'); fprintf('%s\n', srcDirs{:});
    fprintf('Options: \n');  disp(O);
  end;

%
% ----------------------------------------------------------------------
% Fcn:	Finds .c-files in srcDirs
% In:	lst	= List of S-Function modules
%	src	= Path to directories containing source code
% Out:	files	= List of .mex/.m-files that the models call.
%		  Each name only occurs once.
%	notFound = List of modules not found.
% Note:	The system is loaded but not showed.
%
function [files, notFound] = findAllCFiles(O, srcDirs)
  files = {}; notFound = {};
  for i=1:length(srcDirs)		% Find all .c-files
    D = dir(fullfile(srcDirs{i}, '*.c'));
    for j=1:length(D)
      if IsValidSrcFile(O, fullfile(srcDirs{i}, D(j).name))
	files = {files{:}, fullfile(srcDirs{i}, D(j).name)};
      end
    end
  end;
  
% ----------------------------------------------------------------------
% Fcn:	Checks if a file is an S-Function file
% In:	file	= Path to file
% Out:	y	= If it's not an S-Function file
% Note:	
%
function y = IsValidSrcFile(O, file)
  SFcn = 0;
  y = 0;
  fid = fopen(file, 'rt');
  if fid == -1, error(sprintf('Unable to open ''%s''!', file)); end;
  for c=1:100
    line = fgets(fid);
    if ~ischar(line), break; end;
    if strncmpi(line, '#DEFINE S_FUNCTION_NAME', 23), SFcn=1; break; end;
  end
  fclose(fid);
  if O.SFcnMode, y = SFcn; else y = ~SFcn; end;
  Log(O,2, 'IsValidSrcFile(''%s'') -> %d (Is SFcn=%d)', file, y, SFcn);

%
% ----------------------------------------------------------------------
% Fcn:	Goes through a list of files and checks if it is a compilable
%	S-function 
% In:	lst	= List of S-Function modules
%	src	= Path to directories containing source code
% Out:	files	= List of .mex/.m-files that the models call.
%		  Each name only occurs once.
%	notFound = List of modules not found.
% Note:	The system is loaded but not showed.
%
function [files, notFound] = findCFiles(opts, lst, srcDirsArg)
  files = {}; notFound = {};
  if ~iscell(srcDirsArg), dirs = {srcDirsArg}; else dirs=srcDirsArg;end;
  Log(opts, 2, 'findCFiles(): Entering');
  %
  % First check if there is a corresponding .c-file in any of the source
  % directories 
  %
  name = '';
  for i=1:length(lst)
    for j=1:length(dirs)
      name = fullfile(dirs{j}, [lst{i}, '.c']);
      if exist(name, 'file') == 2,
        files = {files{:}, name}; name = 'found'; break;
      end;
    end;
    %
    % If no file has been found, see if a .m-file can be found.
    %
    if ~strcmp(name, 'found'),
      switch exist(lst{i})
      case {0}, notFound = {notFound{:}, lst{i}};
      case {2},
        if opts.warnings.foundMFile
          fprintf('Warning: Using %s.m as MEX-file\n', lst{i});
        end;        
      case {3},
        if opts.warnings.foundMexFile
          fprintf('Warning: Found ''%s''.mex-file, but can''t find the source.\n', lst{i});
        end;
      otherwise, error('Unknown file type, the module ''%s''could be a variabl\n', lst{i});
      end;      
    end;      
  end;

  if opts.warnings.moduleNotFound & (length(notFound) > 0)
    fprintf('Warning: Can''t find module ''%s''\n', notFound{:});
  end;

%
% ----------------------------------------------------------------------
% Fcn:	Diagnostic function that optionally shows warning messages
% In:	notFound	- String array of the function modules not found.
%
function showNotFound(opts, notFound)
  Log(opts, 'showNotFound(): Entering');
  if opts.listBlocksWOSrc == 0, return; end;
  for i=1:length(notFound)
    fprintf('Blocks refering to: %s\n', notFound{i});
    blocks = find_system(...
	opts.model, 'FollowLinks', 'on', 'LookUnderMasks', 'on', ...
	'BlockType', 'S-Function', 'FunctionName', notFound{i});
    find_system(opts.model, 'FollowLinks', 'on', 'LookUnderMasks', 'on', ...
		'BlockType', 'S-Function', 'SFunctionModule', notFound{i});
    blocks = {blocks{:} ans{:}};
    fprintf('\t%s\n', blocks{:});
  end;
  
%
%  ----------------------------------------------------------------------
% Fcn:	Search through a model and return all S-functions that are used.
% In:	mdl	= Name of model
% Out:	files	= List of .mex/.m-files that the models call.
%		  Each name only occurs once.
% Note:	The system is loaded but not showed.
%
function files = findSFcnModules(opts)
  Log(opts, 'findSFcnModules(): Entering');
  load_system(opts.model);
  blocks = find_system(opts.model, 'FollowLinks', 'on', ...
		       'LookUnderMasks', 'on', 'BlockType', 'S-Function');
  files = {};
  for i=1:size(blocks,1)
    files = {files{:} ...
        get_param(blocks{i},'FunctionName') ...
        get_param(blocks{1},'SFunctionModules')};
  end;
  % Remove empty-string
  files = setdiff(unique(files), {''''''});
  if opts.verbose
    fprintf('Found references to the following files:\n');
    fprintf('%s\n', files{:});
  end
%  
% Fcn:	Searches using a fileMask and returns the names of all files
% Out:	flag	= 1	Allt gick bra
%		= -1	No source files found
%		= -2	Source directory not found
%		= -3	A compile error (shown on screen) occured
%
function [flag] = makeMask(fileMask, targDir, opts)
  Log(opts, 'makeMask(): Entering');
  flag = 0;		% Code indicating no files found

  D = dir(fileMask);
  if size(D,1) < 1, return; end;

  srcDir = fileparts(fileMask);
  if exist(srcDir, 'dir') ~= 7; flag = -2; return; end;
  if nargin == 1, targDir = srcDir; end;

  for i=1:size(D,1)
    srcFile = fullfile(srcDir, D(i).name);
    flag = maybeMexFile(srcFile, targDir, opts);
    if ischar(flag);
      disp(flag);
      flag = -3;
      return;
    end;
  end
  
%  
% Fcn:	Searches using a fileMask and returns the names of all files
% Out:	flag	= 1	Allt gick bra
%
%		= "Felmeddelande"
%		= -1	No source files found
%		= -2	Source directory not found
%		= -3	A compile error (shown on screen) occured
%
function [flag] = makeList(opts, srcFiles)
  Log(opts, 'makeList(): Entering');
  flag = 0;		% Code indicating no files found

  for i=1:length(srcFiles)
    flag = maybeMexFile(opts, srcFiles{i});
    if ischar(flag);
      disp(flag);
      flag = -3;
      return;
    end;
  end
  
%
% ----------------------------------------------------------------------
%	date = getDate(file)
% Fcn:	Returns a 'DATENUM' with the date of 'file'
% In:	file	= Path and filename
% Out:	date	= DATENUM of the file if it exists,
%		= -1 otherwise
% Ex:	>>getDate('../src/hej.c')
%
function [date] = getDate(file)
  date = -1;
  [dirPath,name,e] = fileparts(file);
  name = [name, e];
  D = dir(dirPath);
  for i =1:size(D,1)
    if strcmp(D(i).name, name),
      if isfield(D, 'datenum')
        date = D(i).datenum;
      else                      % For very old MATLAB releases
        date = datenum(D(i).date);
      end
      return
    end
  end

  %
  % Since DOS/Windows don't differentiate between 'A' and 'a'
  % do one more check w/o case sensitivity
  %
  if isunix, return; end;

  for i =1:size(D,1)
    if strcmpi(D(i).name, name),
      if isfield(D, 'datenum')
        date = D(i).datenum;
      else                      % For very old MATLAB releases
        date = datenum(D(i).date);
      end
      return
    end
  end

%
% ----------------------------------------------------------------------
%	flag = isUpToDate(targFile, srcFile)
% Fcn:	Checks if a 'targFile' is newer than 'srcFile'
% In:	opts	= Options structure
%	srcFile	= Filename for sourcefile (w/ extension and path)
% Out:	flag	= 0,  if 'targFile' is older or does not exist.
%		= 1,  if 'targFile' is newer than 'srcFile'
%		= -1, if 'srcFile' does not exist.
%
% Ex:	>>isUpToDate('../mex/target.mexsol', '../src/target.c')
%
function [flag] = isUpToDate(opts, srcFile)
  if ~exist(srcFile, 'file'), flag = -1; return; end

  [p,n,e] = fileparts(srcFile);
  targFile = [fullfile(opts.mexDir, n) '.' mexext];
  
  flag = -1;
  srcDate = getDate(srcFile);
  if srcDate < 0, return; end;		% Check that 'srcFile' exists

  targDate = getDate(targFile);
  flag = 0;
  if targDate < 0, return; end;		% Check that 'targFile' exists

  flag = targDate >= srcDate;		% Compare dates

%
% ----------------------------------------------------------------------
%	flag = maybeMexFile(opts, srcFile)
% Fcn:	Only compiles if the target file is too old (or non-existant)
% In:	srcFile	= Source file (w/ extension and path)
%	opts	= Structure with options
% Out:	flag	= 1, If everthing worked.
%		= "Felmeddelande"
%		= -1, if 'srcFile' does not exist'
%		= -2, compile error
%
function [flag] = maybeMexFile(opts, srcFile)
  Log(opts, 'maybeMexFile(''%s''): Entering', srcFile);
  if exist(srcFile) ~= 2; flag = -1; end;
  if opts.autoCompile
    flag = isUpToDate(opts, srcFile);
  else
    flag = 0;
  end;
  
  switch  flag
   case -1,
    flag = sprintf('The file %s does not exist', srcFile);
   case 0,		% 'targFile' is too old
    if ~isempty(opts.incDirs),
      incDirsF = sprintf('-I%s ', opts.incDirs{:});
    else
      incDirsF = '';
    end;      
    s = ['mex ', ...
	 sprintf('%s ',   opts.mexOpts{:}) ...
	 incDirsF ...
	 sprintf('-outdir %s %s', opts.mexDir, srcFile)];
    if opts.showCommands,
      if opts.do_compile, fprintf('Executing: %s\n', s);
      else fprintf('Not executing: %s\n', s);
      end;
    end;
      
    flag = 1;
    if opts.do_compile
      try
        eval(s);
      catch ME
        flag = sprintf('# Build error using:\n#\t%s\n#\n%s', s, ME.message());
      end
    end       
   case 1,
    flag = 1;		% 'targFile' is up to date
   otherwise,
    flag = sprintf('Unknown error while making %s', targFile);
  end

%
% ----------------------------------------------------------------------
% Fcn:	Maybe be verbose and show information
% In:	O	= Options structure, O.verbose decides showing or not
%	h	= Handle to block, whose name and parent is printed
%	varargin= {formatString, ...}
% Ex:	Show(O, h, 'Message %d', 1)
%	Show(O, h, 2, 'Message printed if verbocity >= 2')
% Note:	Show(O, h, 0, 'Message always printend!')
%
function Log(O, varargin)
  if isempty(O) | ~O.verbose, return; end,
  
  if ~isnumeric(varargin{1})		% Check for verbocity-level
    i = 1;
  elseif O.verbose < varargin{1}
    return
  else
    i = 2;
  end
  fprintf(['chrMake: ' varargin{i} '\n'], varargin{i+1:end});

