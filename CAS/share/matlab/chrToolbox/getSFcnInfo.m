function D = getSFcnInfo(h, type, S, I)
%getSFcnInfo	Return array with numerical parameter values
%
% Stx:	pars = getSFcnInfo(h, type, S, index)
% Fcn:	Return S-function interface information depending on 'type'
% In:	h	= Block handle or name of the S-function
%	type	= Type of information, i.e. one of:
%		'P'	= S-Function parameter values
%		'IC'	= S-Function initial condition values
%		'I'	= Information about incoming signals
%		'O'	= Information about outgoing signals
%		'H'	= Return help information
%			  (or print it if no output arguments)
%		
%	S	= Optional datastructure with parameter values, or the
%		  string 'base', indicating the base workspace contains
%		  the parameter values
%	index	= Optional. Not implemented yet.
%
% Note:
%	* This function is called by findSignals() to get signal-
%	  information about an S-function.
%	% Or to create a vector with parameter values for an S-function
%	  It can extract parameter values from a data structure or WS
%	* This function can also be called with an S-function block
%	  handle as it's first parameter. 
%	
% Copyright Christian Ridderström <chr@kth.se>, 2002-2019
  if ishandle(h), sfcn = get_param(h, 'FunctionName'); else sfcn=h; end;
  if nargin == 1, type = 'h'; end;
  try
    L = feval([sfcn '_info'], lower(type));
  catch
    fprintf('\nFailed to call %s_info(.., ''%s'')\n', sfcn, type);
    ShowCallers(dbstack);
    ShowCurrentBlock;
    error(lasterr);
  end
  
  % Possibly show the help/information for this S-Function
  if type(1) == 'h'
    if nargout==0, fprintf('%s\n', L{:}); end;
  else
    D = L;
    if type(1) == 'H'			% Stupid patch to convert
      for i=1:length(D)			% empty string to ' '
	if length(D{i}) == 0, D{i} = ' '; end;
      end
    end
  end;

  % Return unless we are getting parameters or initial conditions
  if type(1) ~= 'P' & ~strcmp(type, 'IC'), return; end;
  if nargin < 3, error('No data argument supplied!'); end;
  if nargin <= 3, I = []; end;
  % D	= Cell string array with symbol names (of variables or fields)
  % S	= Optional structure with parameter values, or a string with
  %	  the name of the workspace that contains the parameter values
  % I	= Optional. Not implemented yet.
  %
  if ischar(S)
    s = sprintf('%s,', D{:});
    s = ['[' s(1:end-1) ']'];
    try
      D = evalin(S, s);
    catch
      fprintf(['\nFailed to get parameter values during: ' ...
	       '\tevalin(''%s'', ''%s'')\n'], S, s);
      ShowCallers(dbstack);
      ShowCurrentBlock;
      error(lasterr);
    end
    return
  end

  if isstruct(S)			% Old calling syntax
    if ~isempty(I)
      switch I
       case 1
       otherwise
	% Not implemented yet...
	error(sprintf('Invalid ''indices''-value =  %f\n', I))
      end
    end
  
    if length(D)>0
    	s = sprintf('S.%s,', D{:}); s = ['[' s(1:end-1) ']'];
    else
    	s='[]';
    end
    try
      D = eval(s);
    catch
      fprintf('The structure argument has the following fields:\n  ');
      f = fields(S);
      for i=1:length(f);
	fprintf('''%s''  ', f{i});
	if mod(i,9) == 0, fprintf('\n  '); end;
      end
      fprintf('\n');
      fprintf(['\nFailed to get parameter values during: \n' ...
	       '\teval(''%s'');\n'], s);
      ShowCallers(dbstack);
      ShowCurrentBlock;
      error(lasterr);
    end
    return
  else
    error('Invalid type of argument ''S''!');
  end  

function ShowCallers(C)
  fprintf('in %s.m, that was called by:\n', mfilename);
  for i=2:length(C)
    if length(C(i).name) > 60
      name = [C(i).name(1:5) '... ' C(i).name(end-60:end)];
    else
      name = C(i).name;
    end
    fprintf('  %s#%d\n', name, C(i).line);
  end
  
function ShowCurrentBlock
  h = gcbh;
  if ishandle(h)
    fprintf('Current block is %s/%s\n', get_param(h,'parent'), ...
	    get_param(h, 'name'));
  else
    fprintf('There is no current block\n');
  end
