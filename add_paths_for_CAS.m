%% Add the subfolder 'CAS/share/matlab/chrToolbox/' to the
% Octave/MATLAB path, making the scripts therein globally usable.
%
% Note: 'CAS' stands for 'Centre for Autonomus Systems', i.e. some of
% scripts etc from the WARP project.
%
% Example:
%	run(fullfile('<path-to-repo-root>', 'add_paths_for_CAS.m'));

% © 2017 Christian Ridderström <chr@kth.se>. Licensed under LGPL v3 or later.
addpath(fullfile(fileparts(mfilename('fullpath')), ...
		 'CAS', 'share', 'matlab', 'chrToolbox'));


%%% Section with test cases - No tests defined.
