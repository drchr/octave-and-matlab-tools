function [data, cols, rows] = xlsread_struct_array(file, sheet, data, fields, marker)
%{
xlsread_struct_array() - Create struct array from data in a spreadsheet

	[data, cols] = xlsread_struct_array(file, sheet, data, fields, marker)

Typically a worksheet would look something like this:

	| Some heading	| Another hdg	|		|		|
	| label_1	| label_2	| <-labels	|		|
	| 11		| 12		| 13		|		|
	| 21		| 22		| 23		|		|
	| 31		| 32		| 33		|		|

from which this function would output the following 1x3 struct array:

	struct('label_1', { 11 21 31 }, 'label_2', { 12 22 32 })

where each row of data produced one element in the struct array,
and the labels (columns) were mapped to different fields in the
struct. The special string "<-labels" in the cell to the right of
the labels was used to find and identify the column labels.

Inputs:
- file	= Path/name to spreadsheet file (or cell array with data)
- sheet	= Name of worksheet
- data	= [optional] Struct array to which data is to be written.
- fields = [optional] Specifies the fields of the resulting
	  structs to be created/populated with data as follows:
	  - Empty cellstr, '{}' (default) => use all found
	    column labels as field names.
	  - Non-empty cellstr => use as a list of field names,
	    requiring identical labels to be used in the worksheet.
	  - Struct => use as map from field names to column labels.
- marker = [optional] Specifies how to determine the row with labels.
	  - Empty, '[]' (default) => Search for '<-labels' in a
	    cell to the right of cells with labels.
	  - A string => Search for this string instead of '<-labels'
	  - A function (not yet implemented) => The user provided
	    function will be called to determine what data to read.

Outputs:
- data	= Struct array with data read from 'sheet' in 'file'.
- cols	= Struct with the column numbers that were used, primarily
	  useful if you want to verify which columns that are used.
- rows	= The row numbers of the first and last row of data used.

Side effect:	Reads (depends on) data from a file.

Notes:
- If given an input struct array 'data' with fields that do not
  correspond to columns in the worksheet, then those fields will
  be left unchanged in the output 'data'. Further:
  - If the input 'data' has fewer elements than the n:o data rows,
    then the output 'data' is extended with created/new elements.
  - If the input 'data' has more elements than the n:o data rows,
    then elements at the end are left unchanged. You can use the
    output 'rows' to see how many rows of data that were read.

- If finding labels via a "marker string", then the worksheet is
  searched column wise for it, starting with the rightmost column.
  An error is thrown if the column has more than one matching marker.

- Data is only read from rows below the row with 'marker'.

- If not all labels from 'fields' are found an error is thrown.

Example:
If the labels in the worksheet match the desired data fields
and the row of labels is marked with the default string:

	file = 'some_spreadsheet_file.xlsx';
	sheet = 'worksheet name';
	data = xlsread_struct_array(file, sheet);

However, if the column labels contain spaces, use 'fields' to
map to data fields from column labels. Further, you can provide
the 'marker' string that marks the row containing column labels:

	fields = struct('ID', 'ID key', 'val', 'Value label');
	data = xlsread_struct_array(file, sheet, [], fields, '<-label_row');


If 'data' already exists and only some of its fields, e.g. 'val'
are to be updated with values from the worksheet:

	data = struct('ID',  { 1   2   3 }, ...
		      'val', {'a' 'b' 'c'});
	fields = struct('val', 'value label');
	[data, ~, rows] = xlsread_struct_array(file, sheet, data, fields);
	assert(rows == 3);

However, you likely must also ensure that the order of elements
in 'data' matches the order of the rows of data in the worksheet.
E.g. by having a column of ID:s in the worksheet data that
uniquely idenfies each row, and (first!) compare the IDs:

	IDs = xlsread_struct_array(file, sheet, [], {'ID'});
	assert(isequal([IDs.ID], [data.ID]));


See also:	xlswrite_struct_array.m
%}
% © 2017 Christian Ridderström <chr@kth.se>. Licensed under LGPL v3 or later.

  narginchk(2, 5);
  if ~exist('data', 'var'), data = []; end;
  if ~exist('fields', 'var'), fields = {}; end;
  if ~exist('marker', 'var'),
    marker = default_marker_for_label_row_in_worksheet_data();
  end;

  assert_argument_has_valid_type(file, 'file', @(x) ischar(x) || iscell(x));
  assert_argument_has_valid_type(sheet, 'sheet', @ischar);
  assert_argument_has_valid_type(fields, 'fields', ...
				 @(x) isstruct(x) || iscellstr(x));
  assert_argument_has_valid_type(marker, 'marker', @ischar);
  data = ensure_argument_is_a_struct(data, 'data');

  
  C = read_worksheet_data_as_cell(file, sheet);
  [labels, labels_row] = get_labels_from_worksheet_data(C, marker);

  fields = make_struct_mapping_field_names_to_labels(fields, labels);

  cols   = make_struct_mapping_field_names_to_column_number(fields, labels);

  data = ensure_struct_has_fields(data, fieldnames(fields)');
  rows = rows_with_data_in_worksheet(C, labels_row);
  data = read_data(C, data, cols, rows);
end


%% Read the data columnwise, downwards. 
% TODO: Try to vectorise while still retaining any preexisting
% content of 'data'.
function data = read_data(worksheet_data, data, columns, rows)
  for F = fieldnames(columns)'
    col = columns.(F{1});
    ii = 1;
    for row = rows
      data(ii).(F{1}) = worksheet_data{row, col};
      ii = ii + 1;
    end
  end
end


%!function [P] = test_parameters()
%!  P.FUT = @xlsread_struct_array;
%!  P.name = 'xlsread_struct_array';
%!
%!  P.function_dir = fileparts(which(P.name));
%!  P.test_data_dir = fullfile(P.function_dir, 'test');
%!  P.worksheet_name_format = 'tc-data-%d';
%!endfunction


%!function assert_result_matches(Pars, expected, result)
%!  for f = expected.fields
%!    assert(isfield(result.data, f{1}), sprintf('Missing field %s', f{1}));
%!  end
%!  
%!  assert(all(size(expected.data) == size(result.data)), ...
%!	   sprintf('Size mismatch: %s vs %s', ...
%!		   mat2str(size(expected.data)), ...
%!                 mat2str(size(result.data))));
%!endfunction


%!test
%! Pars = test_parameters();
%! file = fullfile(Pars.test_data_dir, 'test-case-1.xlsx');
%! worksheet = sprintf(Pars.worksheet_name_format, 1);
%!
%! E = struct();
%! E.fields = { 'lbl_1' 'lbl_2' 'lbl_3' 'lbl_4' 'lbl_5' };
%! E.data = struct();
%! E.data = cell2struct({ 31 32 33 34 35 ; ...
%!		          41 'forty and two' 43 44 45 }, ...
%!		        E.fields, 2);
%! E.data = E.data';  %% Hmm
%!		     
%! R = struct();
%! [R.data, R.cols] = Pars.FUT(file, worksheet);
%!
%! assert_result_matches(Pars, E, R);

