function [marker] = default_marker_for_label_row_in_worksheet_data()
  marker = '<-labels';
end    

%% If changing default, remember to update documenation in
% 'xlsread_...' and 'xlswrite_...'
%
%!test
%! FUT = @default_marker_for_label_row_in_worksheet_data;
%! expected = '<-labels';
%! assert(strcmp(FUT(), expected));
