function [labels, row] = get_labels_from_worksheet_data(ws_data, marker)
  [row, col] = find_label_marker(ws_data, marker);
  labels = ws_data(row, 1:col-1);

  %% For e.g. testing, make it always have the same size when empty
  if isempty(labels), labels = {}; end
end    


function [row, col] = find_label_marker(worksheet_data, marker)
  rightmost_column = size(worksheet_data, 2);

  for col = rightmost_column : -1 : 1
    row = find(strcmpi(marker, worksheet_data(:,col)));
    if numel(row) == 1
      return
    elseif numel(row) > 1
      fprintf('row = %s, col = %s, labels:', mat2str(row), mat2str(col));
      disp(worksheet_data(row, :));
      error('Found too many cells matching the label marker ''%s''', ...
	    marker);
    end
  end
  
  error('Could not find a cell matching the label marker ''%s''', marker);
end

%!function P = test_parameters()
%!  % Set the 'function under test', simplifies in case of renaming
%!  P.FUT = @get_labels_from_worksheet_data;

%!error <Could not find a cell matching the label marker 'M'> ...
%!      test_parameters().FUT({}, 'M');

% TODO Reinstall error check below
% !error <Found too many cells matching the label marker 'M'> ...
% !      test_parameters().FUT({'M'; 'M'}, 'M');

%!test
%! P = test_parameters();
%! C = { 'M' };  % A row with a marker, but no labels
%! [R.labels, R.labels_row] = P.FUT(C, 'M');
%! E = struct('labels', {{}}, 'labels_row', 1);
%! assert(isequal(R, E));

%!test
%! P = test_parameters();
%! C = { NaN 'M' };  % A row with a marker, but only empty label
%! [R.labels, R.labels_row] = P.FUT(C, 'M');
%! E = struct('labels', {{NaN}}, 'labels_row', 1);
%! assert(isequaln(R, E));

%!test
%! P = test_parameters();
%! C = { 11 12 13; 'lbl-1' 'M' 23; 31 32 33 };
%! [R.labels, R.labels_row] = P.FUT(C, 'M');
%! E = struct('labels', {{'lbl-1'}}, 'labels_row', 2);
%! assert(isequaln(R, E));
%! 
