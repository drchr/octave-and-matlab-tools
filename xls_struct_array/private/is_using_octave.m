function is_octave = is_using_octave ()
  persistent p;
  if isempty(p)
    p = exist ('OCTAVE_VERSION', 'builtin');
  end
  is_octave = p;
end
