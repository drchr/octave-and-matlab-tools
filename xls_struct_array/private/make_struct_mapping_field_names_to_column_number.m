function cols = make_struct_mapping_field_names_to_column_number(fields, labels)
  cols = struct();
  for F = fieldnames(fields)'
    label = fields.(F{1});
    col = find(strcmp(label, labels));
    if numel(col) == 0
      labels
      error('Label ''%s'' not found', label);
    elseif numel(col) > 1
      col, labels
      error('Found %d matches for label ''%s''', numel(col), label);
    end
    cols.(F{1}) = col;
  end
end

%!function P = test_pars()
%!  % Set the 'function under test', simplifies in case of renaming
%!  P.FUT = @make_struct_mapping_field_names_to_column_number;

%!assert(isequal(test_pars().FUT(struct('A','lbl-A','B','lbl-B'), ...
%!                               {'' 'lbl-A' NaN 'lbl-B'}),  ...
%!               struct('A',2, 'B',4)));

%% Check we get error when 'labels' is missing a label
%!error <Label 'lbl-A' not found> ...
%!      test_pars.FUT(struct('A','lbl-A'), {'lbl'});

%% Check we get error when 'labels' has a duplicate label
%!error <Found 2 matches for label 'lbl-A'> ...
%!      test_pars.FUT(struct('A','lbl-A'), {'lbl-A' 'lbl-A'});

