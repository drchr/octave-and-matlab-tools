function C = read_worksheet_data_as_cell(spreadsheet_file, worksheet)
  if ~iscell(spreadsheet_file)
    assert_xlsread_is_available();
    [~, ~, C] = xlsread(spreadsheet_file, worksheet);
  else
    %% Note: Returning 'spreadsheet_file' as 'C' when it's a cell
    %% array simplifies debugging.
    C = spreadsheet_file;
  end
end

function assert_xlsread_is_available()
  if ~is_using_octave(), return; end
  if ~exist('xlsread', 'file')
    error('Function xlsread() not found, try:\n\t%s\n\n', 'pkg load io');
  end
end


%!function [P] = test_pars()
%!  P.FUT = @read_worksheet_data_as_cell;
%!  P.file = fullfile('..', 'test', 'test-case-1.xlsx');

%!test
%! C = test_pars().FUT({}, 'sheet');
%! assert(isequal(C, {}));

%!test
%! P = test_pars();
%! C = P.FUT(P.file, 'tc-data-0a')
%! assert(isequal(C, {}));

%!test
%! P = test_pars();
%! C = P.FUT(P.file, 'tc-data-0b');
%! E = { 11 'B1' 13 ; 21 22 'C2' };
%! assert(isequal(C, E));
