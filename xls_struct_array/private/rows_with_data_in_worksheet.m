%% Given a cell array of worksheet data and the row number for the row
% containing the labels, return the rows that actually contain data by
% skipping any trailing empty rows.
function rows = rows_with_data_in_worksheet(worksheet_data, label_row)

  % Note: "empty" is defined as [], '' or a single NaN.
  is_empty = @(v) ~iscell(v) && (isempty(v) || isequaln(v, NaN));

  for row = size(worksheet_data,1) : -1 : label_row
    if ~all(cellfun(is_empty, worksheet_data(row,:)))
      break
    end
  end

  rows = (label_row+1) : row;
end


%!function P = test_pars()
%!  % Set the 'function under test', simplifies in case of renaming
%!  P.FUT = @rows_with_data_in_worksheet;

%!assert(isequal(test_pars().FUT( { 'h1' 'h2'; 1 2; 3 4; '' '' }, 1), ...
%!               2:3));
