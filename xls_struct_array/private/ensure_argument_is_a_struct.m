function S = ensure_argument_is_a_struct(S, name)
  narginchk(2,2);
  if ~isstruct(S)
    %% Note: Using 'if' instead of 'assert()' to avoid unneccessary
    %% evaluation of the 'evalc(...)'
    if ~isequal(S, [])
      error('Argument ''%s'' must be a struct or [], but is: %s', ...
            name, evalc('disp(S)'));
    end
    S = struct();
  end
end

%!error <Argument 'dummy-name' must be a struct or \[], but is:  1> ...
%!      ensure_argument_is_a_struct(1, 'dummy-name');

%!test
%! FUT = @ensure_argument_is_a_struct;
%!
%! ensure_argument_is_a_struct(struct(), 'dummy-name');
%! ensure_argument_is_a_struct([], 'dummy-name');

