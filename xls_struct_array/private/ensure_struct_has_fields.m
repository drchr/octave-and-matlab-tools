function S = ensure_struct_has_fields(S, fields)
  narginchk(2, 2);

  %% If the struct 'S' is empty, it'll have to cleared before
  %% returning, as adding fields makes it non-empty.
  was_empty = isempty(S);
    
  for F = fields
    if ~isfield(S, F{1})
      S(1).(F{1}) = [];
    end
  end

  if was_empty 
    S(1) = [];
  end
end

%!function [P] = test_pars()
%!  P.FUT = @ensure_struct_has_fields;

%!test
%! S0 = struct();
%! R = test_pars().FUT(S0, {});
%! E = S0;
%! assert(isequal(R, E));

%!test
%! S0 = struct();
%! R = test_pars().FUT(S0, {'field'});
%! E = S0; E.field = [];
%! assert(isequal(R, E));

%!test
%! S1 = struct('a', {1});
%! R = test_pars().FUT(S1, {'field'});
%! E = S1; E.field = [];
%! assert(isequal(R, E));

%!test
%! S2 = struct('a', {1 2});
%! R = test_pars().FUT(S2, {'field'});
%! E = S2; E(1).field = [];
%! assert(isequal(R, E));

