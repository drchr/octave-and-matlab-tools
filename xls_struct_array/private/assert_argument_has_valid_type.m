function assert_argument_has_valid_type(argument, name, check_function)
  assert(check_function(argument), ...
	 'Argument ''%s'' has unexpected type: %s', name, class(argument));
end

%!test
%!  assert_argument_has_valid_type('hello', 'dummy', @ischar);

%!error <Argument 'dummy' has unexpected type: struct>
%!  assert_argument_has_valid_type(struct(), 'dummy', @ischar);

