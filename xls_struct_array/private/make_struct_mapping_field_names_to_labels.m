function fields = make_struct_mapping_field_names_to_labels(fields, labels)
  if isstruct(fields), return; end
  assert(iscellstr(fields));
  assert(iscell(labels));

  if isequal(fields, {})
    fields = make_fields_struct_from_column_labels(labels);
  elseif iscellstr(fields)
    fields = make_fields_struct_from_cellstr(fields);
  end
end


function fields = make_fields_struct_from_column_labels(labels)
  fields = labels(cellfun(@is_label, labels));
  fields = cell2struct(fields, fields, 2);
end


function is_lbl = is_label(s)
  is_lbl = ischar(s) && ~all(isstrprop(s, 'wspace'));
end


%% Note: Consider also allowing Fields e.g. being a two-row cellstr,
% with e.g. the 1st row being fields and the 2nd row being labels.
function fields = make_fields_struct_from_cellstr(Fields)
  fields = struct();
  for field = Fields
    name = field{1};
    fields.(name) = name;
  end
end


%!function P = test_pars()
%!  % Set the 'function under test', simplifies in case of renaming
%!  P.FUT = @make_struct_mapping_field_names_to_labels;

%% A struct 'S' in should just be returned.
%!assert(isequal(test_pars().FUT(struct('a',1), {}),  ...
%!               struct('a',1)));

%% If 'fields' is a cellstr, a struct based on it is returned
%!assert(isequal(test_pars().FUT({'lbl_A' 'lbl_B'}, {}),  ...
%!               struct('lbl_A', 'lbl_A', 'lbl_B', 'lbl_B')));

%% If 'fields' is empty cell, use 'labels' to create struct
%!assert(isequal(test_pars().FUT({}, {'lbl_A'}),  ...
%!               struct('lbl_A', 'lbl_A')));

%% ... ignoring "empty" cells in 'labels'
%!assert(isequal(test_pars().FUT({}, {NaN '' 'lbl_A' ' '}),  ...
%!               struct('lbl_A', 'lbl_A')));

