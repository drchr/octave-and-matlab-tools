function [C, cols, rows] = xlswrite_struct_array(file, sheet, data, fields, marker)
%{
xlswrite_struct_array() - Write data to spreadsheet from a struct array

	data = xlswrite_struct_array(file, sheet, data, fields, marker)

Typically a worksheet's first lines would look something like this:

	| Some heading	| Another hdg	|		|		|
	| label_1	| label_2	| <-labels	|		|

This function then writes e.g. a 1x3 struct array to the worksheet
using the following commands:

	data = struct('label_1', { 11 21 31 }, 'label_2', { 12 22 32 });
	xlswrite_struct_array('file.xlsx', 'sheet', data);

Resulting in a worksheet that now contains

	| Some heading	| Another hdg	|		|		|
	| label_1	| label_2	| <-labels	|		|
	| 11		| 12		| 13		|		|
	| 21		| 22		| 23		|		|
	| 31		| 32		| 33		|		|

where element of the struct array was written as a row of data,
with different fields going to different labels (columns).
The special string "<-labels" in the cell to the right of
the labels was used to find and identify the column labels, as well
as to which rows the data was to be written.


Inputs:
- file	= Path/name to spreadsheet file (or cell array with data)
- sheet	= Name of worksheet
- data	= Struct array with data
- fields = [optional] Specifies the fields of 'data' to be written
	  to 'sheet' data as follows:
	  - Empty cellstr, '{}' (default) => use all found
	    column labels as field names.
	  - Non-empty cellstr => use as a list of field names,
	    requiring identical labels to be used in the worksheet.
	  - Struct => use as map from field names to column labels.
- marker = [optional] Specifies how to determine the row with labels.
	  - Empty, '[]' (default) => Search for '<-labels' in a
	    cell to the right of cells with labels.
	  - A string => Search for this string instead of '<-labels'
	  - A function (not yet implemented) => The user provided
	    function will be called to determine what data to read.

Out:
- C	= Cell array populated with 'data', from which the script
	  has written to the spreadsheet.
- cols	= Struct with the column numbers that were used, primarily
	  useful if you want to verify which columns that are used.
- rows	= The row numbers of the first and last row of data used.

Side effect:	Reads/writes data to a file.

Note:
- 'file' must be writeable.

- If finding labels via a "marker string", then the worksheet is
  searched column wise for it, starting with the rightmost column.
  An error is thrown if the column has more than one matching marker.

- Data is only written to rows below the row with 'marker'.
  Formulas on rows above the marker are therefore preserved.

- If not all labels from 'fields' are found an error is thrown.


Example:

If the labels in the worksheet match the fields in 'data'
and the row of labels is marked with the default string:

	file = 'some_spreadsheet_file.xlsx';
	sheet = 'worksheet name';
	data = struct('ID',  {'ID-1', 'ID-2'}, 'value', {1, 2});
	xlswrite_struct_array(file, sheet, data);

However, if the column labels contain spaces, use 'fields'
to map between data fields and column labels:

	fields = struct('ID', 'ID key', 'value', 'Value label');
	xlswrite_struct_array(file, sheet, data, fields);

Note: The subsequent examples assume 'file' and 'sheet' have been set.


See also:	xlsread_struct_array.m
%}
% © 2017 Christian Ridderström <chr@kth.se>. Licensed under LGPL v3 or later.

  narginchk(3, 5);
  if ~exist('fields', 'var'), fields = {}; end;
  if ~exist('marker', 'var'), markerStr = []; end;
  if ~exist('marker', 'var'),
    marker = default_marker_for_label_row_in_worksheet_data();
  end;

  assert_argument_has_valid_type(file, 'file', @(x) ischar(x) || iscell(x));
  assert_argument_has_valid_type(sheet, 'sheet', @ischar);
  assert_argument_has_valid_type(data, 'data', @isstruct);
  assert_argument_has_valid_type(fields, 'fields', ...
				 @(x) isstruct(x) || iscellstr(x));
  assert_argument_has_valid_type(marker, 'marker', @ischar);

  C = read_worksheet_data_as_cell(file, sheet);
  [labels, labels_row] = get_labels_from_worksheet_data(C, marker);

  fields = make_struct_mapping_field_names_to_labels(fields, labels);

  cols = make_struct_mapping_field_names_to_column_number(fields, labels);
  rows = labels_row + (1:numel(data));
  C = update_worksheet_data(C, data, rows, cols);

  if ~iscell(file)
    write_data(file, sheet, C, labels_row);
  end
end


%% Helper functions

function C = update_worksheet_data(C, data, rows, cols)
  %% Copy 'data' into 'C'. its rows are automatically increased if needed
  for F = fieldnames(cols)'
    col = cols.(F{1});
    C(rows, col) = { data.(F{1}) };
    
    %% "Empty" any cells below the rows just set from 'data'
    [C{rows(end)+1:end, col}] = deal(NaN);
  end
end


function write_data(file, sheet, worksheet_data, labels_row)
  assert_xlswrite_is_available();
  row = labels_row + 1;	% Start row at which data will be written
  assert(size(worksheet_data, 1) >= row, ...
         'No data found for row %d and higher', row)
  range = sprintf('A%d', row);
  xlswrite(file, worksheet_data(row:end,:), sheet, range);
end    


function assert_xlswrite_is_available()
  if ~is_using_octave(), return; end
  if ~exist('xlswrite', 'file')
    error('Function xlswrite() not found, try:\n\t%s\n\n', 'pkg load io');
  end
end
