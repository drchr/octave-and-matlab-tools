%% Convenience function to return '[]' if a variable is not defined
% (i.e., not defined in the workspace that's calling this function).
%
% Note: Not recommended for performance critical code sections.
%
%!demo
%!  x = use_empty_if_variable_is_undefined('x_missing')
%!  # Sets 'x' to '[]' as the variable 'x_missing' is undefined
%
%!demo
%!  x_exists = 1;
%!  x_exists = use_empty_if_variable_is_undefined('x_exists')
%!  # Leaves 'x_exists' unchanged as it exists
%
%!demo
%!  x_exists = 1;
%!  [x_exists, x_missing] = use_empty_if_variable_is_undefined...
%!                             ('x_exists', 'x_missing')
%!  # Leaves 'x_exists' unchanged, but sets 'x_missing' to []

% © 2017 Christian Ridderström <chr@kth.se>. Licensed under LGPL v3 or later.
function varargout = use_empty_if_variable_is_undefined(varargin)
  assert(nargin >= 1, 'Not enough input arguments');
  assert(iscellstr(varargin), 'Arguments must be strings');

  for ii = 1:numel(varargin)
    var_name = varargin{ii};
    is_defined_cmd = sprintf('exist(''%s'', ''var'')', var_name);
    if evalin('caller', is_defined_cmd);
      varargout{ii} = evalin('caller', var_name);
    else
      varargout{ii} = [];
    end
  end
end



%%% Section with Octave test cases
%
% Convenience function to avoid modifying tests if renaming the FUT.
% (FUT = function under test).
%
%!function fut = FUT() fut = @use_empty_if_variable_is_undefined; end


%!test % Single defined variable
%!  x_exists = 1;
%!  fut = FUT();
%!  result = fut('x_exists');
%!  assert(result, 1);


%!test % Single undefined variable
%!  # x_missing = 2;
%!  fut = FUT();
%!  result = fut('x_missing');
%!  assert(result, []);


%!test % One undefined variable and one defined variable
%!  # x_missing = [];
%!  x_exists = 1;
%!  fut = FUT();
%!  [result{1:2}] = fut('x_missing', 'x_exists');
%!  assert(result, {[], 1});



%!error<Not enough input arguments>FUT()()

%!error<Arguments must be strings>FUT()(1)


