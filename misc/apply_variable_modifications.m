%% Apply variable modifications, often to fields in a struct.
%
% Support function to modify values of variables. The variable is
% often a struct, and it's individual fields representing parameters
% that are to be modified.
%
% In:
% - S = Variable whose value(s) are optionally to be modified
% - data = The variable modifications to apply.
%     It's a cell array with one variable reference and
%     one or more new value per row. 
%     - The 1st column references the variable / field to modify, 
%       e.g. as { 'name_of_struct' 'name_of_field' }.
%     - The 2nd column contains the corresponding new values.
% - dest_reference = (Opt.) Represents the name of 'S' and it is used to
%     match against the variable references in 'data'.
% 
%!demo
%!  v = 1;
%!  mod_data = { { 'v' }      2  ; ...
%!               { 'S' 'b' } 10  };
%!  [v, changed] = apply_variable_modifications(v, mod_data)
%!  # Modified 'v' to 2
%
%!demo
%!  S = struct('a', 1, 'b', 2);
%!  mod_data = { { 'S' 'b' } 10 };
%!  [S, changed] = apply_variable_modifications(S, mod_data)
%!  # Modified 'S.b' to 10
%
%!demo
%!  Pars = struct('par_1', 1, ...
%!                'par_2', 2, ...
%!                'par_3', 3);
%!  mod_1 = { { 'Pars' 'par_1' } 1.1 1.2 ; ...
%!            { 'Pars' 'par_2' } 2.1 2.2 };
%!  mod_2 = { { 'Pars' 'par_3' } 3.1 3.2 };
%!  mod_data = combine_variable_modifications({mod_1, mod_2});
%!  disp(variable_modifications_to_char({mod_data}));
%!  [Pars, changed] = apply_variable_modifications(Pars, mod_data(:,[1 1+3]))
%!  # Modifying parameters inside a struct
%
% See also variable_modifications_example.m, combine_variable_modifications(), 
% variable_modifications_factor() and variable_modifications_to_char().

% © 2017 Christian Ridderström <chr@kth.se>. Licensed under LGPL v3 or later.
function [S, changed_vars] = apply_variable_modifications(S, data, dest_reference)
  narginchk(2, 3);
  if ~exist('dest_reference', 'var'), dest_reference = []; end
  dest_reference = cellstr(use_default_if_empty(dest_reference, inputname(1)));

  assert_valid_data_type(data);
  assert(iscellstr(dest_reference) && numel(dest_reference) >= 1 && ...
         ~isempty(dest_reference{1}), ...
         'Arg. ''%s'' must be a non-empty ''cellstr''', 'dest_reference');

  if isempty(data), changed_vars = {}; return; end


  changed_vars = {};
  idx = find(cellfun(@(C) strcmp(dest_reference{1}, C{1}), data(:, 1)));

  for F = data(idx, 1:2)'
    [ref, value] = deal(F{:});
    [S, changed] = apply(S, ref, value, dest_reference);
    changed_vars = [changed_vars changed];
  end
end


function [S, changed] = apply(S, ref, value, destination)
  if isequal(ref, destination)
    S = value;
    changed = join_reference_components(destination);
  elseif ~isstruct(S) || numel(ref) <= numel(destination)
    changed = {};
  else
    field = ref{1 + numel(destination)};
    if isfield(S, field)
      [S.(field), changed] = apply(S.(field), ref, value, [destination {field}]);
    else
      changed = {};
    end
  end
end


function assert_valid_data_type(data)
  assert(iscell(data), 'Arg. ''%s'' must be a cell array', 'data');
  if isempty(data), return; end

  assert(size(data, 2) == 2, 'Expected ''%s'' to have two columns', 'data');

  assert(all(cellfun(@iscellstr, data(:, 1))), ...
         'First column of arg. ''%s'' must be cellstr:s', 'data');
  %% todo: assert non-zero size of first dest path value
end


function s = join_reference_components(C), s = strjoin(C, '.'); end



%%% Section with Octave test cases
%
% Convenience function to avoid modifying tests if renaming the FUT.
% (FUT = function under test).
%
%!function fut = FUT() fut = @apply_variable_modifications; end

%!function S = original_data()
%!  S = struct('f1', 0, ...
%!             'f2', [0 0], ...
%!             'f3', struct('f11', 0));
%! end


%!test % Empty modification data
%!  fut = FUT();
%!  S = original_data();
%!  expect = S;
%!  result = fut(S, {}, 'S');
%!  assert(expect, result);
%!
%!  % Test with name of the referenced variable automatically deduced
%!  result = fut(S, {});
%!  assert(expect, result);


%!test % Modify single non-struct variable
%!  fut = FUT();
%!  some_variable = 0;
%!  data = { ...
%!           { 'S' 'f1' }          1 ; ...
%!           { 'some_variable' }  21 ; ...
%!         };
%!  expect = 21;
%!  [result{1:2}] = fut(some_variable, data, 'some_variable');
%!  assert({expect, {'some_variable'}}, result);
%!
%!  % Test with name of the referenced variable automatically deduced
%!  [some_variable, changed_vars] = fut(some_variable, data);
%!  assert(some_variable, expect);
%!  assert(changed_vars, { 'some_variable' });


%!test % Modify scalar number (in struct), i.e. field '.f1'
%!  fut = FUT();
%!  field = 'f1';
%!  data = { { 'S' field } 1 };
%!  [S, S1] = deal(original_data());
%!  S1.(field) = data{end};
%!  expect = { S1  { 'S.f1' } };
%!  [result{1:2}] = fut(S, data);
%!  assert(expect, result);


%!test % Modify field (in struct) that contains an array, i.e. field '.f2'
%!  fut = FUT();
%!  field = 'f2';
%!  data = { { 'S' field }  [1 2] };
%!  [S, S1] = deal(original_data());
%!  S1.(field) = data{end};
%!  expect = { S1  { 'S.f2' } };
%!  [result{1:2}] = fut(S, data);
%!  assert(expect, result);


%!test % Modify field (in struct) that contains a struct, i.e. field '.f3'
%!  fut = FUT();
%!  field = 'f3';
%!  data = { { 'S' field }  struct('f11', 1) };
%!  [S, S1] = deal(original_data());
%!  S1.(field) = data{end};
%!  expect = { S1  { 'S.f3' } };
%!  [result{1:2}] = fut(S, data);
%!  assert(expect, result);


%!test % Modify nested field in struct, i.e. field '.f3.f11'
%!  fut = FUT();
%!  data = { { 'S' 'f3' 'f11' }  1 };
%!  [S, S1] = deal(original_data());
%!  S1.f3.f11 = data{end};
%!  expect = { S1  { 'S.f3.f11' } };
%!  [result{1:2}] = fut(S, data);
%!  assert(expect, result);


%!test % Modify multiple fields in a struct, i.e. field '.f1' and '.f3.f11'
%!  fut = FUT();
%!  data = { { 'S' 'f1' }        2 ; ...
%!           { 'S' 'f3' 'f11' }  1 };
%!  [S, S1] = deal(original_data());
%!  S1.f1     = data{1, end};
%!  S1.f3.f11 = data{2, end};
%!  expect = { S1  { 'S.f1'  'S.f3.f11' } };
%!  [result{1:2}] = fut(S, data);
%!  assert(expect, result);


%!test % Cellstr with two components as destination reference
%!  fut = FUT();
%!  data = { { 'S' 'f3' 'f11' }  1 };
%!  Par.S = original_data();
%!  Par1.S = Par.S;
%!  Par1.S.f3.f11 = data{end};
%!  expect = { Par1.S  { 'S.f3.f11' } };
%!  [result{1:2}] = fut(Par.S, data, {'S'});
%!  assert(expect, result);



%!error<not enough input arguments>FUT()()
%!error<not enough input arguments>FUT()(1)

%!error<Expected 'data' to have two columns>...
%!     FUT()(struct(), { 1 })

%!error<First column of arg. 'data' must be cellstr:s>...
%!     FUT()(struct(), { 1 1 })
%!error<First column of arg. 'data' must be cellstr:s>...
%!     FUT()(struct(), { { 'v' } 1 ; { 1 } 1 })


%!error<Arg. 'dest_reference' must be a non-empty 'cellstr'>...
%!     FUT()(struct('f',{}), {})

