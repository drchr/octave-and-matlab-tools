%% Convert a cell array to a char vector, typically for human to see.
%
% An extra options argument, 'opts', allows control of how the cell
% array is converted. By default the size of the output is limited in
% terms of the number of rows and columns.
%
% Note: The main rationale for this function is that I don't like the
% standard way cell arrays are printed in Octave.
%
%!demo
%!  C = { 1 'b' [1 2]; ...
%!        2 'd' struct('a', 1) };
%!  s = cell_to_char(C);
%!  fprintf('%s\n', s);
%!  # Convert cell to string
%
%!demo
%!  cell_to_char('--default-opts')
%!  # See the default options struct
%
%!demo
%!  opts = struct('max_cols', 4);
%!  cell_to_char('--opts', opts)
%!  # Returns 'opts', with additional defaults added
%
% Caveat: The output is intended for humans, and not for serialization.
% In particular, the transformation might be lossy.
%

% © 2017 Christian Ridderström <chr@kth.se>. Licensed under LGPL v3 or later.
function s = cell_to_char(C, opts)
  assert(nargin >= 1, 'Not enough input arguments');

  if ~exist('opts', 'var'), opts = struct(); end
  assert(isstruct(opts), 'A second options argument must be a struct');
  default = struct(...
                   'indent_symbol',           ' ', ...
                   'indent_size',             4, ...
                   'max_column_width',        12, ...
                   'min_column_width',        4, ...
                   'column_separator',        ' ', ...
                   'row_separator',           sprintf('\n'), ...
                   'quote_char_vector',       true, ...
                   'limit_rows',              true, ...
                   'max_rows',                30, ...
                   'always_include_last_row', true, ...
                   'limit_cols',              true, ...
                   'always_include_last_col', true, ...
                   'max_cols',                7, ...
                   'row_suffix',              [], ...
                   'return_cellstr',          false, ...
                   'hide_empty',              false, ...
                   'hide_empty_char',         false, ...
                   'hide_empty_cell',         false, ...
                   'hide_empty_matrix',       false);
  assert_no_extra_fields_in_struct(opts, default);
  opts = add_missing_field_to_struct(opts, default);
  if isequal(C, '--default-opts'), s = default; return; end
  if isequal(C, '--opts'), s = opts; return; end
  assert(iscell(C));

  check_options(opts, C);

  D = maybe_reduce_size(opts, C);
  S = print_cell_array(opts, D);
  if opts.return_cellstr
    s = S;
  else
    s = strjoin(S, '');
  end
  if nargout == 0, fprintf('%s\n', strjoin(S, '')); end
end


function D = maybe_reduce_size(opts, C)
  [nr_of_rows, nr_of_cols]  = size(C);

  [rows, limited_rows] = range(nr_of_rows, opts.limit_rows, opts.max_rows, ...
                               opts.always_include_last_row);
  [cols, limited_cols] = range(nr_of_cols, opts.limit_cols, opts.max_cols, ...
                               opts.always_include_last_col);

  D = C(rows, cols);
  
  if limited_rows && opts.always_include_last_row
    D(end+1,:) = D(end,:);
    D(end-1,:) = { truncation_symbol() };
  end

  if limited_cols && opts.always_include_last_col
    D(:, end+1) = D(:, end);
    D(:, end-1) = { truncation_symbol() };
  end
end


function y = truncation_symbol()
  y = struct('truncation_symbol_value', '…');
end


function [index, is_limited] = range(number, limit, max_nr, include_last)
  is_limited = limit && number > max_nr;
  if is_limited
    if include_last
      index = [1:max_nr-1 number];
    else
      index = 1:max_nr;
    end
  else
    index = 1:number;
  end
end


function S = print_cell_array(opts, D)
  C = cellfun(@(s) cell_element_to_char(opts, s), D, 'uniform', 0);
  N = max(cellfun(@length, C), [], 1);

  for col = find(N > opts.max_column_width)
    C(:, col) = cellfun(@(s) maybe_truncate(s, opts.max_column_width), ...
                        C(:, col), 'uniform', 0);
  end

  for col = 1:size(C, 2)
    width = max(min(N(col), opts.max_column_width), opts.min_column_width);
    fmt = sprintf('%%-%ds', width);
    C(:, col) = cellfun(@(s) sprintf(fmt, s), C(:, col), 'uniform', 0);
  end

  rows = size(C, 1);
  S = cell(1, rows);
  indent = repmat(opts.indent_symbol, 1, opts.indent_size);
  for row = 1:rows
    if isempty(opts.row_suffix)
      suffix = '';
    elseif ischar(opts.row_suffix)
      suffix = opts.row_suffix;
    elseif iscell(opts.row_suffix) && numel(opts.row_suffix) == 1
      suffix = opts.row_suffix{1};
    else
      suffix = opts.row_suffix{row};
    end
    row_string = strjoin(C(row, :), opts.column_separator);
    S{row} = [indent row_string suffix opts.row_separator];
  end
end


function s = cell_element_to_char(opts, value)
  if isempty(value)
    if opts.hide_empty ...
       || (opts.hide_empty_char   && ischar(value)) ...
       || (opts.hide_empty_cell   && iscell(value)) ...
       || (opts.hide_empty_matrix && isnumeric(value))
      s = '';
      return;
    end
  end

  if isnumeric(value) || islogical(value)
    s = mat2str(value);
  elseif ischar(value)
    s = char_array_to_char(opts, value);
  elseif iscell(value) && isempty(value)
    s = '{}';
  elseif isstruct(value) && isfield(value, 'truncation_symbol_value')
    s = value.truncation_symbol_value;
  elseif iscell(value) && isrow(value)
    [is_small, s] = is_small_to_print(opts, value);
    if ~is_small, s = to_size_and_type(value); end
  else
    s = to_size_and_type(value);
  end
end


function [is_small, s] = is_small_to_print(opts, C)
  [is_small, s] = deal(false, '');
  if ~all(cellfun(@(x) ischar(x) || isnumeric(x), C)), return; end
  s = strjoin(cellfun(@(x) cell_element_to_char(opts, x), C, 'uni', 0));
  s = ['{' s '}'];
  is_small = length(s) <= opts.max_column_width;
end


function s = char_array_to_char(opts, value)
  is_row = size(value, 1) <= 1; % True also for '', unlike 'isrow('')'
  printable = is_row && all(isstrprop(value, 'print'));
  if printable
    quoting_avoidable = ~isempty(value) ...
                        && isstrprop(value(1),   'alpha') ...
                        && isstrprop(value(end), 'alphanum');
    if ~opts.quote_char_vector && quoting_avoidable
      s = value;
    else
      s = quote(value);
    end
  else
    s = to_size_and_type(value);
  end
end


function s = to_size_and_type(value)
  [row, col] = size(value);
  if iscell(value)
    s = sprintf('{%dx%d %s}', row, col, class(value));
  else
    s = sprintf('[%dx%d %s]', row, col, class(value));
  end
end


function s = quote(s)
  s = ['''' s ''''];
end


function s = maybe_truncate(s, width)
  if length(s) <= width, return; end

  if s(1) == '['
    s = [s(1:width-2) '…]'];
  elseif s(1) == '{'
    s = [s(1:width-2) '…}'];
  else
    s = [s(1:width-1) '…'];
  end
end


function check_options(opts, C)
  assert_row_suffix(opts.row_suffix, size(C, 1));
end


function assert_row_suffix(row_suffix, nr_of_rows)
  if isempty(row_suffix) || ischar(row_suffix), return, end
  assert(iscellstr(row_suffix), ...
         'Option .%s is ''%s'' instead of ''char'' or ''cellstr''', ...
         'row_suffix', class(row_suffix));
  assert(numel(row_suffix) == nr_of_rows, ...
         'Option .%s has %d elements, but cell array has %d rows', ...
         'row_suffix', numel(row_suffix), nr_of_rows)
end



%%% Section with Octave test cases
%
% Convenience function to avoid modifying tests if renaming the FUT.
% (FUT = function under test).
%
%!function fut = FUT() fut = @cell_to_char; end

%% Support options for testing, e.g. no indentation
%!function S = test_opts_0(varargin)
%!  S = struct('indent_size', 0, varargin{:});
%!endfunction

%!function y = quote(s) y = ['''' s '''']; end 
%!function y = replace_ws(s) y = strrep(s, ' ', '_'); end
%!function y = LF() y = sprintf('\n'); end

%!test % Empty cell array
%!  fut = FUT();
%!  expected = ''; % Surprising, but what Octave and MATLAB do
%!  result = fut({});
%!  assert(expected, result);


%!test % Single element
%!  C = { 1 };
%!  opts = test_opts_0();
%!  expect = ['1   ' LF];
%!  result = cell_to_char(C, opts);
%!  assert(result, expect);


%!test % Cell column array with different types
%!  %%    Input         Expected element
%!  C = { ...
%!        1             '1'            ; ...
%!        true          'true'         ; ...
%!        false         'false'        ; ...
%!        'a'           quote('a')     ; ...
%!        struct()	'[1x1 struct]' ; ...
%!        []            '[]'           ; ...
%!        {}            '{}'           ; ...
%!      };
%!  max_width = max(cellfun(@length, C(:,2)));
%!  fmt = sprintf('%%-%ds', max_width);
%!  s1 = cellfun(@(v) sprintf(fmt, v), C(:,2), 'uni', 0);
%!  expect = sprintf('%s\n', strjoin(s1, sprintf('\n')));
%!  fut = FUT();
%!  result = fut(C(:,1), test_opts_0());
%!  assert(expect, result);


%% todo: Make output use '×' instead of 'x'. Non-trivial though,
%% Octave and MATLAB handles that character differently.
%% 
%!test % Cell row array with different types
%!  %%    Input         Expected element
%!  C = { ...
%!        1             '1'            ; ...
%!        true          'true'         ; ...
%!        false         'false'        ; ...
%!        'a'           quote('a')     ; ...
%!        struct()	'[1x1 struct]' ; ...
%!        []            '[]'           ; ...
%!        {}            '{}'           ; ...
%!      };
%!  s1 = cellfun(@(v) sprintf('%-4s', v), C(:,2), 'uni', 0);
%!  expect = sprintf('%s\n', strjoin(s1, ' '));
%!  fut = FUT();
%!  result = fut(C(:,1)', test_opts_0());
%!  assert(expect, result);


%!function compare_row_strings(expect, result)
%!   s = [expect; result];
%!   compare = char(double('0') + (s(1,:) == s(2,:)))
%!   disp([s; compare; '-'])
%! end

%!test % Single element and row suffix
%!  C = { 1 };
%!  opts = test_opts_0('row_separator', '', 'row_suffix', ' - suffix');
%!  expect = ['1   ' opts.row_suffix];
%!  result = cell_to_char(C, opts);
%!  % compare_row_strings(expect, result);
%!  assert(expect, result);


%!error<Not enough input arguments>FUT()()
