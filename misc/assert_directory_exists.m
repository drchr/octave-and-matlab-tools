%% Convenience function to assert that a folder exists.
%
% See Octave test cases at the end for some examples.

% © 2017 Christian Ridderström <chr@kth.se>. Licensed under LGPL v3 or later.
function assert_directory_exists(dir, varargin)
  if ~exist('dir', 'var')
    error('Expecting at least one argument, got none');
  end
  if numel(varargin) == 0
    assert(exist(dir, 'dir') == 7, ...
	   'Expected folder not found: ''%s''', dir);
  else
    assert(exist(dir, 'dir') == 7, varargin{:});
  end
end


%%% Section with test cases - mostly only smoke testing
%
% Convenience function to refer to the function under test (FUT).
%!function fut = FUT() fut = @assert_directory_exists; end


%!test
%!  fut = FUT();
%!  fut('.');


%!error <Expecting at least one argument, got none> FUT()();

%!error <Expected folder not found: 'non-existing-dir'> FUT()('non-existing-dir');
