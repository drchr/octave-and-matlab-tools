function y = fold_right(operator, operands, identity_element)
%%% fold_right() - Right-associatively apply the binary 'operator' to
%%% 'operands', folding the vector of 'operands'.
%
% The (optional) 'identity_element' is returned if and only if the
% 'operands' is empty. 
%
%!demo
%!  fold_right(@and, [true true false])
%!  # Does 'and()' over vector of booleans, equivalent to 'all()'
%
%!demo
%!  fold_right(@plus, [1 3 4])
%!  fold_right(@plus, {1 3 4})
%!  # Sum elements of an array (or a cell array)
%
%!demo
%!  fold_right(@plus, [], 0)
%!  # Illustrate returning the identity element  
%
%!demo
%!  fold_right(@(a,b) [a ' ' b], {'hello' 'world'})
%!  # Joining strings
%

  narginchk(2, 3);

  if isempty(operands)
    assert(exist('identity_element', 'var') == 1, ...
           'No ''%s'' provided at the same time as ''%s''', ...
           'identity_element', 'operands');
    y = identity_element;
    return
  end

  %% Todo: Consider supporting operations on e.g. columns of a matrix,
  % although I don't have a good use case yet.
  assert(isvector(operands), ...
         'The ''%s'' argument is not a row- or col. vector', ...
         'operands');

  if iscell(operands)
    y = operands{end};
    for Operand = reshape(operands((end-1):-1:1), 1, [])
      operand = Operand{1};
      y = operator(operand, y);
    end
  else
    y = operands(end);
    for operand = reshape(operands((end-1):-1:1), 1, [])
      y = operator(operand, y);
    end
  end
end


%%% Section with (Octave) test cases
%
% Convenience function to refer to the function under test (FUT).
%!function fut = FUT() fut = @fold_right; end


%!test # Test 1 - plus
%!  fut = FUT();
%!  op = @plus;
%!  assert(fut(op, [], 0),   0);
%!  assert(fut(op, [1]),     1);
%!  assert(fut(op, [1 2]),   3);
%!  assert(fut(op, [1 2 3]), 6);
%!  assert(fut(op, {}, 0),   0);
%!  assert(fut(op, {1}),     1);
%!  assert(fut(op, {1 2}),   3);
%!  assert(fut(op, {1 2 3}), 6);


%!test # Test 2 - multiply
%!  fut = FUT();
%!  assert(fut(@(a,b) a*b, [2 3 4]), 24);


%!test # Test 3 - string concatenation
%!  fut = FUT();
%!  op = @(a,b) [a b];
%!  assert(fut(op, [], ''), '');


%!test # Test 4 - passing in a cell array
%!  fut = FUT();
%!  op = @(a,b) [a b];
%!  assert(fut(op, {}, ''), '');


%!test # Test 5 - operating on characters
%!  fut = FUT();
%!  op = @(a,b) [b a];
%!  assert(fut(op, 'abcd'), 'dcba');


%!test # Test 6 - power
%!  fut = FUT();
%!  op = @(a,b) a^b;
%!  assert(fut(op, [2 3 4]), 2^(3^4));


%!error<not enough input arguments>FUT()()

%!error<not enough input arguments>FUT()(1)

%!error<too many input arguments>FUT()(1,2,3,4)

%!error<No 'identity_element' provided at the same time as 'operands'>
%!     FUT()(@plus, [])

%!error<The 'operands' argument is not a row- or col. vector>
%!     FUT()(@plus, [1 2; 3 4])
