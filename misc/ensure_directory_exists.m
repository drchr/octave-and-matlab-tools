%% Convenience function to ensure a folder exists.
% If the folder is not found, this function attempts to create it.
%
% Throws an error if unable to create the folder.

% © 2017 Christian Ridderström <chr@kth.se>. Licensed under LGPL v3 or later.
function created_dir = ensure_directory_exists(dir)
  if ~exist(dir, 'dir')
    mkdir(dir);
    assert(exist(dir, 'dir') == 7, ...
	   'Unable to create folder ''%s''', dir);
    created_dir = true;
  else
    created_dir = false;
  end
end



%%% Section with test cases - mostly smoke testing
%
% Convenience function to avoid modifying tests if renaming the FUT.
% (FUT = function under test).
%
%!function fut = FUT() fut = @ensure_directory_exists; end


%!test
%!  fut = FUT();
%!  assert(fut('.'), false);
