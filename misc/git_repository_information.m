%% Get Git repository details, e.g. commit hash and branch name.
%
% Use 'system()' to call 'git' in order to get repository details
% such as the commit hash and branch.
%
% Caveat: Your system must be able to execute 'git' from within
% Octave/MATLAB via 'system()'
%
%!demo
%  info = git_repository_information()
%  % Get Git info from this folder
%
%  repo_dir = fullfile('..');
%  info = git_repository_information(repo_dir);
%
% Caveat: Calling 'system()' is somewhat slow, don't use this
% in performance critical code sections.
% Further, script is WIP - interface may change.

% © 2017 Christian Ridderström <chr@kth.se>. Licensed under LGPL v3 or later.
function info = git_repository_information(repository_dir)
  narginchk(0, 1);
  default = @use_default_if_variable_is_undefined_or_empty;
  repository_dir = default('repository_dir', pwd());

  assert(isdir(repository_dir), ...
         'Arg. ''%s'' must be a Git repository directory', 'repository_dir');

  old_dir = pwd();
  try
    cd(repository_dir);
    info.status_output = git_status();
    %% todo: Consider better way of setting n:o symbols in short hash
    info.hash = info.status_output.branch_oid(1:6);
    info.branch = info.status_output.branch_head;

    cd(old_dir)

  catch ME
    cd(old_dir)
    rethrow(ME);
  end
end


function status_output = git_status()
  s = system_wrapper('git status --porcelain=v2 --branch');

  C = strsplit(s, sprintf('\n'));

  header_indices = cellfun(@(s) strncmp(s, '# ', 2), C);
  headers = C(header_indices);
  status_output.branch_oid = header_value(C, 'branch.oid');
  status_output.branch_head = header_value(C, 'branch.head');
  status_output.branch_upstream = header_value(C, 'branch.upstream');
  status_output.branch_ab = header_value(C, 'branch.upstream');

  status_output.non_headers = C(~header_indices);
end


function y = system_wrapper(varargin)
  [status, y] = system(varargin{:});
  if status ~= 0
    fprintf('# Calling ''system()'' with the folowing arguments failed:\n');
    disp(varargin);
    error('Call to system');
  end

  y = remove_trailing_LF(y);
end


function s = remove_trailing_LF(s)
  LF = sprintf('\n');
  n = length(LF);               % 1 on Linux, 2 on Windows :-(
  if strcmp(s(end-(n-1):end), LF)
    s = s(1:end-n);
  end
end


function value = header_value(C, header_token)
  start_positions = cellfun(@(s) strfind(s, header_token), C, 'uniform', 0);
  row_idx = find(~cellfun(@isempty, start_positions));

  assert(numel(row_idx) > 0, 'No header found for ''%s''', header_token);
  assert(numel(row_idx) < 2, 'Multiple headers found for ''%s''', header_token);

  row = C{row_idx};
  [~, remainder] = strtok(row(start_positions{row_idx}:end));
  value = strtrim(remainder);
end



%%% Section with Octave test cases
%
% Convenience function to avoid modifying tests if renaming the FUT.
% (FUT = function under test).
%
%!function fut = FUT() fut = @git_repository_information; end


%!test % Simple smoke test
%!  fut = FUT();
%!  fut();

%!error<narginchk: too many input arguments>FUT()('.', 2)
