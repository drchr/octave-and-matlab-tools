%% Convenience class to create a timer that measures and then prints
% a duration, typically the execution of a function.
%
%!demo
%!  tmr = tic_toc_timer('Short delay'); pause(0.1); clear tmr
%!  # Basic case. The 'clear' command is not needed inside a function.
%
%!demo
%!  tmr = tic_toc_timer('# Start X\n', [], '# End X'); pause(0.1); clear tmr
%!  # Split "start"- and "done" message into separate lines.
%
%!demo
%!  tmr = tic_toc_timer({'Run %d' 10}, 'ms'); pause(0.1); clear tmr
%!  # Give start message as cell array, to pass options to printf().
%!  # Also, use scale factor to show duration in milliseconds
%
%!demo
%!  tmr = tic_toc_timer('Short delay', 'us'); pause(0.1); clear tmr
%!  # Use 'us' as scale factor get duration in µs.
%
%!demo
%!  tmr = tic_toc_timer('Short delay', [], {'%d/%d completed' 5 5}); pause(0.1); clear tmr
%!  # Use custom "done" message, with options to printf() command.
%
%!demo
%!  tmr = tic_toc_timer('', [], 'Done with XXX'); pause(0.1); clear tmr
%!  # Only show the "done" message.
%
%!demo
%!  tmr = tic_toc_timer('Several delays', []);
%!  pause(0.1); fprintf('%.1f s … ', tmr.elapsed());
%!  pause(0.2); fprintf('%.1f s … ', tmr.elapsed());
%!  pause(0.1); clear tmr
%!  # Use .elapsed() to see how much time that has currently elapsed.
%
% Caveat / note [1]:
% The implementation is currently limited by restrictions in Octave.
% Make the class inherit from 'handle', and it'll then be possible
% to e.g. cancel the display of the 'done' message after the timer
% has been started.
  
% © 2017 Christian Ridderström <chr@kth.se>. Licensed under LGPL v3 or later.
classdef tic_toc_timer  % < handle  % See [1]
  properties
    tic                         % Timepoint at creation (from tic())
    done_message = 'done'       % Msg shown at end of elapsed duration
    elapsed_time_fmt = 'in %.1f s' % Format of how to display the elapsed duration
    finish_object      % An 'onCleanup()'-object to do the job
    scale_factor = 1   % Scaling of elapsed time, use 1000 for milliseconds
    cancel = false     % For test cases, to prevent printout
  end

  methods
    function d = tic_toc_timer(start_msg, scale_factor, done_msg, elapsed_time_fmt)
      narginchk(1, 3);
      d.tic = tic();

      if ~isempty(start_msg)
        if ischar(start_msg), start_msg = { start_msg }; end
        fmt = start_msg{1};
        if ~isequal(fmt(end-1:end), '\n'), fmt = [fmt ' … ']; end
        fprintf(fmt, start_msg{2:end});
      end

      if ~exist('scale_factor', 'var'), scale_factor = []; end
      if isempty(scale_factor), scale_factor = 1; end

      if ~exist('elapsed_time_fmt', 'var'), elapsed_time_fmt = []; end
      if isempty(elapsed_time_fmt),
        switch scale_factor
          case {1, 's'}
            [d.scale_factor, d.elapsed_time_fmt] = deal(1,   ' in %.1f s');
          case {1e3, 'ms'}
            [d.scale_factor, d.elapsed_time_fmt] = deal(1e3, ' in %.1f ms');
          case {1e6, 'us'}
            [d.scale_factor, d.elapsed_time_fmt] = deal(1e6, ' in %g µs');
          otherwise
            error('Invalid ''%s''', 'scale_factor');
        end
      else
        [d.scale_factor, d.elapsed_time_fmt] = deal(1, elapsed_time_fmt);
      end

      if ~exist('done_msg', 'var'), done_msg = []; end
      if isempty(done_msg), done_msg = 'done'; end
      if ischar(done_msg), done_msg = { done_msg }; end
      d.done_message = done_msg;

      d.finish_object = onCleanup(@() show_done_message(d));
    end

    function t = elapsed(d)
      t = toc(d.tic);
    end

    function delete(d)
      %% show_done_message(d); % See [1]
    end

    function show_done_message(d)
      if d.cancel, return; end
      
      fprintf([d.done_message{1} d.elapsed_time_fmt '\n'], ...
              d.done_message{2:end}, ...
              d.scale_factor * toc(d.tic));
    end
  end
end



%%% Section with test cases - not yet implemented
%
% Convenience function to refer to the function under test (FUT).
%!function fut = FUT() fut = @tic_toc_timer; end


%!test # Smoke test
%!  fut = FUT();
%!  tmr = fut('Measure it');
