%% Support wrapper function that checks if a path name is relative.
%
% Rationale: Though Octave has 'is_absolute_path()', there's no direct
% equivalent in MATLAB. Hence this wrapper that's independent.
%
% The implementation does not check if the path name exists.
%
%!demo
%!  is_relative_path('.')
%!  # A relative path
%
%!demo
%!  if isunix(), is_relative_path('/home/chr'), end
%!  if ispc(), is_relative_path('c:\dummy'), end
%!  # An absolute path
%
% Caveat: Implementation and results are platform dependent, so on Linux
% the path name 'C:\Users/chr' is considered relative path.
% Similarly, on Windows, the path name '/home/chr' is considered relative.
% This could possibly be considered a bug.
%
% See also convert_to_absolute_path.m

% © 2017 Christian Ridderström <chr@kth.se>. Licensed under LGPL v3 or later.
function is_relative = is_relative_path(path_name)
  narginchk(1, 1);

  if is_matlab()
    is_relative = ~java.io.File(path_name).isAbsolute();
  else
    is_relative = ~is_absolute_filename(path_name);
  end
end



%%% Section with Octave test cases
%
% Convenience function to avoid modifying tests if renaming the FUT.
% (FUT = function under test).
%
%!function fut = FUT() fut = @is_relative_path; end


%!test % Simple test
%!  fut = FUT();
%!  assert(fut('.'), true)


%!test % Test on some relative paths
%!  fut = FUT();
%!  path_names = { '.' '../dummy_dir' };
%!  results = cellfun(fut, path_names);
%!  assert(all(results))


%!test % Test on some abslute paths
%!  fut = FUT();
%!  if ispc()
%!    path_names = { 'C:\' 'C:\Users\chr' };
%!  else
%!    path_names = { '/' '/home/chr' };
%!  end
%!  results = cellfun(fut, path_names);
%!  assert(~any(results))


%!error<not enough input arguments>FUT()()
