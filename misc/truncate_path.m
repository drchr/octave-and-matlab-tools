%% Truncate a path for display within a limited space.
%
% This function tries to cleverly truncate a path, specified through
% it's path components, so it fits within a limited width.
%
% The first argument must be a 'cellstr' with path components.
% The second argument (default is 72 characters) is the maximum allowed
% width for the joined path.
% The third argument (default is 'filesep()') is used to join path components.
%
%!demo
%!  separator = '/';
%!  path_cmps = strsplit('abcde/12345/ABCDE/abcdef', separator);
%!  truncate_path(path_cmps, 20, separator)
%!  # Remember to add examples here

% © 2017 Christian Ridderström <chr@kth.se>. Licensed under LGPL v3 or later.
function s = truncate_path(path_components, max_width, separator)
  narginchk(1, 3);
  if ~exist('max_width', 'var'), max_width = 72; end
  if ~exist('separator', 'var'), separator = filesep(); end
  assert(iscellstr(path_components));
  if isempty(path_components)
    s = '';
    return
  end
  assert(isvector(path_components));
  assert(isscalar(max_width) && isnumeric(max_width));

  N = numel(path_components);
  L_sep = length(separator);
  assert(N == 1 || max_width - L_sep >= 3, ...
         'Too small max width, %d, for more than one component', max_width);
  assert(N ~= 1 || max_width >= 2, ...
         'Too small max width, %d, for one component', max_width);

  Ls = cellfun(@length, path_components);
  max_wo_separators = max_width - (N-1) * L_sep;

  if sum(Ls) <= max_wo_separators
    s = strjoin(path_components, separator);
  elseif N == 1
    s = truncate_with_ellipsis(path_components{1}, max_width);
  elseif max_wo_separators < 4
    s = ['…' separator, ...
         truncate_with_ellipsis(path_components{end}, max_width - 1 - L_sep)]
  else
    max_Ws = num2cell(allocate_widths(N, Ls, max_wo_separators));
    pc = cellfun(@truncate_with_ellipsis, ...
                 reshape(path_components, 1, []), max_Ws, 'unif', 0);
    s = strjoin(pc, separator);
  end
end


function max_Ws = allocate_widths(N, Ls, max_wo_separators)
  allotment = ones(1, N);
  allotment(end) = allotment(end) + 2; % Larger fraction for last component
  allotment(1) = allotment(1) + 1; % Tiny bit more for 1st component

  %% Allocate max size for each component based on allotment fractions
  max_Ws = floor(max_wo_separators * allotment / sum(allotment));

  %% Find components smaller than their allowed size
  margins = max(max_Ws - Ls, 0);
  max_Ws = max_Ws - margins;    % Reduce 'max_Ws' for such components
  margin = max_wo_separators - sum(max_Ws);

  %% Distribute any extra space to the components, starting with the last
  ii = numel(Ls);
  while margin > 0
    if max_Ws(ii) < Ls(ii)
      max_Ws(ii) = max_Ws(ii) + 1;
      margin = margin - 1;
    end
    ii = ii + 1;
    if ii > N, ii = 1; end
  end
end


function [y, margin] = truncate_with_ellipsis(s, max_width)
  len = length(s);
  if len <= max_width
    margin = max_width - len;
    y = s;
  else
    margin = 0;
    y = [s(1:max_width-1) '…'];
  end
end



%%% Section with Octave test cases
%
% Convenience function to avoid modifying tests if renaming the FUT.
% (FUT = function under test).
%
%!function fut = FUT() fut = @truncate_path; end


%!test % Single short component is left unchanged
%!  fut = FUT();
%!  arg1 = { '12345678' };
%!  arg2 = 8;
%!  expect = strjoin(arg1, '/');
%!  result = fut(arg1, arg2);
%!  assert(expect, result);


%!test % Single too long component is truncated
%!  fut = FUT();
%!  arg1 = { '12345678' };
%!  arg2 = 7;
%!  expect = [arg1{1}(1:6) '…'];
%!  result = fut(arg1, arg2);
%!  assert(expect, result);


%!test % Less than threshold results in no truncation
%!  fut = FUT();
%!  arg1 = { 'aaaaaaaa' 'bbbbbbbb' };
%!  expect = strjoin(arg1, '/');
%!  result = fut(arg1, 16+1);
%!  assert(expect, result);


%!test % Prioritize last component when too long
%!  fut = FUT();
%!  arg1 = { 'abcdefgh' '12345678' };
%!  expect = 'abcdef…/12345678';
%!  result = fut(arg1, 16);
%!  assert(expect, result);

%!error<not enough input arguments>FUT()()
