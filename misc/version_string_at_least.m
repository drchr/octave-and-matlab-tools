%% Support function to compare if a first ("current") version string
% is at least equal to that of a second ("required") version string,
% e.g. that "1.0.2" >= "1.0".
%
% Typical use case would be to check if the (current) version of a package
% is sufficient (i.e. >= the required version).
%
%!demo
%!  version_string_at_least('2.0', '1.0') 
%!  # The current ver. 2.0 is more then the required ver. 1.0
%
%!demo
%!  version_string_at_least('2.0', '2.0.1') 
%!  # The current ver. 2.0 does not meet the required ver. 2.0.1
%
% See also is_version_string.

% © 2017 Christian Ridderström <chr@kth.se>. Licensed under LGPL v3 or later.
function is_greater_or_equal = version_string_at_least(current_version, required_version)
  narginchk(2, 2);

  assert(is_version_string(current_version), ...
         'Expected 1st arg. to be a version string');
  assert(is_version_string(required_version), ...
         'Expected 2nd arg. to be a version string');

  [v1, n1] = to_numbers(current_version);
  [v2, n2] = to_numbers(required_version);
  if n1 < n2
    v1(n1+1:n2) = 0;
  elseif n1 > n2
    v2(n2+1:n1) = 0;
  end

  for ii = 1:numel(v1)
    if v1(ii) > v2(ii), is_greater_or_equal = true; return; end
    if v1(ii) < v2(ii), is_greater_or_equal = false; return; end
  end
  is_greater_or_equal = true;
end


function [c, n] = to_numbers(s)
  c = cellfun(@str2num, strsplit(s, '.'));
  n = numel(c);
end


%%% Section with Octave test cases
%
% Convenience function to avoid modifying tests if renaming the FUT.
% (FUT = function under test).
%
%!function fut = FUT() fut = @version_string_at_least; end


%!test % Simple case
%!  fut = FUT();
%!  assert(fut('2', '1'), true);


%!test % Various version comparisons expected to be true
%!  fut = FUT();
%!  results = cellfun(fut, {'0'  '0.1'  '1.0'  '1.0.1' '1.0'  '1.0.1'}, ...
%!                         {'0'  '0.0'  '1.0'  '1.0'   '0.9'  '1.0.0'} );
%!  assert(all(results));



%!test % Version comparisons of different lengths
%!  fut = FUT();
%!  args = { {'0.2' '0.2.1' '1.2.3.4'}, ...
%!           {'0'   '0'     '1.2.0'  } };
%!  results1 = cellfun(fut, args{:} );
%!  assert(all(results1));
%!  
%!  results2 = cellfun(fut, args{[2 1]} );
%!  assert(~any(results2));


%!test % Various version comparisons expected to be false
%!  fut = FUT();
%!  results = cellfun(fut, {'0'  '0.1'  '1.0'  '1.0'   '1.0.1'}, ...
%!                         {'1'  '0.2'  '1.1'  '1.0.1' '1.0.2'} );
%!  assert(~any(results));


%!error<not enough input arguments>FUT()()

%!error<not enough input arguments>FUT()('1')

%!error<Expected 1st arg. to be a version string>FUT()(1, 1)

%!error<Expected 2nd arg. to be a version string>FUT()('1', 1)
