%% Support function to return a struct with info about a package (Octave)
% or a toolbox (MATLAB). Primarily used to get the version.
%
% Note: As a special (hard coded) case, the info for this package
% can also be returned, e.g. to check it's version.
%
% The result is based on the following:
%   pkg('describe', <package>)     % For Octave
%   ver(<package>)                 % For MATLAB
% and the result is placed in a struct with these fields
%   .name
%   .version
%   .description                   % Empty in MATLAB
%
% If no matching package/toolbox is found, an empty struct is returned.
%
%!demo
%!  info = package_info('io')
%!  # In Octave: Gets struct with info about the package 'io'
%
%!demo
%!  info = package_info('simulink')
%!  # In MATLAB: Gets struct with info about the toolbox 'Simulink'
%
%!demo
%!  info = package_info(which('package_info'));
%!  # Gets struct with info about the package containing this script.
%!  # The info is taken from '../package_info_about_this_package.m'
%
% See also assert_sufficient_package_version.m

% © 2017 Christian Ridderström <chr@kth.se>. Licensed under LGPL v3 or later.
function [info, details] = package_info(package)
  narginchk(1, 1);

  assert(ischar(package), 'Expected a string with a package name');

  if is_octave()
    P = pkg('describe', package);
    details = P{1};
    if isempty(details)
      info = ctor({}, {}, {});
    else
      info = ctor(details.name, details.version, details.description);
    end
  else
    details = ver(package);
    if isempty(details)
      info = ctor({}, {}, {});
    else
      info = ctor(details.Name, details.Version, '');
    end
  end

  if isempty(info) && isequal(package, which(mfilename()))
    S = details_of_this_package(mfilename('fullpath'));
    details = struct('name', S.name, 'version', S.version, ...
                     'description', S.description);
    info = ctor(details.name, details.version, details.description);
  end
end


function S = ctor(name, version, description)
  S = struct('name', name, 'version', version, 'description', description);
end


function [info] = details_of_this_package(package_info_file)
  old_dir = pwd();
  on_cleanup = onCleanup(@() cd(old_dir));

  fcn_name = 'package_info_about_this_package';
  clear('-f', fcn_name);

  package_dir = fullfile(fileparts(package_info_file), '..');
  assert_file_exists(fullfile(package_dir, [fcn_name '.m']));

  cd(package_dir);
  info = feval(fcn_name);
end


%%% Section with Octave test cases
%
% Convenience function to avoid modifying tests if renaming the FUT.
% (FUT = function under test).
%
%!function fut = FUT() fut = @package_info; end


%!test % Simple smoke test
%!  fut = FUT();
%!  fut('io');


%!test % Get info of package 'io'
%!  fut = FUT();
%!  result = fut('io');
%!  assert(result.name, 'io');


%% Note: Test case below must be updated when version of this package changes
%!test % Get info of this package
%!  fut = FUT();
%!  result = fut(which('package_info'));
%!  assert(result.version, '1.8.0');


%!error<not enough input arguments>FUT()()

%!error<Expected a string with a package name>FUT()(1)
