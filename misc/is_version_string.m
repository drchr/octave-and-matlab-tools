%% Support function to check if an argument is a "version string",
% e.g. like "1.0.2".
%
%!demo
%!  is_version_string('1.0')
%!  # This is a version string
%
%!demo
%!  is_version_string('1-0')
%!  is_version_string('.1')
%!  # These are not considered version strings

% © 2017 Christian Ridderström <chr@kth.se>. Licensed under LGPL v3 or later.
function is_version = is_version_string(version)
  narginchk(1, 1);

  is_version = ischar(version) && ~isempty(regexp(version, '^\d+([.]\d+)*$'));
end


%%% Section with Octave test cases
%
% Convenience function to avoid modifying tests if renaming the FUT.
% (FUT = function under test).
%
%!function fut = FUT() fut = @is_version_string; end


%!test % Empty, non-string argument
%!  fut = FUT();
%!  assert(fut([]), false);


%!test % Various version strings
%!  fut = FUT();
%!  results = cellfun(fut, {'0', '0.1', '1.0', '1.0.1'});
%!  assert(all(results));


%!test % Various non-version arguments
%!  fut = FUT();
%!  results = cellfun(fut, {1, [], '', '.1', '1.0.', '1-0', 'R1', '1..1'});
%!  assert(~any(results));


%!error<not enough input arguments>FUT()()
