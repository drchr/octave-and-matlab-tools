%% Convenience function to assert that 'exist(..., 'file') == 4,
% to make it convenient to assert that a Simulink model or library
% with the given name exists on the MATLAB search path
% (as defined by 'exist()').
%
% Example:
%   %% Check for 'top_model', no need to include file extension.
%   assert_simulink_model_or_library_exists('top_model');
%
%   %% Check with a custom error message
%   model = 'name_of_some_Simulink_library';
%   assert_simulink_model_or_library_exists(model, ...
%                                           'Library %s not found', model);
%
% Caveat: In Octave, the assert is not performed.

% © 2017 Christian Ridderström <chr@kth.se>. Licensed under LGPL v3 or later.
function assert_simulink_model_or_library_exists(model, varargin)
  if ~exist('model', 'var')
    error('Expecting at least one argument, got none');
  end
  if is_matlab()
    if numel(varargin) == 0
      assert(exist(model, 'file') == 4, ...
	     'Simulink model or library not found: ''%s''', model);
    else
      assert(exist(model, 'file') == 4, varargin{:});
    end
  end
end



%%% Section with test cases
%
% Convenience function to refer to the function under test (FUT).
%!function fut = FUT() fut = @assert_simulink_model_or_library_exists; end


%!error <Expecting at least one argument, got none> FUT()();
