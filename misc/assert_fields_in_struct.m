%% Assert that struct 'S' contains the 'expected_fields'.
% If not, an error is thrown and e.g. the names of missing fields are shown.
% The argument 'name', used in error messages, refers to the struct's name.
%
% Note: If 'expected_fields' is a struct with any field that is
% a struct, then those substructs are also checked against 'S'.
%
% See the Octave test cases at the end for usage examples.

% © 2017 Christian Ridderström <chr@kth.se>. Licensed under LGPL v3 or later.
function assert_fields_in_struct(S, expected_fields, name)
  narginchk(2, 3);
  assert(isstruct(S), 'First argument must be a struct');
  if ~exist('name', 'var'), name = inputname(1); end

  if isstruct(expected_fields)
    E = expected_fields;
    for Field = fieldnames(E)'
      field = Field{1};
      if isstruct(E.(field))
	assert_fields_in_struct(S.(field), E.(field), [name '.' field]);
      end
    end
    expected_fields = fieldnames(expected_fields)';
  end
  assert(iscellstr(expected_fields), 'Second argument must be a cellstr');
  assert(ischar(name) && ~isempty(name), ...
	 'Third argument must be a non-empty string');

  actual_fields = fieldnames(S)';

  a = setdiff(expected_fields, actual_fields);
  b = setdiff(actual_fields, expected_fields);

  show_missing_and_unexpected_fields = nargout == 0;

  if ~isempty(a) && show_missing_and_unexpected_fields
    show('Missing the following expected fields fields in ''%s'':\n', name, a);
  end

  if ~isempty(b) && show_missing_and_unexpected_fields
    show('Found the following unexpected fields fields in ''%s'':\n', name, b);
  end

  assert(isempty(a), 'Missing fields in argument ''%s'', see above.', name);
  assert(isempty(b), 'Extra fields in argument ''%s'', see above.', name);
end


function show(fmt, name, fields)
  fprintf(fmt, name);
  for F = fields
    fprintf('\t%s\n', F{1});
  end
end


%%% Section with test cases - mostly only smoke testing
%
% Convenience function to refer to the function under test (FUT).
%!function fut = FUT() fut = @assert_fields_in_struct; end

%% For the following test cases, they pass if there are no errors.

%!test % Test empty struct and implicit name - ok if no errors
%!  S = struct();
%!  args = { S {} };
%!  fut = FUT();
%!  fut(S, {});


%!test % Test empty struct and explicit name - ok if no errors
%!  S = struct();
%!  args = { S {} 'dummy_name' };
%!  fut = FUT();
%!  fut(args{:});
%!  fut(S, {});


%!test % Testing struct with a single field
%!  S = struct('a', 1);
%!  args = { S {'a'} 'dummy_name' };
%!  fut = FUT();
%!  fut(args{:});


%!test % Testing struct against another struct
%!  S = struct('a', 1, 's', struct('b', []));
%!  args = { S S 'dummy_name' };
%!  fut = FUT();
%!  fut(args{:});


%!error<not enough input arguments>FUT()()

%!error<not enough input arguments>FUT()(1)

%!error<too many input arguments>FUT()(1,2,3,4)

%!error<First argument must be a struct>FUT()([], {}, 'dummy')

%!error<Second argument must be a cellstr>FUT()(struct(), 1, 'dummy')

%!error<Third argument must be a non-empty string>FUT()(struct(), {}, 1)

%!error<Missing fields in argument 'dummy', see above.>a = FUT()(struct(), {'b'}, 'dummy')

%!error<Extra fields in argument 'dummy', see above.>a = FUT()(struct('a',0), {}, 'dummy')
