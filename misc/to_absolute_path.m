%% Support function that converts a relative path name into an
% absolute, and leaves an already absolute path name unchanged.
%
% The path destination (dir or file) does not have to exist.
% The function is idempotent.
%
% The implementation checks if the 'path' argument is relative,
% and if so prepends 'pwd()' to it, otherwise it's returned
% unmodified.
%
% Caveat: MATLAB implementation requires Java.
%
%!demo
%!  to_absolute_path('.')
%!  # Remember to add examples here
%
% See also is_relative_path.m

% © 2017 Christian Ridderström <chr@kth.se>. Licensed under LGPL v3 or later.
function path_name = to_absolute_path(path_name, reference_directory)
  narginchk(1, 2);
  if ~exist('reference_directory'), reference_directory = pwd(); end

  assert(ischar(path_name), 'Expected 1st arg. to be a char array');
  assert(~isempty(path_name), 'Expected non-empty 1st arg.');

  if is_relative_path(path_name)
    path_name = fullfile(reference_directory, path_name);
  end
end



%%% Section with Octave test cases
%
% Convenience function to avoid modifying tests if renaming the FUT.
% (FUT = function under test).
%
%!function fut = FUT() fut = @to_absolute_path; end


%!function [path_names, is_relative] = data(is_unix)
%!  path_names = { '.'  '..' '../a/b'  '..\a\b'   '/home/chr'  'C:\Users\chr'};
%!  if is_unix
%!    is_relative = [1   1    1         1           0            1];
%!  else
%!    is_relative = [1   1    0         1           1            0];
%!  end


%!test % Test on some paths
%!  [path_names, is_relative] = data(isunix());
%!  expected = cellfun(@(x) fullfile(pwd(), x), path_names, 'uniform', 0);
%!  expected(~is_relative) = path_names(~is_relative);
%!  fut = FUT();
%!  results = cellfun(fut, path_names, 'uniform', 0);
%!  assert(results, expected)


%!test % Test on some paths with a specified 'reference_directory'
%!  ref_dir = '/abs';
%!  [path_names, is_relative] = data(isunix());
%!  expected = cellfun(@(x) fullfile(ref_dir, x), path_names, 'uniform', 0);
%!  expected(~is_relative) = path_names(~is_relative);
%!  fut = FUT();
%!  results = cellfun(@(p) fut(p, ref_dir), path_names, 'uniform', 0);
%!  assert(results, expected)


%!error<not enough input arguments>FUT()()

%!error<Expected 1st arg. to be a char array>FUT()(1)

%!error<Expected non-empty 1st arg.>FUT()('')

