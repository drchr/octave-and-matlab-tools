%% Extract variable references from variable modification data (VMD).
%
% This is a support function to variable modification functions.
%
% The VMD must be a cell row, where each element is a modification
% "factor".
% For each modification "factor", this function extracts the variable
% references as a cell column.
% The cell columns corresponding to the different factors are returned
% as a cell row.
%
% See also VMD_extract_value_vectors() and combine_variable_modifications()

% © 2017 Christian Ridderström <chr@kth.se>. Licensed under LGPL v3 or later.
function references = VMD_extract_variable_references(VMD)
  narginchk(1, 1);
  references = cellfun(@reference, VMD, 'uni', 0);
end


function y = reference(vmd_n)
  if size(vmd_n, 2)
    y = vmd_n(:, 1);
  else
    y = {};
  end
end


%%% Section with Octave test cases - must be run from dir private/
%
% Convenience function to avoid modifying tests if renaming the FUT.
% (FUT = function under test).
%
%!function fut = FUT() fut = @VMD_extract_variable_references; end

%% Example VMD with only a single factor without any rows
%!function VMD = VMD0()
%!  vmd_0 = { };
%!  VMD = { vmd_0 };
%! end


%% Example VMD with single factor, only one row in the factor
%!function VMD = VMD1()
%!  vmd_1 = { { 'v1' } 1 2 };
%!  VMD = { vmd_1 };
%! end


%% Example VMD with two factors, two rows in each
%!function VMD = VMD2()
%!  vmd_1 = { { 'v1' }      1 2; ...
%!            { 's2' 'f1' } 3 4 };
%!  vmd_2 = { { 'v3' }  5 6 7; ...
%!            { 'v4' }  8 9 0};
%!  VMD = { vmd_1, vmd_2 };
%! end


%% todo: Unsure what to return here
%!test % Empty input argument
%!  fut = FUT();
%!  VMD = VMD0();
%!  expect = { { } };
%!  result = fut(VMD);
%  assert(expect, result);


%!test % Normal behaviour, one factor in the VMD
%!  fut = FUT();
%!  VMD = VMD1();
%!  expect = { ...
%!             { ...
%!               { 'v1' } ...
%!             }
%!           };
%!  result = fut(VMD);
%!  assert(expect, result);


%!test % Normal behaviour, two factors in the VMD
%!  fut = FUT();
%!  VMD = VMD2();
%!  expect = { ...
%!             { ...
%!               { 'v1'      } ; ...
%!               { 's2' 'f1' }   ...
%!             }, ...
%!             {  ...
%!               { 'v3' } ; ...
%!               { 'v4' }   ...
%!             } ...
%!           };
%!  result = fut(VMD);
%!  assert(expect, result);


%!error<not enough input arguments>FUT()()

