%% Check if cell array represents a variable modification "factor".
%
% Using the term "factor" as combine_variable_modifications() can
% generate the "product" of several factors.
%
%!demo
%!  factor = { };
%!  assert(true, VMD_is_factor(factor));
%!  # An empty factor is considered a factor
%
% See also apply_variable_modifications() and VMD_is_variable_reference()

% © 2017 Christian Ridderström <chr@kth.se>. Licensed under LGPL v3 or later.
function is_factor = VMD_is_factor(factor)
  narginchk(1, 1);
  if ~iscell(factor)
    is_factor = false;          % Must be cell array
  elseif isempty(factor)
    is_factor = true;           % Empty cell is considered a factor
  elseif size(factor, 2) < 2
    %% Need variable references and at least one value vector
    is_factor = false;  
  else
    is_factor = all(cellfun(@VMD_is_variable_reference, factor(:, 1)));
  end
end



%%% Section with Octave test cases - must be run from dir private/
%
% Convenience function to avoid modifying tests if renaming the FUT.
% (FUT = function under test).
%
%!function fut = FUT() fut = @VMD_is_factor; end


%!test % Simple check on empty argument
%!  fut = FUT();
%!  factor = {};
%!  result = fut(factor);
%!  expect = true;
%!  assert(true, result);


%!test % Check simple valid argument
%!  fut = FUT();
%!  factor = { { 'v1' } 1 2 };
%!  result = fut(factor);
%!  expect = true;
%!  assert(expect, result);


%!test % Check simple invalid argument
%!  fut = FUT();
%!  factor = { 1 1 2 };
%!  result = fut(factor);
%!  expect = false;
%!  assert(expect, result);


%!test % Check several valid examples
%!  fut = FUT();
%!  factor_1 = { { 'v1' } 1 };
%!  factor_2 = { { 'v1' } 1 2 };
%!  factor_3 = { { 'v1' } 1 2 ; ...
%!               { 'v2' } 3 4 };
%!  factors = { factor_1  factor_2  factor_3 };
%!  expect = true(size(factors));
%!  result = cellfun(fut, factors);
%!  assert(expect, result);


%!test % Check several invalid examples
%!  fut = FUT();
%!  not_factor_1 = { 1 };
%!  not_factor_2 = { { '' } 1 };
%!  not_factor_3 = { { 'v1' } };
%!  not_factor_4 = { { 'v1 ' } 1 2 ; ...
%!                   { 'v2'  } 3 4 };
%!  factors = { not_factor_1  not_factor_2  not_factor_3  not_factor_4 };
%!  expect = false(size(factors));
%!  result = cellfun(fut, factors);
%!  assert(expect, result);


%!error<not enough input arguments>FUT()()

