%% Check if argument is a variable reference, as used by variable modification data (VMD).
%
%!demo
%!  var_ref_1 = { 'v1' };
%!  var_ref_2 = { 'S1' 'f1' };
%!  var_refs = { var_ref1 var_ref2 };
%!  assert(all(cellfun(@VMD_is_variable_reference, var_refs)));
%!  # Examples of variable references
%
%!demo
%!  var_ref_1 = { };
%!  assert(false, VMD_is_variable_reference(var_ref_1));
%!  # An empty cell(str) is not considered a variable references
%
% See also apply_variable_modifications()

% © 2017 Christian Ridderström <chr@kth.se>. Licensed under LGPL v3 or later.
function is_ref = VMD_is_variable_reference(reference)
  narginchk(1, 1);
  if ~iscellstr(reference) || isempty(reference)
    is_ref = false;
  else
    is_ref = all(cellfun(@isvarname, reference));
  end
end



%%% Section with Octave test cases - must be run from dir private/
%
% Convenience function to avoid modifying tests if renaming the FUT.
% (FUT = function under test).
%
%!function fut = FUT() fut = @VMD_is_variable_reference; end


%!test % Simple tests for things that aren't variable references
%!  fut = FUT();
%!  not_refs = { ...
%!               {}, ...
%!               [], ...
%!               1, ...
%!               { 1 }, ...
%!               { '' }, ...
%!               { 'a a' } ...
%!             };
%!  expect = false(size(not_refs));
%!  result = cellfun(fut, not_refs);
%!  assert(expect, result);


%!test % Some variable references
%!  fut = FUT();
%!  refs = { ...
%!           { 'v1' }, ...
%!           { 'S1' 'f1' }, ...
%!           { 'S1' 'f1' 'f11' } ...
%!         };
%!  expect = true(size(refs));
%!  result = cellfun(fut, refs);
%!  assert(expect, result);


%!error<not enough input arguments>FUT()()
