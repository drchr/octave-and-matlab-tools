%% Vertically concatenate elements of cell array 'arg'.
%
% Rationale: The purpose of this function is to avoid creating an
% intermediate variable, e.g. when wanting to concatenate non-uniform
% results of a call to 'cellfun'.
%
%!demo
%!  vert_cat(num2cell(1:3))
%!  assert(isequal(ans, [1; 2; 3]));
%!  # Returns array [ 1; 2; 3 ]
%
%!demo
%!  a = { num2cell(1:3)' };
%!  result = vert_cat(a);
%!  expect = { 1; ...
%!             2; ...
%!             3 };
%!  assert(expect, result);
%!  # Returns cell column
%
%!demo
%!  C = { { 1 2 } 'a'; ...
%!        { 3 4 } 'b'; ...
%!        { 5 6 } 'c'};
%!  result = vert_cat(C(:, 1));
%!  expect = { 1 2 ; ...
%!             3 4 ; ...
%!             5 6 };
%!  assert(isequal(expect, result));
%!  # Unpack cells in 1st column
%
%!demo
%!  C = { { 1 2 }; ...
%!        { 3 4 }; ...
%!        { 5 6 } };
%!  result = vert_cat(cellfun(@(c) c(2), C, 'uniform', 0));
%!  expect = { 2 ; ...
%!             4 ; ...
%!             6 };
%!  assert(isequal(expect, result));
%!  # Extract first elements of cells into a cell column

% © 2017 Christian Ridderström <chr@kth.se>. Licensed under LGPL v3 or later.
function y = vert_cat(arg)
  narginchk(1, 1);
  assert(iscell(arg));
  y = vertcat(arg{:});
end



%%% Section with Octave test cases - must be run from dir private/
%
% Convenience function to avoid modifying tests if renaming the FUT.
% (FUT = function under test).
%
%!function fut = FUT() fut = @vert_cat; end


%!test % Simple smoke test
%!  fut = FUT();
%!  fut({});


%!test % demo 1
%!  fut = FUT();
%!  arg = { num2cell(1:3)' };
%!  expect = { 1; 2; 3 };
%!  result = fut(arg);
%!  assert(expect, result);


%!error<not enough input arguments>FUT()()
