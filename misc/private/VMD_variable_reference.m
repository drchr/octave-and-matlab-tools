%% Constructor for a variable reference used in variable modifications data (VMD)
%
% This function also partially checks if it's a valid variable reference.
%
% A user would typically use this function to convert e.g. 's1.f1' into
% the canonical cellstr format, i.e.
%      { 's1' 'f1' }.
%
% In case of invalid input, and if the caller does not capture the 'valid'
% output, then this function throws an error.
%
%!demo
%!  [ref, valid] = VMD_variable_reference({ 's1' 'f1'})
%!  assert(true, valid);
%!  # Resulting 'ref' is unchanged compared to the argument
%
%!demo
%!  [ref, valid] = VMD_variable_reference('s1.f1')
%!  assert(true, valid);
%!  # Converts to cellstr format, i.e. { 's1' 'f1' }
%
%!demo
%!  [ref, valid] = VMD_variable_reference('s1.f 1')
%!  assert(false, valid);
%!  # Detects invalid variable name
%
% See the Octave test cases at the end of this function for more examples.
% Note: Running the tests requires being in the dir of this function.

% © 2017 Christian Ridderström <chr@kth.se>. Licensed under LGPL v3 or later.
function [variable_reference, valid] = VMD_variable_reference(reference_args)
  narginchk(1, 1);

  %% If the caller discards the 'valid' result, we may throw errors
  caller_captures_valid = nargout >= 2; 

  if isempty(reference_args),
    [variable_reference, valid] = deal({}, false);
    if caller_captures_valid, return, end
    error('An empty argument is not allowed');
  end

  if iscellstr(reference_args)
    variable_reference = reference_args;
  elseif ischar(reference_args)
    variable_reference = strsplit(reference_args, '.');
  else
    [variable_reference, valid] = deal({}, false);
    if caller_captures_valid, return; end
    error('Invalid argument type: %s', class(reference_args));
  end

  valid = all(cellfun(@isvarname, variable_reference));

  if caller_captures_valid, return; end
  if ~valid
    quote = @(s) ['''' s ''''];
    bad_names = variable_reference(~cellfun(@isvarname, variable_reference));
    name_list = strjoin(cellfun(quote, bad_names, 'unif', 0), ', ');
    ref_parts = strjoin(cellfun(quote, variable_reference, 'unif', 0), ' ');
    error('Invalid parts in variable reference { %s }: %s', ...
          ref_parts, name_list);
  end
end





%%% Section with Octave test cases
%
% Convenience function to avoid modifying tests if renaming the FUT.
% (FUT = function under test).
%
%!function fut = FUT() fut = @VMD_variable_reference; end


%!test % Already in cellstr format
%!  fut = FUT();
%!  arg = { 's1' 'f1' };
%!  expected = { arg  true };
%!  [result{1:2}] = fut(arg);
%!  assert(result, expected);


%!test % String format
%!  fut = FUT();
%!  expected = { {'s1' 'f1'}  true };
%!  [result{1:2}] = fut('s1.f1');
%!  assert(result, expected);


%!test % String format
%!  fut = FUT();
%!  expected = { {'s1' 'f1' 'f11'}   true };
%!  [result{1:2}] = fut('s1.f1.f11');
%!  assert(result, expected);


%!test % Empty arguments result in invalid result
%!  fut = FUT();
%!  arg = { };
%!  expected = { {}  false };
%!  [result{1:2}] = fut(arg);
%!  assert(result, expected);


%!test % Invalid specifier, space in field name
%!  fut = FUT();
%!  expected = { {'s1' 'f 1'}  false };
%!  [result{1:2}] = fut('s1.f 1');
%!  assert(result, expected);


%!error<not enough input arguments>FUT()()

%!error<An empty argument is not allowed>FUT()({})

%!error<Invalid argument type: double>FUT()(1)

%!error<Invalid parts in variable reference { 's1' 'f 1' }: 'f 1'>...
%!     FUT()('s1.f 1')
