%% Check if a cell array representing variable modification data (VMD) is empty.
%
%!demo
%!  vmd_1 = { { 'v1' }  1 };
%!  vmd_2 = { { 'v2' }  2 };
%!  VMD = { vmd_1  vmd_2 };
%!  assert(false, VMD_is_empty(VMD));
%!  # Example of VMD that's not empty
%
%!demo
%!  VMD = { {} };
%!  assert(true, VMD_is_empty(VMD));
%!  # No modification data is still considered modification data

% © 2017 Christian Ridderström <chr@kth.se>. Licensed under LGPL v3 or later.
function is_empty = VMD_is_empty(VMD)
  narginchk(1, 1);
  assert(iscell(VMD), 'Arg. ''%s'' must be a cell', 'VMD');
  empty_VMD = { {} };
  is_empty = isequal(VMD, empty_VMD);
end



%%% Section with Octave test cases - must be run from dir private/
%
% Convenience function to avoid modifying tests if renaming the FUT.
% (FUT = function under test).
%
%!function fut = FUT() fut = @VMD_is_empty; end


%!test % Simple check on empty VMD
%!  fut = FUT();
%!  assert(true, fut({{}}));


%!test % Simple check on non-empty VMD
%!  fut = FUT();
%!  vmd_1 = { { 'v1' } 1 };
%!  VMD = { vmd_1 };
%!  result = fut(VMD);
%!  expect = false;
%!  assert(expect, result);


%!error<not enough input arguments>FUT()()

%!error<Arg. 'VMD' must be a cell>FUT()(1)
