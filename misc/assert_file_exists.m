%% Convenience function to wrap a call to 'exist(..., 'file'),
% making it easy to assert that a file exists (as defined by 'exist()').
%
% See the Octave test cases at the end for usage examples.

% © 2017 Christian Ridderström <chr@kth.se>. Licensed under LGPL v3 or later.
function assert_file_exists(file, varargin)
  if ~exist('file', 'var')
    error('Expecting at least one argument, got none');
  end
  if numel(varargin) == 0
    assert(exist(file, 'file') == 2, ...
	   'File not found: ''%s''', file);
  else
    assert(exist(file, 'file') == 2, varargin{:});
  end
end



%%% Section with test cases
%
% Convenience function to refer to the function under test (FUT).
%!function fut = FUT() fut = @assert_file_exists; end


%!test % Self check that this script file exists
%!  file = 'assert_file_exists.m';
%!  fut = FUT();
%!  fut(file);


%!error <Expecting at least one argument, got none> FUT()();
%!error <File not found: 'non-existing-file-name.dummy'> FUT()('non-existing-file-name.dummy');
