%% Support function to check a struct has no additional fields.
% Typically used to check fields in an options struct.
%
%!demo
%!  S = struct('a', 0, 'c', 3);
%!  expected_fields = { 'a' 'b' 'c' };
%!  assert_no_extra_fields_in_struct(S, expected_fields);
%!  # This is ok, all fields in 'S' are listed in 'expected_fields'
%
%!demo
%!  S = struct('a', 0, 'c', 3);
%!  expected_fields = struct('a', 1, 'b', 1, 'c', 1);
%!  assert_no_extra_fields_in_struct(S, expected_fields);
%!  # This is also ok, the expected fields can be given as a struct
%
% todo: Consider extending functionality to recursively check struct fields.

% © 2017 Christian Ridderström <chr@kth.se>. Licensed under LGPL v3 or later.
function assert_no_extra_fields_in_struct(S, expected_fields)
  narginchk(2, 2);
  assert(isstruct(S), 'First argument must be a struct');
  assert(iscellstr(expected_fields) || isstruct(expected_fields), ...
         'Second argument must be a cellstr or a struct');

  if isstruct(expected_fields)
    expected_fields = fieldnames(expected_fields);
  end

  unexpected_fields = setdiff(fieldnames(S), expected_fields);
  if isempty(unexpected_fields)
    return;
  end

  unexpected_names = strjoin(cellfun(@(s) quote(s), ...
                                     unexpected_fields, 'uni', 0), ...
                             ', ');
  in_name = inputname(1);
  if isempty(in_name)
    in_name = '1st arg.';
  else
    in_name = quote(in_name);
  end 
  error('Unexpected fields in %s: %s', in_name, unexpected_names);
end


function q = quote(s), q = ['''' s '''']; end



%%% Section with Octave test cases
%
% Convenience function to avoid modifying tests if renaming the FUT.
% (FUT = function under test).
%
%!function fut = FUT() fut = @assert_no_extra_fields_in_struct; end


%!test
%!  S = struct('a', 0, 'c', 3);
%!  expected_fields = { 'a' 'b' 'c' };
%!  fut = FUT();
%!  fut(S, expected_fields);


%!test
%!  S = struct('a', 0, 'c', 3);
%!  expected_fields = struct('a', 1, 'b', 1, 'c', 1);
%!  fut = FUT();
%!  fut(S, expected_fields);


%!error<Unexpected fields in 1st arg.: 'a'>FUT()(struct('a',0), {'b'})

%!error<not enough input arguments>FUT()()

%!error<not enough input arguments>FUT()(1)

%!error<First argument must be a struct>FUT()(1, {})

%!error<Second argument must be a cellstr or a struct>FUT()(struct(), 1)


