%% Support/convenience function to check if a specific directory
% is in the MATLAB/Octave path.
%
% See the Octave test cases at the end for usage examples.

% © 2017 Christian Ridderström <chr@kth.se>. Licensed under LGPL v3 or later.
function is_in_path = is_directory_in_path(dir, path_str)
  narginchk(1,2);
  if ~exist('path_str', 'var'), path_str = path(); end;
  C = regexp(path_str, pathsep(), 'Split');
  is_in_path = any(ismember(dir, C));
end



%%% Section with test cases
%
% Convenience function to refer to the function under test (FUT).
%!function fut = FUT() fut = @is_directory_in_path; end


%!test % Self check that the dir containing this script is in the path
%!  fut = FUT();
%!  path_to_test = fullfile(pwd(), 'dummy-1-2-3');
%!  modified_path = [path_to_test pathsep() path()];
%!  result = fut(path_to_test, modified_path);
%!  assert(result, true);


%!test % Check that some weird string is not in the Octate/MATLAB path
%!  fut = FUT();
%!  dir_not_in_path = fullfile('dummy', 'x', 'y', 'z');
%!  result = fut(dir_not_in_path);
%!  assert(result, false);


%!error <not enough input arguments> FUT()();
