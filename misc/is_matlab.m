%% Support function to check if running from MATLAB as opposed to Octave.
%
% See also: is_octave.m

% © 2017 Christian Ridderström <chr@kth.se>. Licensed under LGPL v3 or later.
function y = is_matlab()
  persistent cache;
  if (isempty(cache))
    cache = ~exist ('OCTAVE_VERSION', 'builtin') ~= 0;
  end
  y = cache;
end

%% Note: Based on https://stackoverflow.com/a/9838357

%%% Section with (Octave) test cases
%
% Convenience function to refer to the function under test (FUT).
%!function fut = FUT() fut = @is_matlab; end

%!assert (isequal(FUT()(), false))
