%% Generate combinations of variable modifications
%
%% todo: Change doc. to reflect that output is now a factor
%% todo: Update test cases accordingly
%
% Given "factors" of variable modifications, generate a "product" that
% corresponds to all combinations of variable modifications.
%
% The "factors" and "product" includes a reference, a cellstr, to the
% variables to be modified. Typically the variable to be changed is a
% field in struct, which is often used as a parameter.
%
% The following example illustrates how to specify combinations of variable
% modifications. Also see the test cases at the end of this file.
%
%!demo
%!  vmd_1 = { { 'S1' 'f1' } 1 ; ...
%!            { 'S1' 'f2' } 5   };
%!  vmd_2 = { { 'S2' 'f2' } 2 3 };
%!  vmd_3 = { { 'S1' 'f3' } 4 5 };
%!  var_mod_data = { vmd_1  vmd_2  vmd_3 };
%!  C = combine_variable_modifications(var_mod_data)
%!  # Variable modification data is abbreviated 'VMD'above.
%!  # Combination keeps 'S1.f1' at 1 while 'S2.f2' varies over 2 and 3
%
% The "factors" specifying the combinations, i.e. 'vmd_1' etc,
% are passed in as a (row) cell array. The example above returns:
%
%   { ...
%     1  1  1  1  { 'S1' 'f1' } ; ...
%     5  5  5  5  { 'S1' 'f2' } ; ...
%     2  3  2  3  { 'S2' 'f2' } ; ...
%     4  4  5  5  { 'S1' 'f3' }   ...
%   }
%
% Note that the first two columns of the returned cell array corresponds to two
% different sets of variable modifications, where e.g. the 2nd column corresponds
% to modifying 'S1.f1' to 1, 'S1.f2' to 5 and, 'S2.f2' to 3 and 'S1.f3' to 4.
%
% Each factor should be a cell array, where each row corresponds to one
% variable modification. The first element of the row is a cellstr
% that references the variable to be changed. E.g. reference a field
% in a struct as follows:
%       { 'struct_name' 'field_name' }
%
% See the examples and the test cases for more details.
%
%!demo
%!  modification_factors = ...
%!  { ...
%!    { ...
%!      { 'S1' 'field1' } 10; ...
%!      { 'S2' 'field2' } 12 ...
%!    }, ...
%!    { ...
%!      { 'S2' 'field3' } 1 2 ...
%!    } ...
%!  };
%!  C = combine_variable_modifications(modification_factors, 2)
%!  # Select only the 2nd combination from all the combinations
%
% See also variable_modifications_example.m, apply_variable_modifications(), 
% variable_modifications_factor() and variable_modifications_to_char().

% Note: Functions named VMD_…() are found in dir private/.

% © 2017 Christian Ridderström <chr@kth.se>. Licensed under LGPL v3 or later.
function C = combine_variable_modifications(modifications, select)
  assert(nargin >= 1, 'not enough input arguments');
  VMD = modifications;          % Using 'VMD' for terseness
  assert_is_variable_modification_data(VMD, 'modifications');

  if VMD_is_empty(VMD), C = {}; return; end

  value_vectors = VMD_extract_value_vectors(VMD);
  indices = generate_index_combinations(value_vectors);
  values = vert_cat(cellfun(@(v, idx) v(:, idx), ...
                            value_vectors, num2cell(indices, 2)', 'unif', 0));

  if ~exist('select', 'var') || isempty(select)
    select = 1:size(values, 2);
  end

  variable_references = vert_cat(VMD_extract_variable_references(VMD));
  C = [ variable_references values(:, select) ];

  %% For convenience, show result when output is never assigned
  if nargout == 0
    fprintf('%s\n', variable_modifications_to_char({ C }));
  end
end


function assert_is_variable_modification_data(VMD, name)
  assert(iscell(VMD) && isrow(VMD), ...
         'Arg. ''%s'' must be a row cell', name);

  is_factor = cellfun(@VMD_is_factor, VMD);
  if all(is_factor), return; end
  not_factors = find(~is_factor);

  s1 = sprintf('#\tThese elements are not variable modification factors: %s', ...
               num2str(not_factors));
  s2 = sprintf('#\tOnly showing the first invalid element:');
  s3 = evalc('disp(VMD{not_factors(1)})');
  error(['Arg. ''%s'' is invalid, %d of %d elements are not variable ', ...
         'modification factors.\n%s\n%s\n%s'], ...
        name, numel(not_factors), numel(VMD), s1, s2, s3);
end



%%% Section with Octave test cases
%
% Convenience function to avoid modifying tests if renaming the FUT.
% (FUT = function under test).
%
%!function fut = FUT() fut = @combine_variable_modifications; end


%% Example VMD with only a single factor without any rows
%!function VMD = VMD0()
%!  vmd_0 = { };
%!  VMD = { vmd_0 };
%! end


%% Example VMD with single factor, only one row in the factor
%!function VMD = VMD1()
%!  vmd_1 = { { 'v1' } 1 2 };
%!  VMD = { vmd_1 };
%! end


%% Example VMD with two factors, two rows in each
%!function VMD = VMD2()
%!  vmd_1 = { { 'v1' }      1 2; ...
%!            { 's2' 'f1' } 3 4 };
%!  vmd_2 = { { 'v3' }  5 6 7; ...
%!            { 'v4' }  8 9 0};
%!  VMD = { vmd_1, vmd_2 };
%! end


%!test % Simple smoke test, an ("empty) variable modification spec. as input
%!  fut = FUT();
%!  assert(fut({{}}), {});


%!test % Simple single input set of parameters
%!  fut = FUT();
%!  vmd_1 = { { 'S1' 'f1' } 1 };
%!  VMD = { vmd_1 };
%!  expect = { { 'S1' 'f1' } 1 };
%!  result = fut(VMD);
%!  assert(expect, result);


%!test % A single input set of parameters with two parameters
%!  fut = FUT();
%!  vmd_1 = { ...
%!            { 'S1' 'f1' } 1 ; ...
%!            { 'S2' 'f2' } 2   ...
%!          };
%!  VMD = { vmd_1 };
%!  expect = { ...
%!             { 'S1' 'f1' } 1 ; ...
%!             { 'S2' 'f2' } 2   ...
%!           };
%!  result = fut(VMD);
%!  assert(expect, result);


%!test % Combine two input sets of parameters
%!  fut = FUT();
%!  vmd_1 = { { 'S1' 'f1' } 1 };
%!  vmd_2 = { { 'S2' 'f2' } 2 3 };
%!  VMD = { vmd_1  vmd_2 };
%!  result = fut(VMD);
%!  expect = { ...
%!             { 'S1' 'f1' } 1 1 ; ...
%!             { 'S2' 'f2' } 2 3   ...
%!           };
%!  assert(expect, result);


%!test % The first demo
%!  fut = FUT();
%!  vmd_1 = { { 'S1' 'f1' } 1 ; ...
%!            { 'S1' 'f2' } 5   };
%!  vmd_2 = { { 'S2' 'f2' } 2 3 };
%!  vmd_3 = { { 'S1' 'f3' } 4 5 };
%!  VMD = { vmd_1  vmd_2  vmd_3 };
%!  result = fut(VMD);
%!  expect = { ...
%!             { 'S1' 'f1' }   1  1  1  1 ; ...
%!             { 'S1' 'f2' }   5  5  5  5 ; ...
%!             { 'S2' 'f2' }   2  3  2  3 ; ...
%!             { 'S1' 'f3' }   4  4  5  5 ; ...
%!           };
%!  assert(expect, result);


%!test % The last demo
%!  fut = FUT();
%!  VMD = { ...
%!          { ...
%!            { 'S1' 'field1' } 10; ...
%!            { 'S2' 'field2' } 12 ...
%!          }, ...
%!          { ...
%!            { 'S2' 'field3' } 1 2 ...
%!          } ...
%!        };
%!  expect = { ...
%!             { 'S1' 'field1' }  10; ...
%!             { 'S2' 'field2' }  12; ...
%!             { 'S2' 'field3' }   2; ...
%!           };
%!  result = fut(VMD, 2);
%!  assert(expect, result);


%!error<not enough input arguments>FUT()()


%!error<Arg. 'modifications' must be a row cell>FUT()(1)
%!error<Arg. 'modifications' must be a row cell>FUT()({})

