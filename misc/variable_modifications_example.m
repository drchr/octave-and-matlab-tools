%% Example using the functions related to variable modifications.
%
% Look inside this file for more details.
%
% The function returns intermediate results in a struct for you to examine
% or use in your own tests.
%
% Example:
%   S = variable_modifications_example();
%
% See also apply_variable_modifications(), combine_variable_modifications(),
% variable_modifications_factor() and variable_modifications_to_char().

% © 2017 Christian Ridderström <chr@kth.se>. Licensed under LGPL v3 or later.
function S = variable_modifications_example()
  narginchk(0, 0);
  if nargout == 0, help variable_modifications_example; return; end

  S.VMD = make_variable_modification_data();
  show_modifications(S.VMD, 'S.VMD');

  S.combinations = combine_variable_modifications(S.VMD);
  show_modifications({ S.combinations }, 'S.combinations');

  S.dummy_variables = make_dummy_variables_to_be_modified();
  S.modified_vars = modify_variables(S.dummy_variables, S.combinations, 2);
end


function vars = modify_variables(vars_to_modify, combinations, mod_set)
  %% Select the variable modifications to apply
  MD = combinations(:, [1 1+mod_set]);

  %% Illustrate modifying a single variable in current workspace
  some_variable = vars_to_modify.some_variable;
  some_variable = apply_variable_modifications(some_variable, MD, 'some_variable');
  vars.some_variable = some_variable;

  %% Illustrate modifying fields in a struct
  vars.Pars = apply_variable_modifications(vars_to_modify.Pars, MD, 'Pars');
end
  



%% Illustrate several ways in which you can create a specification for
% how variables should be changed.
%
% It's assumed the "destination" of the variables to change are
% fields in a struct. The three structs assumed in this example
% are assumed to be called:
% - 'Pars'
% - 'name_of_struct_with_long_name'
% - 'another_struct'
%
% For completeness, we also assume that a variable named 'some_variable'
% will be changed.
function specs = make_variable_modification_data()

  %% To avoid repetition, it can be useful to specify the destination
  % name in only one place. Let's assume 
  name_long = 'pars_with_long_name';
  another_name = 'another_struct';

  %% Helper function to create specifications for fields in the
  % destination with the name 'name'.
  P = @(f) { name_long f };

  %% Another helper function, for desinations in 'another_struct'
  A = @(f) { another_name f };

  %% Desinations that are fields in a struct can also be specified
  %% as a string, with the parts separated by a perdod ('.').

  ctor = @variable_modifications_factor;

  %% Create a specification of parameters where the rows illustrate a
  %% few different ways to specify the variable/parameter/field that
  %% is to be changed.
  %% The set of new values is given as colum vectors in the cell array,
  %% here there's only one vector of values.
  p1 = ctor({ ...
              { 'Pars' 'f11' }       11.1 ; ...
              { name_long  'f12' }   12.1 ; ...
              'Pars.f13'             13.1 ; ...
              P('f14')               14.1 ; ...
              A('f15')               15.1 ; ...
              'some_variable'        16.1 ; ...
            });

  %% Note: you don't have to use the constructor function, but it does
  %% error checking so you'll catch mistakes earlier. This is the result:
  p1_expt = { ...
              { 'Pars' 'f11' }       11.1 ; ...
              { name_long  'f12' }   12.1 ; ...
              { 'Pars' 'f13'}        13.1 ; ...
              { name_long 'f14' }    14.1 ; ...
              { another_name 'f15' } 15.1 ; ...
              { 'some_variable' }    16.1 ; ...
            };
  assert(isequal(p1, p1_expt));

  %% Here's how you specify two value vectors.
  p2 = ctor({ ...
              'Pars.f21'   21.1  21.2; ...
              'Pars.f22'   22.1  22.2; ...
              'Pars.f23'   23.1  23.2; ...
            });

  %% And here's how you can specify three value vectors
  p3 = ctor({ ...
              'Pars.f31'   31.1  31.2  31.3; ...
            });

  %% The values, i.e. the elements of the value vectors, must be
  %% numbers or structs, but they don't have to be scalars.
  p4 = { ...
         'Pars.f41_vector2'  [41.11 41.12]   [41.21 42.22]; ...
         'Pars.f42_struct'   struct('a',1)   struct('a', 2); ...
         'Pars.f43_vector3'  40.10+(1:3)     40.20+(1:3); ...
       };

  %% Finally, as we are interested in specifying combinations of
  %% parameter modifictaions, we want to collect the "factors" of this
  %% "production" into a single cell row with variable modification data.
  [specs{1:4}] = variable_modifications_factor(p1, p2, p3, p4);
end


function S = make_dummy_variables_to_be_modified()
  some_variable = 0;

  Pars = struct('f11', 0, 'f12', 0, 'f13', 0, ...
                'f21', 0, 'f22', 0, 'f23', 0, ...
                'f31', 0, 'f32', 0, 'f33', 0, ...
                'f41_vector2', [0 0], ...
                'f42_struct', struct(), ...
                'f43_vector3', [0 0 0]);

  parameters_with_long_name = struct('f12', 0, 'f14', 0);

  another_struct = struct('f15', 0);

  for Name = who()'
    S.(Name{1}) = eval(Name{1});
  end
end


function show_modifications(modifications, text)
  fprintf('\n%s\n', repmat('-', 1, 70));
  opts = struct('max_cols', 4);
  s = variable_modifications_to_char(modifications, opts);
  fprintf('%s = \n\n%s\n', text, s);
end


%%% Section with Octave test cases
%
% Convenience function to avoid modifying tests if renaming the FUT.
% (FUT = function under test).
%
%!function fut = FUT() fut = @variable_modifications_example; end


%!error<too many input arguments>FUT()(1)
