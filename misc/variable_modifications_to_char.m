%% todo: Add test case for when there's an 'opts' argument
%% Convert a cell array with variable modifications to char.
%
%!demo
%!  specs = { { { 's1' 'f1' } 11.1; ...
%!              { 's2' 'f2' } 12.1 } };
%!  s = variable_modifications_to_char(specs);
%!  fprintf('%s\n', s);
%
% See also variable_modifications_example.m, apply_variable_modifications(),
% combine_variable_modifications(), and variable_modifications_factor(),
% as well as cell_to_char().
%
% Caveat: The result is only intended for humans, i.e. it's lossy.

% © 2017 Christian Ridderström <chr@kth.se>. Licensed under LGPL v3 or later.
function s = variable_modifications_to_char(specs, opts)
  narginchk(1, 2);
  assert(iscell(specs));
  if ~exist('opts', 'var') || isempty(opts), opts = struct(); end

  nr_of_factors = numel(specs);
  [factor_rows, factor_cols] = cellfun(@(C) size(C), specs);
  nr_of_vars = sum(factor_rows);
  nr_of_combinations = prod(factor_cols - 1);
  s0 = sprintf(['Variable modification factors ', ...
                ' (factors: %d, vars.: %d, comb.: %d)'], ...
               nr_of_factors, nr_of_vars, nr_of_combinations);
  C = cellfun(@(set, set_number) show_spec_set(set, set_number, opts), ...
              specs, num2cell(1:nr_of_factors), 'uniform', 0);
  s = strjoin([{s0} C], sprintf('\n'));
end


function s = show_spec_set(set, set_number, opts)
  [nr_of_variables, cols] = size(set);
  nr_of_combinations = cols - 1;
  s0 = sprintf('  Factor %d (variables: %d, combinations: %d)\n', ...
               set_number, nr_of_variables, nr_of_combinations);

  opts0 = struct('return_cellstr',   true, ...
                 'indent_size',      0, ...
                 'column_separator', '  ', ...
                 'row_separator',    '', ...
                 'max_column_width', 16);
  for F = fieldnames(opts0)'
    if ~isfield(opts, F{1}), opts.(F{1}) = opts0.(F{1}); end
  end
  C = cell_to_char(set(:, 2:end), opts)';

  s1 = strjoin(cellfun(@(a,b) sprintf('    %s    %s\n', a, b), ...
                       format_names(set(:, 1), 20), C, 'unif', 0), '');

  s = [s0 s1];
end


function names = format_names(path_specs, max_width)
  max_name_length = max(cellfun(@(c) length(strjoin(c)), path_specs));
  width = min(max_width, max_name_length);
  fmt = sprintf('%%-%ds', width);
  names = cellfun(@(c) sprintf(fmt, truncate_path(c, width, '.')), ...
                  path_specs, 'uniform', 0);
end


%%% Section with Octave test cases
%
% Convenience function to avoid modifying tests if renaming the FUT.
% (FUT = function under test).
%
%!function fut = FUT() fut = @variable_modifications_to_char; end



%!test % Simple smoke test
%!  fut = FUT();
%!  specs = { { { 's1' 'f1' } 11.1; ...
%!              { 's2' 'f2' } 12.1  } };
%!  result = fut(specs);


%!error<not enough input arguments>FUT()()
