%% Convenience function that returns a variable's value if it's
% defined in the caller's workspace and it's is not empty.
% Otherwise a default value is returned.
%
% Note: Not recommended for performance critical code sections.
%
%!demo
%!  default = @use_default_if_variable_is_undefined_or_empty;
%!  x_missing = default('x_missing', 2)
%!  # Sets 'x' to 2' as the variable 'x_missing' is undefined
%
%!demo
%!  default = @use_default_if_variable_is_undefined_or_empty;
%!  x_exists = 1;
%!  x_exists = default('x_exists', 3)
%!  # Leaves 'x_exists' unchanged as it exists
%
%!demo
%!  default = @use_default_if_variable_is_undefined_or_empty;
%!  x_exists = 1;
%!  [x_exists, x_missing] = default('x_exists', 3, 'x_missing', 2)
%!  # Leaves 'x_exists' unchanged, but sets 'x_missing' to 2

% © 2017 Christian Ridderström <chr@kth.se>. Licensed under LGPL v3 or later.
function [varargout] = use_default_if_variable_is_undefined_or_empty(varargin)
  assert(nargin >= 1, 'Not enough input arguments');
  assert(iscellstr(varargin(1:2:end)), 'Variable name arguments must be strings');
  varargout = {};

  for ii = 1:2:numel(varargin)
    [var_name, default_value] = deal(varargin{ii}, varargin{ii+1});
    is_defined_cmd = sprintf('exist(''%s'', ''var'')', var_name);

    if evalin('caller', is_defined_cmd);
      varargout{end+1} = evalin('caller', var_name);
      if isempty(varargout{end}), varargout{end} = default_value; end
    else
      varargout{end+1} = default_value;
    end
  end
end



%%% Section with Octave test cases
%
% Convenience function to avoid modifying tests if renaming the FUT.
% (FUT = function under test).
%
%!function fut = FUT() fut = @use_default_if_variable_is_undefined_or_empty; end


%!test % Single defined variable
%!  x_exists = 1;
%!  fut = FUT();
%!  result = fut('x_exists', 2);
%!  assert(result, 1);


%!test % Single undefined variable
%!  # x_missing = 2;
%!  fut = FUT();
%!  result = fut('x_missing', 3);
%!  assert(result, 3);


%!test % Single variable that's empty
%!  x_empty = [];
%!  fut = FUT();
%!  [result{1}] = fut('x_empty', 5);
%!  assert(result, {5});


%!test % One undefined variable and one defined variable
%!  # x_missing = 4;
%!  x_exists = 1;
%!  fut = FUT();
%!  [result{1:2}] = fut('x_missing', 3, 'x_exists', 2);
%!  assert(result, {3, 1});



%!error<Not enough input arguments>FUT()()

%!error<Variable name arguments must be strings>FUT()(1, 1)


