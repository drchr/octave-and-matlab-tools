%% Convenience function to check if the diary is off, i.e.
% not outputting text to a log file.
%
% For Octave, this function currently doesn't work and
% just returns that there's no diary functionality active.

% © 2017 Christian Ridderström <chr@kth.se>. Licensed under LGPL v3 or later.
function [is_off, diary_file] = is_diary_off()
  if is_matlab()
    is_off = strcmpi(get(0, 'Diary'), 'off');

    if ~is_off && nargout > 1
      diary_file = get(0, 'DiaryFile');
    end
  else
    [is_off, diary_file] = deal(true, []);
  end
end


%%% Section with test cases - not yet implemented
%
% Convenience function to refer to the function under test (FUT).
%!function fut = FUT() fut = @is_diary_off; end

