%%% Generate the indices corresponding to all combinations of 'length(L)' sets,
% where 'L' is a row vector containing the sizes of a number of sets.
%
% The indices of the different combinations are returned as 'prod(L)' column
% vectors forming a matrix. The j:th column vector comprises the indices for
% the j:th combination, and the i:th row is the index for the i:th set.
%
% Example: Assume two sets, of size three and two respectively, then:
%     generate_index_combinations([3 2])
% results in these six combinations:
%   1   2   3   1   2   3
%   1   1   1   2   2   2
%
% Note that the members of the first set are varied first.
%
% A use case corresponding to the example above would be that the first
% set contains three combinations of parameters, and the second set contains
% two parameter combinations. There is then six ways to combine the parameter
% sets.
%
%!demo
%!  generate_index_combinations([3 2])
%!  # Indices of combinations for two sets of size three and two respectively.
%
%!demo
%!  s1 = [ 1.1  1.2  1.3 ; ...  # Set 1, two values in each of the three variants
%!         2.1  2.2  2.3 ];
%!  s2 = [ 5    6  ];           # Set 2
%!  indices = generate_index_combinations([length(s1) length(s2)]);
%!  C = cat(1, s1(:, indices(1,:)), s2(:, indices(2,:)));
%!  disp(C)
%!  # Indices of combinations for two sets of size two and three resp.
%!  # The columns in 'C' contain all the different combinations of values.
%
%!demo
%!  sets = { [ 1.1  1.2  1.3 ; ...  
%!             2.1  2.2  2.3 ], ...
%!           [ 5    6  ] ...
%!         };
%!  indices = generate_index_combinations(sets)
%!  # For convenience, if given a cell array, the n:o columns in the
%!  # elements of the array are used to define 'L'.
%
%!demo
%!  sets = { [ 1.1  1.2  1.3 ; ...  
%!             2.1  2.2  2.3 ], ...
%!           [ 5    6  ] ...
%!         };
%!  indices = generate_index_combinations(sets)
%!  cellfun(@(s, idx) s(:, idx), sets, num2cell(indices, 2)', 'uniform', 0);
%!  combinations = vertcat(ans{:})
%!  # Generalised way to also create the value combinations based on the indices.
%
%!demo
%!  sets = { { 1.1  1.2  1.3 ; ...  
%!             2.1  2.2  2.3  }, ...
%!           { [1 1]  [2 2] } ...
%!         };
%!  indices = generate_index_combinations(sets)
%!  cellfun(@(s, idx) s(:, idx), sets, num2cell(indices, 2)', 'uniform', 0);
%!  combinations = vertcat(ans{:});
%!  last_combination = combinations(:,end)
%!  # Using cell arrays and also non-scalar values as elements of the last value set

% © 2017 Christian Ridderström <chr@kth.se>. Licensed under LGPL v3 or later.
function indices = generate_index_combinations(L)
  narginchk(1, 1);
  if isempty(L), indices = []; return; end
  if ~iscell(L)
    indices = make_indices(L);
  else
    indices = make_indices(cellfun(@(x) size(x,2), L));
  end
end


function indices = make_indices(L)
  assert(isrow(L));
  indices = 1:L(1);
  for d = L(2:end)
    new_row = reshape(repmat(1:d, size(indices, 2), 1), 1, []);
    indices = [repmat(indices, 1, d) ; new_row ];
  end
end


%%% Section with Octave test cases
%
% Convenience function to avoid modifying tests if renaming the FUT.
% (FUT = function under test).
%
%!function fut = FUT() fut = @generate_index_combinations; end


%!test % Input an empty matrix, or a single one, two and three
%!  fut = FUT();
%!  assert(fut([]), []);
%!  assert(fut(1), 1);
%!  assert(fut(2), 1:2);
%!  assert(fut(3), 1:3);


%!test % Somewhat more complicated
%!  fut = FUT();
%!  expected = [ 1 2 3  1 2 3 ; ...
%!               1 1 1  2 2 2 ];
%!  assert(fut([3 2]), expected);


%!test % Somewhat more complicated, 2
%!  fut = FUT();
%!  expected = [ 1 2  1 2  1 2 ; ...
%!               1 1  2 2  3 3 ];
%!  assert(fut([2 3]), expected);


%!test % Input cell array from which lengths are extracted
%!  fut = FUT();
%!  inputs = { [1 2; 3 4; 5 6]  [1 2 3] };
%!  expected = [ 1 2  1 2  1 2 ; ...
%!               1 1  2 2  3 3 ];
%!  assert(fut(inputs), expected);

%!error<not enough input arguments>FUT()()
