%% Convenience function for use in other functions, to make it easy
% to replace an empty argument with a default value.
%
% Example of use in functions:
%   function foo(opt1, opt2)
%     if ~exist('opt1', 'var'), opt1 = []; end
%     if ~exist('opt2', 'var'), opt2 = []; end
%     [opt1, opt2] = use_default_if_empty(opt1, 10, opt2, 20);
%
%!demo
%!  x_default = 10;
%!  x = use_default_if_empty([], x_default)
%!  # Returns 'x_default' as the first argument, '[]', is empty
%
%!demo
%!  x_default = 10;
%!  x = use_default_if_empty(1, x_default)
%!  # Returns the first argument as it's not empty
%
%!demo
%!  [x_default, y_default] = deal(11, 12);
%!  [x, y] = use_default_if_empty([], x_default, 2, y_default)
%!  # Assign defaults when empty for multiple arguments.
%!  # Here 'x' is set to 'x_default', and 'y' stays at '2'.

% © 2017 Christian Ridderström <chr@kth.se>. Licensed under LGPL v3 or later.
function varargout = use_default_if_empty(varargin)
  assert(nargin >= 2, 'Not enough input arguments');
  varargout = varargin(1:2:end);
  defaults  = varargin(2:2:end);
  idx = cellfun(@isempty, varargout);
  varargout(idx) = defaults(idx);
end



%%% Section with Octave test cases
%
% Convenience function to avoid modifying tests if renaming the FUT.
% (FUT = function under test).
%
%!function fut = FUT() fut = @use_default_if_empty; end


%!test % One non-empty argument
%!  fut = FUT();
%!  result = fut(1, 2);
%!  assert(result, 1);


%!test % One empty argument
%!  fut = FUT();
%!  result = fut([], 2);
%!  assert(result, 2);


%!test % One non-empty and one empty argument
%!  fut = FUT();
%!  [result{1:2}] = fut(1, 2, [], 4);
%!  assert(result, {1, 4});


%!test % One empty and one non-empty argument
%!  fut = FUT();
%!  [result{1:2}] = fut([], 2, 3, 4);
%!  assert(result, {2, 3});


%!error<Not enough input arguments>FUT()()
