% Support function to add field(s) that are missing from a struct,
% 'S'. Typically used by other functions, e.g. to add defaults to
% a struct that's used as an 'options' struct.
%
% Example:
%     function foo(x, opts)
%       opts_default = struct('opt_flag_1', true);
%       opts = add_missing_field_to_struct(opts, opts_default);
%     end
%
% Note: If an optional last argument ('consider_empty_missing')
% is given as 'true', then empty fields in 'S' are
% also considered as being missing.
%
% Used e.g. to add default values.
%
%!demo
%!  S = struct('a', 1);
%!  S = add_missing_field_to_struct(S, 'b', 2, 'c', 3)
%!  # Adds the field '.b' as 2 and the field '.c' as 3
%
%!demo
%!  S = struct('a', 1);
%!  S = add_missing_field_to_struct(S, { 'b' 2; 'c' 3 })
%!  # Adds '.b' as 2 and '.c' as 3.
%!  # Imporant: Note the ';' after the '2' in the argument above.
%
%!demo
%!  S = struct('a', 1);
%!  S = add_missing_field_to_struct(S, struct('b', 2, 'c', 2))
%!  # Adds '.b' as 2 and '.c' as 3
%
%!demo
%!  S = struct('b', []);
%!  S = add_missing_field_to_struct(S, struct('b', 2), true)
%!  # Replaces empty '.'b with a value

% © 2017 Christian Ridderström <chr@kth.se>. Licensed under LGPL v3 or later.
function S = add_missing_field_to_struct(S, varargin)
  assert(nargin >= 2, 'Expecting at least two arguments');
  assert(isstruct(S), 'First argument must be a struct');

  [defaults, consider_empty_missing] = parse_arguments(varargin);

  assert(all(cellfun(@ischar, defaults(:,1))), ...
        'Some field names are not char');

  for D = defaults'
    [field, value] = deal(D{1}, D{2});
    if ~isfield(S, field) || (consider_empty_missing && isempty(S.(field)))
      S.(field) = value;
    end
  end
end


function C = reshape_pair_values(C)
  if isrow(C)
    C = reshape(C, 2, [])';
  end
end


function [defaults, consider_empty_missing] = parse_arguments(args)
  consider_empty_missing = false;
  N = numel(args);

  if ischar(args{1})
    assert(mod(N, 2) == 0, ...
          'Expected an even n:o arguments when receiving name-value-pairs');
    defaults = reshape_pair_values(args);
  else
    if isstruct(args{1})
      defaults = [ fieldnames(args{1}) struct2cell(args{1}) ];
    elseif iscell(args{1});
      %% For convenience, support a single row of name-value-pairs by reshaping it
      defaults = reshape_pair_values(args{1});
    else
      error('Second argument is of unsupported type: %s', class(args{1}));
    end
    if N == 2, consider_empty_missing = args{2}; end
    assert(N <= 2, 'Too many arguments');
  end
end


%%% Section with test cases - not yet implemented
%
% Convenience function to refer to the function under test (FUT).
%!function fut = FUT() fut = @add_missing_field_to_struct; end


%!test # demo 1
%!  fut = FUT();
%!  S = struct('a', 1);
%!  result = fut(S, 'b', 2, 'c', 3);
%!  expect = struct('a', 1, 'b', 2, 'c', 3);
%!  assert(result, expect);


%!test # demo 2
%!  fut = FUT();
%!  S = struct('a', 1);
%!  result = fut(S, { 'b' 2; 'c' 3 });
%!  expect = struct('a', 1, 'b', 2, 'c', 3);
%!  assert(result, expect);


%!test # demo 2c
%!  fut = FUT();
%!  S = struct('a', 1);
%!  result = fut(S, { 'b' 2  'c' 3 'd' 4});
%!  expect = struct('a', 1, 'b', 2, 'c', 3, 'd', 4);
%!  assert(result, expect);


%!test # demo 3
%!  fut = FUT();
%!  S = struct('a', 1);
%!  result = fut(S, struct('b', 2, 'c', 3));
%!  expect = struct('a', 1, 'b', 2, 'c', 3);
%!  assert(result, expect);


%!test # demo 4a
%!  fut = FUT();
%!  S = struct('b', []);
%!  result = fut(S, struct('b', 2), false);
%!  expect = struct('b', []);
%!  assert(result, expect);


%!test # demo 4b
%!  fut = FUT();
%!  S = struct('b', []);
%!  result = fut(S, struct('b', 2), true);
%!  expect = struct('b', 2);
%!  assert(result, expect);


%!error <Expecting at least two arguments> FUT()();

%!error <Expecting at least two arguments> FUT()(1);

%!error <First argument must be a struct> FUT()(1, 2);

%!error <Expected an even n:o arguments when receiving name-value-pairs> ...
%!      FUT()(struct(), 'a', 1, 'b');

%!error <Second argument is of unsupported type: double> ...
%!      FUT()(struct(), 1, 1);

%!error <Too many arguments> ...
%!      FUT()(struct(), {'a' 1}, false, 1);

%!error <Some field names are not char> FUT()(struct(), {1 1});
