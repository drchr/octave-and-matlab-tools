%% Convenience function to assert that e.g. an Octave package
% is available with a sufficiently high version number, i.e.
% equal or larger.
%
% Can also be used for checking the version of a custom package.
%
% Caveat: Comparison of versions is done lexiographically.
%
%!demo
%!  if is_octave(), assert_sufficient_package_version('io', '1.2'); end
%!  # Check that at least version 1.2 is available for the Octave package 'io'
%
%!demo
%!  if is_matlab(), assert_sufficient_package_version('Simulink', '5.0'); end
%!  # Check that at least version 8.8 is available for the MATLAB toolbox 'Simulink'
%
%!demo
%!  assert_sufficient_package_version(which('package_info'), '1.0');
%!  # Checks that the version of the package containing these scripts
%!  # are of at least version 1.0. See 'package_info.m' for more info.
%
%!demo
%!  cur_ver = 20010704;  % Somehow get the current version of a custom package
%!  assert_sufficient_package_version(cur_ver, 20001203);
%!  # Check that at least version 20170101 is available of a custom package
%!  # The required version in 'v' must be either a number, or a string
%!  # with digits (and some periods).
%
% See also package_info.m, is_version_string.m, version_string_at_least.m

% © 2017 Christian Ridderström <chr@kth.se>. Licensed under LGPL v3 or later.
function assert_sufficient_package_version(pkg_name_or_current_ver, required_ver)
  narginchk(2, 2);

  if ~ischar(pkg_name_or_current_ver)
    assert(is_number(pkg_name_or_current_ver), ...
           'Expected 1st arg to be scalar number if it is not a string');
    assert(is_number(required_ver), 'Expected 2nd arg. to also be a number');
    current_ver = pkg_name_or_current_ver;
    assert(current_ver >= required_ver, ...
           'The current version, %.1f, is less than the required version: %.1f', ...
           current_ver, required_ver);
    return
  end

  assert(ischar(pkg_name_or_current_ver), ...
         'Unexpected type of 1st argument: ''%s''', class(pkg_name_or_current_ver));

  assert(is_version_string(required_ver), ...
         'Expected 2nd argument to be a version string');

  if is_version_string(pkg_name_or_current_ver)
    pkg_name = '';
    current_ver = pkg_name_or_current_ver;
  else
    pkg_name = pkg_name_or_current_ver;
    info = package_info(pkg_name);
    assert(~isempty(info), 'Unable to find package ''%s''', pkg_name);
    current_ver = info.version;
  end

  assert(version_string_at_least(current_ver, required_ver), ...
         ['The current version, ''%s'' of package ''%s'', ', ...
          'is less than the required version ''%s'''], ...
         current_ver, pkg_name, required_ver);
end


function y = is_number(x)
  y = isscalar(x) && isreal(x) && ~ischar(x);
end



%%% Section with Octave test cases
%
% Convenience function to avoid modifying tests if renaming the FUT.
% (FUT = function under test).
%
%!function fut = FUT() fut = @assert_sufficient_package_version; end

%!test % Compare version of a package
%!  assert_sufficient_package_version('io', '1.2');

%!test % Compare numeric versions
%!  assert_sufficient_package_version(2001.0, 2000.11);

%!test % Compare string versions
%!  assert_sufficient_package_version('1.0', '0.9');

%% Note: Unfortunately the error message below depends on the Octave version
%% that is uses. Not sure how/if that's possible to fix.
%!error<Current version, '2.4.13' of package 'io', is less than version '10'>FUT()('io', '10')

%!error<Current version, 0.9, is less than version 1.0>FUT()(0.9, 1.0)

%!error<Current version, '0.9' of package '', is less than version '1.0'>FUT()('0.9', '1.0')


%!error<not enough input arguments>FUT()()

%!error<not enough input arguments>FUT()(1)

%!error<Expected 1st arg to be scalar number if it is not a string>FUT()([1 1], 1)

%!error<Expected 1st arg to be scalar number if it is not a string>FUT()(struct(), 1)

%!error<Expected 2nd arg. to also be a number>FUT()(1, '1')

%!error<Current version, 2001.0, is less than version 2001.1>FUT()(2001.0, 2001.1)
