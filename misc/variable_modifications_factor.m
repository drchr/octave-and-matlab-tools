%% Create an element, a "factor", of variable modifications data (VMD).
%
% This convenience function allows the user to more conveniently
% create an element for a VMD cell row. In particular, allow the user
% to reference variables using a char vector rather than a cellstr.
%
% From such a VMD, all the combinations ("product") of variable values
% can be generated.
%
%!demo
%!  vmd_0 = variable_modifications_factor()
%!  # Create an empty element for a VMD cell row.
%!  VMD = { vmd_0 }; # Create an "empty" VMD
%
%!demo
%!  # Illustrate a few different ways to reference variables.
%!  # First define some constants and help functions
%!  P0 = 'long_struct_name';
%!  P0_1 = @(field) { P0 field };
%!  P0_2 = @(field) [ P0 '.' field ];
%!  # Prepare a cell array, 's1', that's a variable modification
%!  # with two sets, two "vectors", of alternative variable values.
%!  s1 = { ...
%!         { 's0' 'f11' }   11.1 11.2; ...
%!         { P0  'f12' }    12.1 12.2; ...
%!         's1.f13'         13.1 13.2; ...
%!         P0_1('f14')      14.1 14.2; ... 
%!         P0_2('f15')      15.1 15.2; ...
%!       };
%!  # Create the factor, with two vectors of alternative variable values
%!  variable_modifications_factor(s1)
%
% See the Octave test cases at the end of this function for more examples.
% Note: Running the tests requires being in the dir of this function.
%
% See also variable_modifications_example.m, apply_variable_modifications(),
% combine_variable_modifications(), and variable_modifications_to_char().

% © 2017 Christian Ridderström <chr@kth.se>. Licensed under LGPL v3 or later.
function [varargout] = variable_modifications_factor(varargin)
  if ~nargin, varargout{1} = {}; return; end

  arg_numbers = num2cell(1:nargin);
  cellfun(@assert_cell_array,          varargin, arg_numbers);
  cellfun(@assert_not_empty,           varargin, arg_numbers);
  cellfun(@assert_1st_col_is_cellstr_or_char, varargin, arg_numbers);
  cellfun(@assert_has_value_columns,   varargin, arg_numbers);
  cellfun(@assert_valid_value_vectors, varargin, arg_numbers);
  assert(nargout == nargin || (nargout == 0 && nargin == 1), ...
         'The n:o output arguments, %d, must match the n:o inputs: %d', ...
         nargout, nargin);

  S = cellfun(@check_and_canonize_parameter_specifications, varargin, 'unif', 0);

  varargout = S;
end


function C = check_and_canonize_parameter_specifications(arg, arg_nr)
  refs = cellfun(@VMD_variable_reference, arg(:,1), 'uniform', 0);
  C = [refs arg(:, 2:end)];
end


function assert_cell_array(arg, arg_nr)
  assert(iscell(arg), 'Argument %d is not a cell array', arg_nr);
end


function assert_not_empty(arg, arg_nr)
  assert(~isempty(arg), 'Argument %d may not be empty', arg_nr);
end


function assert_1st_col_is_cellstr_or_char(arg, arg_nr)
  assert(all(cellfun(@(c) ischar(c) || iscellstr(c), arg(:,1))), ...
         'First column of arg. %d is not all cellstr:s or char:s', arg_nr);
end


function assert_has_value_columns(arg, arg_nr)
  assert(size(arg, 2) >= 2, 'Argument %d has no value columns', arg_nr);
end


function assert_valid_value_vectors(arg, arg_nr)
  valid = cellfun(@(c) isnumeric(c) || isstruct(c), arg(:, 2:end));
  if all(all(valid)), return, end

  for col = 1:(size(arg, 2)-1)
    for row = 1:size(arg, 1)
      if ~valid(row, col)
        error(['Invalid value, not numeric nor struct, in row %d ', ...
               'of vector %d in arg. %d'], ...
              row, col, arg_nr);
      end
    end
  end
end


%%% Section with Octave test cases
%
% Convenience function to avoid modifying tests if renaming the FUT.
% (FUT = function under test).
%
%!function fut = FUT() fut = @variable_modifications_factor; end


%!test % Simple smoke test
%!  fut = FUT();
%!  arg = { { 's1' 'f1' } 1 2 3 };
%!  expected = arg;
%!  result = fut(arg);
%!  assert(expected, result);


%!test % Simple smoke test 2
%!  fut = FUT();
%!  arg = { { 's1' } struct('a', 1) };
%!  expected = arg;
%!  result = fut(arg);
%!  assert(expected, result);


%!error<Argument 1 is not a cell array>FUT()(1)

%!error<Argument 2 is not a cell array>FUT()({}, 1)

%!error<Argument 1 may not be empty>FUT()({})

%!error<First column of arg. 1 is not all cellstr:s or char:s>...
%!     FUT()({ { 1 } 1 2 3 })

%!error<Argument 1 has no value columns>FUT()({ 's1.f1' })

%!error<Invalid value, not numeric nor struct, in row 2 of vector 3 in arg. 1>...
%!     FUT()({ 's1.f1' 1 2 3; 's1.f2' 1 2 'a' })

%!error<The n:o output arguments, 0, must match the n:o inputs: 2>...
%!     FUT()({ 's1.f1' 1}, { 's2.f2' 2})
