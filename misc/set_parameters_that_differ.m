%% Modify parameters via a handle for e.g. a Simulink model.
%
% Only set parameters (via call to 'set()') whose current value
% differ from their new value. This is to avoid needlessly
% dirtying e.g. a Simulink model.
%
% The old (and modified) parameters and their original values
% are returned.
%
% In: handle  = Handle (or name) of model
%     args    = Cell array with names/values of new parameter values.
%               First column has names, second column values.
% Out: old_…  = Cell array with _modified_ parameters and their old values.
%               Same format as 'args'
%
%!demo
%!  handle = …;    % May be the name of a Simulink model
%!  args = { 'SaveOutput' 'On'; ...
%!           'StopTime'   '10' };
%!  P_old = set_parameters(handle, args);
%!  % Do stuff
%!  set_parameters_that_differ(handle, P_old);  % Restore modified parameters
%
% todo: Fix test case section

% © 2017 Christian Ridderström <chr@kth.se>. Licensed under LGPL v3 or later.
function old_parameter_values = set_parameters_that_differ(handle, args)
  narginchk(2, 2);
  [handle, args] = parse_arguments(handle, args);
  old_parameter_values = set_parameters(handle, args);
end


function C_old = set_parameters(handle, args)
  C_old = {};
  for F = args'
    [param_name, new_value] = deal(F{:});
    old_value = get(handle, param_name);
    if ~isequal(new_value, old_value)
      C_old = [C_old {param_name old_value}];
      set(handle, param_name, new_value);
    end
  end
end


function [handle, args] = parse_arguments(handle, args)
  if ischar(handle)
    handle = get_param(handle, 'handle');
  end
  assert(ishandle(handle));

  if isstruct(args)
    assert(numel(args) == 1);
    args = [ fieldnames(args) struct2cell(args) ]';
  end

  assert(iscell(args));

  if size(args,1) == 1 && size(args,2) > 2
    args = reshape(args', 2, [])';
  end

  assert(isempty(args) || size(args,2) == 2);
end


%%% Section with Octave test cases
%
% Convenience function to avoid modifying tests if renaming the FUT.
% (FUT = function under test).
%
%!function fut = FUT() fut = @set_parameters_that_differ; end


%!test % Simple smoke test
%!  fut = FUT();
%!  fut(1);

%!error<not enough input arguments>FUT()()
