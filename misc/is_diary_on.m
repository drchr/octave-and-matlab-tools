%% Convenience function to check if the diary is on, i.e.
% if we're outputting text to a diary log file.
%
% For Octave, this function currently doesn't work and
% just returns that there's no diary functionality active.

% © 2017 Christian Ridderström <chr@kth.se>. Licensed under LGPL v3 or later.
function [is_on, diary_file] = is_diary_on()
  if is_matlab()
    is_on = strcmpi(get(0, 'Diary'), 'on');

    if ~is_on && nargout > 1
      diary_file = get(0, 'DiaryFile');
    end
  else
    [is_on, diary_file] = deal(false, []);
  end
end


%%% Section with test cases - not yet implemented
%
% Convenience function to refer to the function under test (FUT).
%!function fut = FUT() fut = @is_diary_on; end


%!test  % Simple smoketest, essentially just detects syntax errors
%!  fut = FUT();
%!  fut();
