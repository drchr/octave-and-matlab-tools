%% Convenience function to check if a variable is not defined
% (i.e., not defined in the workspace that's calling this function).
%
% Note: Not recommended for performance critical code sections.
%
%!demo
%!  is_variable_undefined('x_missing')
%!  # Returns 'true' as the variable 'x_missing' is undefined
%
%!demo
%!  x_exists = 1;
%!  is_variable_undefined('x_exists')
%!  # Returns 'false' as there's a variable 'x_exists'
%
%!demo
%!  x_exists = 1;
%!  is_variable_undefined('x_exists', 'x_unlikely_to_exist')
%!  # Returns '[0 1]', as only the second argument is undefined

% © 2017 Christian Ridderström <chr@kth.se>. Licensed under LGPL v3 or later.
function is_undefined = is_variable_undefined(varargin)
  assert(nargin >= 1, 'Not enough input arguments');
  assert(iscellstr(varargin), 'Arguments must be strings');

  for ii = 1:numel(varargin)
    cmd = sprintf('~exist(''%s'', ''var'')', varargin{ii});
    is_undefined(ii) = evalin('caller', cmd);
  end
end



%%% Section with Octave test cases
%
% Convenience function to avoid modifying tests if renaming the FUT.
% (FUT = function under test).
%
%!function fut = FUT() fut = @is_variable_undefined; end


%!test % Single defined variable
%!  x_exists = [];
%!  fut = FUT();
%!  result = fut('x_exists');
%!  assert(result, false);


%!test % Single undefined variable
%!  # x_missing = [];
%!  fut = FUT();
%!  result = fut('x_missing');
%!  assert(result, true);


%!test % One undefined variable and one defined variable
%!  # x_missing = [];
%!  x_exists = [];
%!  fut = FUT();
%!  result = fut('x_missing', 'x_exists');
%!  assert(result, [true false]);



%!error<Not enough input arguments>FUT()()

%!error<Arguments must be strings>FUT()(1)


