%% Support function that returns various details about the system
% and user. Results are returned in an 'info' struct:
%
% info:
%   .is_octave    = True when in Octave, i.e. result of 'is_octave()'
%   .is_MATLAB    = True when in MATLAB, i.e. result of 'is_matlab()'
%   .is_macOS     = True when on OS X/macOS, result of 'ismac()'
%   .is_unix      = True when on *nix, e.g Linux or macOS,
%                   i.e. result of 'isunix()'
%   .is_linux     = True when *nix and not macOS, which is
%                   typically Linux (but maybe it could be *BSD etc?)
%   .is_windows   = True when on Windows, i.e. result of 'ispc()'
%   .user_name    = Name of user, from $(whoami) on *nix and
%                   %USERNAME% on Windows
%   .user_domain  = Windows only, value of %USERDOMAIN%
%   .uname        = Various results from calling 'uname' on *nix systems
%
%!demo
%!  info = system_and_user_info()
%
%
% Caveat: Don't use this in performance critical code.
%
% See also: is_octave(), is_matlab(), ispc(), isunix(), ismac()

% © 2017 Christian Ridderström <chr@kth.se>. Licensed under LGPL v3 or later.
function info = system_and_user_info()
  narginchk(0, 0);
  is_unix = isunix();
  is_linux = is_unix && ~ismac() && strcmpi('linux', call_system('uname'));
  is_windows = ispc();
  info = struct('is_octave',   is_octave(), ...
                'is_MATLAB',   is_matlab(), ...
                'is_macOS',    ismac(), ...
                'is_unix',     is_unix, ...
                'is_linux',    is_linux, ...
                'is_windows',  is_windows, ...
                'user_domain', [], ...
                'username',    [], ...
                'hostname',    hostname(is_unix), ...
                'uname',       uname_info(is_unix) ...
               );

  [info.user_domain, info.username] = user_domain_and_name(info);
  info.uname = uname_info(is_unix);
end


function [domain, name] = user_domain_and_name(info)
  if info.is_unix
    domain    = [];
    name = call_system('whoami');
  elseif info.is_windows
    domain = getenv('USERDOMAIN');
    name   = getenv('USERNAME');
  else
    disp(info)
    error('Unknown OS, see above for details');
  end
end


function uinfo = uname_info(is_unix)
  uinfo = struct('kernel_name', [], ...
                 'kernel_version', [], ...
                 'hostname',    [], ...
                 'machine',     [], ...
                 'processor',   [], ...
                 'hw_platform', [], ...
                 'OS',          []);
  if ~is_unix, return; end

  uname = @(arg) call_system(['uname ' arg]);
  uinfo.kernel_name = uname('-s');
  uinfo.kernel_version = uname('-v');
  uinfo.hostname    = uname('-n');
  uinfo.machine     = uname('-m');
  uinfo.processor   = uname('-p');
  uinfo.hw_platform = uname('-i');
  uinfo.OS          = uname('-o');
end


function host = hostname(is_unix)
  if is_unix
    host = call_system('uname -n');
  else
    host = getenv('COMPUTERNAME');
  end
end


function out = call_system(varargin)
  [status, out] = system(varargin{:});
  if status ~= 0
    fprintf('# Arguments to ''call_system()'':\n');
    disp(varargin);
    error('System call failed');
  end
  if strcmp(out(end), sprintf('\n')), out = out(1:end-1); end
end



%%% Section with Octave test cases
%
% Convenience function to avoid modifying tests if renaming the FUT.
% (FUT = function under test).
%
%!function fut = FUT() fut = @system_and_user_info; end


%!test % Simple smoke test
%!  fut = FUT();
%!  fut();


%!test % Do a few spot checks
%!  fut = FUT();
%!  info = fut();
%!  assert( info.is_octave);
%!  assert(~info.is_MATLAB);
%!  assert(info.is_unix, isunix());
%!  assert(info.is_windows, ispc());


%!error<too many input arguments>FUT()(1)
