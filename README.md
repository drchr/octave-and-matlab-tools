# octave-and-matlab-tools

Repository for some useful Octave/MATLAB tools I've created, mostly from work on my PhD.

Later updated to try to make scripts work in both Octave and MATLAB.

Licensed under LGPL.


