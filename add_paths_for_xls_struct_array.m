%% Add the subfolder 'xls_struct_array/' to the Octave/MATLAB path,
% making the scripts therein globally usable.
%
% Example:
% 	run(fullfile('<path-to-repo-root>', 'add_paths_for_xls_struct_array.m'));

% © 2017 Christian Ridderström <chr@kth.se>. Licensed under LGPL v3 or later.
addpath(fullfile(fileparts(mfilename('fullpath')), ...
		 'xls_struct_array'));
