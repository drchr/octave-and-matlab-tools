%% TBW
%
%!demo
%!  template(1)
%!  # Remember to add examples here

% © 2017 Christian Ridderström <chr@kth.se>. Licensed under LGPL v3 or later.
function template(x, opts)
  narginchk(1, 2);

  if ~exist('opts', 'var'), opts = []; end

  default_opts = struct();
  opts = use_default_if_empty(opts, default_opts);


end



%%% Section with Octave test cases
%
% Convenience function to avoid modifying tests if renaming the FUT.
% (FUT = function under test).
%
%!function fut = FUT() fut = @template; end


%!test % Simple smoke test
%!  fut = FUT();
%!  fut(1);

%!error<not enough input arguments>FUT()()
